<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="/admin">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">BetPro360</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="{{route('admin')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span>
                <span class="badge badge badge-warning badge-pill float-right mr-2">2</span></a>
            </li>
            <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Users</span></a>
                <ul class="menu-content">
                    <li class="{{ Request::is('user*') ? 'active' : '' }}"><a href="{{route('user')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Users">All Users</span></a> </li>
                   
                    <li class="{{ Request::is('role*') ? 'active' : '' }}"><a href="{{route('role')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Roles">Roles & Permission</span></a></li>

                    <li class="{{ Request::is('advisor*') ? 'active' : '' }}"><a href="{{route('advisor')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Roles">Expert Advisor</span></a></li>
                </ul>
            </li>
            </li>
            <li class="{{ Request::is('plan*') ? 'active' : '' }}"><a href="{{route('plan')}}"><i class="fa fa-yelp"></i><span class="menu-title" data-i18n="Plans">Plans Type</span></a>
            </li>
            <li class="{{ Request::is('duration') ? 'active' : '' }}"><a href="{{route('duration')}}"><i class="feather icon-activity"></i><span class="menu-title" data-i18n="Plan Duration">Plan Duration</span></a>
            </li>

            <li class="{{ Request::is('adminprediction') ? 'active' : '' }}"><a href="{{route('adminprediction')}}"><i class="fa fa-futbol-o"></i><span class="menu-title" data-i18n="Predictions">Predictions</span></a>
            </li>

            <li class="{{ Request::is('country*') ? 'active' : '' }}"><a href="{{route('country')}}"><i class="fa fa-houzz"></i><span class="menu-title" data-i18n="Countries">Countries</span></a>
            </li>

            <li class="{{ Request::is('admincategory') ? 'active' : '' }}"><a href="{{route('admincategory')}}"><i class="fa fa-commenting-o"></i><span class="menu-title" data-i18n="Categories">Categories</span></a>
            </li>

            <li class="{{ Request::is('adminpricing*') ? 'active' : '' }}"><a href="{{route('adminpricing')}}"><i class="feather icon-credit-card"></i><span class="menu-title" data-i18n="Categories">Plan Prices</span></a>
            </li>

            <li class="{{ Request::is('league*') ? 'active' : '' }}"><a href="{{route('league')}}"><i class="fa fa-futbol-o"></i><span class="menu-title" data-i18n="Leagues">Leagues</span></a>
            </li>

            <li class="{{ Request::is('result*') ? 'active' : '' }}"><a href="{{route('result')}}"><i class="fa fa-bell"></i><span class="menu-title" data-i18n="result">Result</span></a>
            </li>

            <li class="{{ Request::is('invest*') ? 'active' : '' }}"><a href="{{route('invest')}}"><i class="fa fa-clock-o"></i><span class="menu-title" data-i18n="invest">Investment Timer</span></a>
            </li>

             <li class="{{ Request::is('advert*') ? 'active' : '' }}"><a href="{{route('advert')}}"><i class="fa fa-bullhorn"></i><span class="menu-title" data-i18n="invest">Advert Setting</span></a>
            </li>
                

            <li class="{{ Request::is('subscription*') ? 'active' : '' }}"><a href="{{route('subscription')}}"><i class="feather icon-credit-card"></i><span class="menu-title" data-i18n="Transactions">Transactions</span></a>
            </li>

            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    <i class="feather icon-power"></i> <span class="menu-title" data-i18n="Settings">Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->