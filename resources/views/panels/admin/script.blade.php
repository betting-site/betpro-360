<!-- BEGIN: Vendor JS-->
    <script src="{{asset('js/admin/vendors.min.js')}}"></script>
    <script src="{{asset('js/admin/forms/select/select2.full.min.js')}}"></script>

    <script src="{{asset('js/admin/scripts/forms/select/form-select2.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('js/admin/tables/datatable/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('js/admin/core/app-menu.js')}}"></script>
    <script src="{{asset('js/admin/core/app.js')}}"></script>
    <script src="{{asset('js/admin/scripts/components.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('js/admin/scripts/datatables/datatable.js')}}"></script>
    <script src="{{asset('js/admin/scripts/extensions/toastr.js')}}"></script>


    <script src="{{asset('js/admin/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('js/admin/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('js/admin/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('js/admin/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{asset('js/admin/scripts/pickers/dateTime/pick-a-datetime.js')}}"></script>  


    <script src="{{asset('js/admin/vue.min.js')}}"></script>  
    <!-- END: Page JS-->
    <script>

        var app = new Vue({
            el: '#app',
            data() {
                return{
                    current : {},
                }
            },
            methods:{
                setCurrent(v){
                    this.current = v;
                }
            }
        });

        $(document).ready(function(){
            $('.close').click(function(){
                $('#toast-container').hide();
            });

            $(".select2-data-ajax").each(function(){
                let $slt = $(this);
                $slt.select2({
                    ajax: {
                        url: "/search/user",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            var evt = $slt.closest('.select-type').data('type');
                            var query = {
                                q : params.term,
                                type : evt
                            }
    
                            return query;
                        },
                        processResults: function (data) {
                        return {
                            results: data,
                            };
                        },
                        cache: true
                    },
                    placeholder: 'Search for a repository'
                    
                });
            });

            // select2 for categories
            $(".select2-ajax").each(function(){
                let $slt = $(this);
                $slt.select2({
                    ajax: {
                        url: "/search/category",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            var evt = $slt.closest('.select-type').data('type');
                            var query = {
                                q : params.term,
                                type : evt
                            }

                            return query;
                        },
                        processResults: function (data) {
                        return {
                            results: data,
                            };
                        },
                        cache: true
                    },
                    placeholder: 'Search for a repository'
                    
                
                }).change(function(){
                    sid = this.value;
                    $.ajax({
                        url: "/specials",
                        dataType: 'json',
                        data : "id="+ sid,
                        success:function(data){
                            let odd = '';

                            special = data.result.special;

                            if(special == 'yes') {
                                odd+='<div class="form-group"><label for="first-name-vertical">1st Set</label><input type="text" class="form-control" name="odd1" placeholder="1set Set Odd" autofocus required></div><div class="form-group"><label for="first-name-vertical">2nd Set</label><input type="text" class="form-control" name="odd2" placeholder="2nd Set Odd" autofocus required></div>';
                            }else{
                                odd+='';
                            }

                            $('.odd').html(odd);
                        }
                    })
                })
            })
        });
</script>