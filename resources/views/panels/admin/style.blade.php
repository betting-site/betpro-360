<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600">
<!-- BEGIN: Vendor CSS-->
<!-- <link rel="stylesheet" type="text/css" href="../index.css"> -->
<link rel="stylesheet" type="text/css" href="../../../css/admin/vendors.min.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/apexcharts.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/tether-theme-arrows.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/tether.min.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/shepherd-theme-default.css">
<!-- END: Vendor CSS-->
<!-- Theme Styles -->
<link rel="stylesheet" href="../../../css/admin/bootstrap.css">
<link rel="stylesheet" href="../../../css/admin/bootstrap-extended.css">
<link rel="stylesheet" href="../../../css/admin/colors.css">
<link rel="stylesheet" href="../../../css/admin/components.css">
<link rel="stylesheet" href="../../../css/admin/dark-layout.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/semi-dark-layout.css">
<!-- Page Styles -->

<link rel="stylesheet" type="text/css" href="../../../css/admin/vertical-menu.css ">
<link rel="stylesheet" type="text/css" href="../../../css/admin/palette-gradient.css ">
<link rel="stylesheet" type="text/css" href="../../../css/admin/dashboard-analytics.css ">
<link rel="stylesheet" type="text/css" href="../../../css/admin/card-analytics.css ">
<link rel="stylesheet" type="text/css" href="../../../css/admin/tour.css ">
<link rel="stylesheet" type="text/css" href="../../../css/admin/toastr.css">
<link rel="stylesheet" type="text/css" href="../../../css/admin/select2.min.css">



<link rel="stylesheet" type="text/css" href="../../../css/admin/pickadate.css">

<style type="text/css">
	th{
		text-transform: uppercase;
	}
</style>
