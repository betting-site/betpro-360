<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<!-- <script src="assets/js/jquery.js"></script> -->
<script src="{{asset('js/client/jquery-3.4.1.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('js/client/bootstrap.min.js')}}"></script>
<!-- owl carousel -->
<script src="{{asset('js/client/owl.carousel.js')}}"></script>
<!-- magnific popup -->
<script src="{{asset('js/client/jquery.magnific-popup.js')}}"></script>
<!-- filterizr js -->
<script src="{{asset('js/client/jquery.filterizr.min.js')}}"></script>
<!-- wow js-->
<script src="{{asset('js/client/wow.min.js')}}"></script>
<!-- clock js -->
<script src="{{asset('js/client/clock.min.js')}}"></script>
<script src="{{asset('js/client/jquery.appear.min.js')}}"></script>
<script src="{{asset('js/client/odometer.min.js')}}"></script>
<script src="{{asset('js/client/oddometer-active.js')}}"></script>
<!-- main -->
<script src="{{asset('js/client/main.js')}}"></script>
<script src="{{asset('js/client/toastr.js')}}"></script>


<script type="text/javascript">
// function googleTranslateElementInit() {
//   new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
// }
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}

$('.expert > p').click(function(){
    $('.expact').toggle('slow');
});
  // Social Media Collapse
  $('#sm-close').click(function(){
      $('.s-media').addClass('sm-collapse');
      $('#sm-open').delay(300).css('left', '0');
  });

  $('#sm-open').click(function(){
      $('#sm-open').css('left', '-60px');
      $('.s-media').removeClass('sm-collapse');
        
  });
  
// $('#pay').hide();

$('#payment_type').change(function(e){
  var type = this.value;
  var transfer = '';

  // $('#pay').show();
  if(type == 'transfer'){
    $('.pay, #gateway, .logo').hide();

    //show info on the page
    transfer+='<div class="row"><div class="col-12"><div class="alert alert-info alert-dismissible fade show" role="alert"><strong>Payment Info</strong> You are to pay to the following account number Zenith bank ......</div></div></div>';

  }else{
    $('.pay, #gateway, .logo').show();
  }

  $('.transfer').html(transfer);

});



$('#plan_duration').change(function(e) {
  var plan_duration = this.value;
  var plan_id = $("#plan_id").val();
  

  $.ajax({
      // type:'post',
      url: "/search",
      dataType: 'json',
      data: {plan_id: plan_id, plan_duration: plan_duration},
      beforeSend: function(data) {
        $('#submit_pay').html('<i class="fa fa-spinner"></i> ');
      },   
      success: function (data) {
        var price = '';

        if (data.status == 'true') {
          toastr.success(data.message);

          $('#submit_pay').html('Pay Now').removeAttr('disabled');

          price +='<div class="form-group"><label for="plan" class="col-form-label text-md-right">Plan price</label><input type="text" class="form-control" name="price" id="price" value='+ data.price +' required autocomplete="pricing_id" readonly ></div>';

          
            
        } else {
            $('#submit_pay').html('Pay Now');
            toastr.error(data.message);
            price +='<div class="form-group"><label for="plan" class="col-form-label text-md-right">Plan price</label><input type="text" class="form-control" name="price" id="price" value='+ data.price +' required autocomplete="pricing_id" readonly ></div>';
        }

        $('.price').html(price);
    }
  });
});


$(".number").click(function(e) {
  var plan_id = $(this).attr('href');
  
  $.ajax({
      // type:'post',
      url: "/plan/feature",
      dataType: 'json',
      data: {plan_id: plan_id},  
      success: function (data) {
        var features = '';

        if (data != '') {

          data.forEach(result);

            function result(item) {

              features +='<div class="col-lg-6"><i class="fa fa-futbol-o"></i><a href="/match/'+item.id+'"><span class="pb-2">'+item.title+'</span></a></div>';
            }
        }

        $('.feature').html(features);
    }
  });
});


</script>