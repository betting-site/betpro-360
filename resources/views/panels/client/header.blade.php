<style>
    .bg-header{
        
        background: #a2a1a121;
    }

    .dropdown-menu{
        min-width: 60rem !important;
    }
    
    .section{
        padding-right: 0px;
        padding-left: 0px;
    }

    .navbar-nav > .nav-item > a.nav-link:hover{

        background: #a2a1a142;
    }

    .dropdown-menu{
        background: #aaa;
    }

    .h5{
        color:#000;
    }


</style>
<!-- header begin -->
    <div class="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-sm-6">
                        <div class="left-area">
                            <ul>
                                <li>
                                    <span class="icon">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                    <span class="text">
                                        <span id="date"></span>
                                        <span id="month"></span>
                                        <span id="year"></span>
                                    </span>
                                </li>
                                <li>
                                    <span class="icon">
                                        <i class="far fa-clock"></i>
                                    </span>
                                    <span class="text clocks">
                                        <span id="hours"></span>:<span id="minutes"></span>:<span id="seconds"></span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-sm-6">
                        <div class="right-area">
                            <ul>
                                <!-- <li>
                                    <div id="google_translate_element"></div>
                                </li> -->
                                @if (Route::has('login'))
                                    @auth
                                        <li>

                                            <a class="link" href="{{ route('dashboard') }}">
                                            
                                            <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="user-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" class="svg-inline--fa fa-user-circle fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm128 421.6c-35.9 26.5-80.1 42.4-128 42.4s-92.1-15.9-128-42.4V416c0-35.3 28.7-64 64-64 11.1 0 27.5 11.4 64 11.4 36.6 0 52.8-11.4 64-11.4 35.3 0 64 28.7 64 64v13.6zm30.6-27.5c-6.8-46.4-46.3-82.1-94.6-82.1-20.5 0-30.4 11.4-64 11.4S204.6 320 184 320c-48.3 0-87.8 35.7-94.6 82.1C53.9 363.6 32 312.4 32 256c0-119.1 96.9-216 216-216s216 96.9 216 216c0 56.4-21.9 107.6-57.4 146.1zM248 120c-48.6 0-88 39.4-88 88s39.4 88 88 88 88-39.4 88-88-39.4-88-88-88zm0 144c-30.9 0-56-25.1-56-56s25.1-56 56-56 56 25.1 56 56-25.1 56-56 56z" class=""></path></svg>
                                                Dashboard
                                            </a>
                                        </li>

                                        <li>

                                            <a class="link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sign-out" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z" class=""></path></svg>
                                                LogOut
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                     @else
                                        <li>
                                            <a class="link" href="{{ route('login') }}">
                                                <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="dolly" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-dolly fa-w-18 fa-fw fa-2x"><path fill="currentColor" d="M575.6 309.8l-5.1-15.2c-1.4-4.2-5.9-6.5-10.1-5.1L526 301.1 451.5 77.9c-2.7-8.1-8.4-14.7-16-18.5-7.7-3.8-16.3-4.4-24.4-1.7L176.3 136l-18-50.7C146.9 53.3 116.7 32 82.8 32H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h74.8c20.3 0 38.4 12.8 45.2 31.9l96.7 271.9c-23.3 17.2-37.2 46.5-31.4 78.9 5.6 31.3 30.4 57 61.5 63.5 51.7 10.7 97.2-28.4 97.2-78.2 0-13.1-3.4-25.2-9-36.1l227.5-76c4.2-1.4 6.5-5.9 5.1-10.1zM187 166.1l106.4-35.5 25 74.9c1.4 4.2 5.9 6.5 10.1 5.1l15.2-5.1c4.2-1.4 6.5-5.9 5.1-10.1l-25-74.9L421.2 88l74.5 223.3-174.4 58.1C307.6 358.7 290.7 352 272 352c-6.3 0-12.3.9-18.1 2.3L187 166.1zM272 480c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48z" class=""></path></svg>
                                                Login
                                            </a>
                                        </li>
                                        @if (Route::has('register'))
                                            <li>
                                                <a class="link" href="{{ route('register') }}">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="user-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" class="svg-inline--fa fa-user-circle fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm128 421.6c-35.9 26.5-80.1 42.4-128 42.4s-92.1-15.9-128-42.4V416c0-35.3 28.7-64 64-64 11.1 0 27.5 11.4 64 11.4 36.6 0 52.8-11.4 64-11.4 35.3 0 64 28.7 64 64v13.6zm30.6-27.5c-6.8-46.4-46.3-82.1-94.6-82.1-20.5 0-30.4 11.4-64 11.4S204.6 320 184 320c-48.3 0-87.8 35.7-94.6 82.1C53.9 363.6 32 312.4 32 256c0-119.1 96.9-216 216-216s216 96.9 216 216c0 56.4-21.9 107.6-57.4 146.1zM248 120c-48.6 0-88 39.4-88 88s39.4 88 88 88 88-39.4 88-88-39.4-88-88-88zm0 144c-30.9 0-56-25.1-56-56s25.1-56 56-56 56 25.1 56 56-25.1 56-56 56z" class=""></path></svg>
                                                    Register
                                                </a>
                                            </li>
                                        @endif
                                    @endauth
                                @endif
                                <li>
                                    <a class="link" href="{{ route('help') }}">
                                        <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="user-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" class="svg-inline--fa fa-user-circle fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm128 421.6c-35.9 26.5-80.1 42.4-128 42.4s-92.1-15.9-128-42.4V416c0-35.3 28.7-64 64-64 11.1 0 27.5 11.4 64 11.4 36.6 0 52.8-11.4 64-11.4 35.3 0 64 28.7 64 64v13.6zm30.6-27.5c-6.8-46.4-46.3-82.1-94.6-82.1-20.5 0-30.4 11.4-64 11.4S204.6 320 184 320c-48.3 0-87.8 35.7-94.6 82.1C53.9 363.6 32 312.4 32 256c0-119.1 96.9-216 216-216s216 96.9 216 216c0 56.4-21.9 107.6-57.4 146.1zM248 120c-48.6 0-88 39.4-88 88s39.4 88 88 88 88-39.4 88-88-39.4-88-88-88zm0 144c-30.9 0-56-25.1-56-56s25.1-56 56-56 56 25.1 56 56-25.1 56-56 56z" class=""></path></svg>
                                        Help
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="navbar" class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-6 d-xl-block d-lg-block d-flex align-items-center">
                                <div class="logo">
                                    <a href="{{ route('home') }}">
                                        <img src="{{asset('/images/client/logo.png')}}" alt="logo">
                                    </a>
                                </div>
                            </div>
                            <div class="col-6 d-xl-none d-lg-none d-block">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9">
                        <div class="mainmenu">
                            <nav class="navbar navbar-expand-lg">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item {{ Request::is('home') ? 'bg-header' : '' }}">
                                            <a class="nav-link" href="{{ route('home') }}">
                                              Home
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown {{ Request::is('category') ? 'bg-header' : '' }}">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Category
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                <div class="row">
                                                    <?php 

                                                        $dbfree = App\Category::where('plan_id', '1')->get();

                                                        $dbsilver = App\Category::where('plan_id', '1,2')->get();

                                                        $dbgold = App\Category::where('plan_id', '1,2,3')->get();

                                                    ?>
                                                    
                                                    <div class="col-md-2 section ml-3">
                                                        <p class="h5 dropdown-item "> Free Plan </p>
                                                        @foreach($dbfree as $free)
                                                            <a class="dropdown-item" href="{{route('login')}}">{{ucwords(strtolower($free->title))}}</a>
                                                        @endforeach
                                                    </div>

                                                    <div class="col-md-2 section ml-3 mr-5">
                                                        <p class="h5 dropdown-item"> Silver Plan </p>
                                                        @foreach($dbsilver as $silver)
                                                            <a class="dropdown-item" href="{{route('login')}}">{{ucwords(strtolower($silver->title))}}</a>
                                                        @endforeach
                                                    </div>


                                                    <div class="col-md-2 section ml-3 mr-3">
                                                        <p class="h5 dropdown-item"> Gold Plan </p>
                                                        @foreach($dbgold as $gold)
                                                            <a class="dropdown-item" href="{{route('login')}}">{{ucwords(strtolower($gold->title))}}</a>
                                                        @endforeach
                                                    </div>

                                                    <div class="col-md-2 section ml-3">
                                                        <a href="{{route('login')}}" class="h5 dropdown-item"> Investment Plan </a>

                                                        <a href="{{route('login')}}" class="h5 dropdown-item"> Expert Advisor </a>


                                                        <a href="{{route('login')}}" class="h5 dropdown-item">Bet RollOver  </a>

                                                        <a href="{{route('login')}}" class="h5 dropdown-item">Twitter VIP </a>

                                                        <a href="{{route('login')}}" class="h5 dropdown-item">Telegram Channel </a>

                                                    </div>

                                                </div>
                                            </div>
                                                    
                                        </li>
                                        <li class="nav-item {{ Request::is('bet') ? 'bg-header' : '' }}">
                                            <a class="nav-link" href="{{ route('bet') }}">Bet Market</a>
                                        </li>

                                        <li class="nav-item {{ Request::is('pricing') ? 'bg-header' : '' }}">
                                            <a class="nav-link" href="{{ route('pricing') }}">Pricing Plans</a>
                                        </li>

                                        


                                        <li class="nav-item {{ Request::is('help') ? 'bg-header' : '' }}">
                                            <a class="nav-link" href="{{ route('help') }}">Help</a>
                                        </li>

                                        <li class="nav-item {{ Request::is('terms') ? 'bg-header' : '' }}">
                                            <a class="nav-link" href="{{ route('terms') }}">T/Cs</a>
                                        </li>

                                        <li class="nav-item {{ Request::is('contact') ? 'bg-header' : '' }}">
                                            <a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                              </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- header end -->