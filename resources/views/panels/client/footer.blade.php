 <!-- footer begin -->
 <style>
    .footer-links {
        color: #fff;
        text-align: center;
        margin: 25px auto 15px;
    }
    .footer-links li {
        display: inline;
        margin: 20px;
    }

    .footer-links li a {
        color: #fff;
    }

    .s-media{
        position: fixed;
        top: 100px ;
        left: 0;
        z-index: 1000;
        transition:all linear 0.2s ;
    }

    .s-media a:first-child{
        border-radius: 0 5px 0 0;
    }

    .s-media a:last-child{
        border-radius: 0 0 5px 0;
    }

    .s-item {
        display:block;
        width: 30px;
        height: 30px;
        color: white;
        font-size: 15px;
        line-height: 30px;
        text-align: center;

        transition:all linear 0.2s ;
    }

    .s-item:hover {
        width:35px;
        
    }

    #sm-open {
        position: fixed;
        top: 50px ;
        left: -30px;
        border-radius:0 15px 15px 0;
        
        transition:all linear 0.2s ;
    }

    .facebook {background-color: #305891;}
    .twitter {background-color: #3AAFD6;}
    .gplus {background-color: #F8694D; }
    .print {background-color: #7CB0A4;}
    .sm-collapse{left: -30px; }

 </style>
 
    <div class="footer" id="contact">
        <div class="container">
            <div class="row text-center">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="about-widget">
                        <a class="logo" href="/">
                            <img src="{{asset('/images/client/logo_white.png')}}" alt="">
                        </a>
                    </div>
                </div>
                <!-- <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <h3 class="mb-2 pb-2 text-white">DISCLAIMER</h3>
                            <p class="pb-2 justify-content-between text-justify text-white">
                                Please be informed that there will be no refund. By opting to receive soccer predictions and tips, you accept that all BetPro360's predictions and tips are for informational purposes only and that BetPro360 will take no responsibility for any losses incurred by you, the subscriber, as a direct result of acting upon received BetPro360's information. We do not encourage gambling in any sort of form. For full terms and conditions, please refer to "TERMS AND CONDITIONS".
                            </p>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <ul class="footer-links">
                                <li>
                                    <a href="{{ route('terms') }}">TERMS &amp; CONDITION</a>
                                </li>
                                <li>
                                    <a href="{{ route('category') }}">BET MARTKET</a>
                                </li>
                                <li>
                                    <a href="{{ route('contact') }}">PLACE ADVERT</a>
                                </li>
                                <li>
                                    <a href="{{ route('offers') }}">OFFERS</a>
                                </li>
                            </ul>
                            <ul class="footer-links">
                                <li>
                                    <a href="{{ route('help') }}">FAQ</a>
                                </li>
                                <li>
                                    <a href="{{ route('cookies') }}">COOKIES</a>
                                </li>
                                <li>
                                    <a href="{{ route('refund') }}">REFUND POLICY</a>
                                </li>
                                <li>
                                    <a href="{{ route('privacy') }}">PRIVACY</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 about-widget">
                            <div class="support text-center">
                                <ul class="footer-links">
                                    <li>
                                        SMS Only: +2348166891966
                                    </li>
                                    <li>
                                        Email: contact@betpro360.com
                                    </li>
                                    <li>
                                        Skype: contact@betpro360.com
                                    </li>
                                    <li>
                                        Telegram: @BetPro_360
                                    </li>
                                    <li>
                                        <a href="https://wa.me/2348166891966">Whatsapp: +2348166891966
                                    </li>
                                </ul>
                                <!-- <ul>
                                    <li>
                                        <span class="icon">
                                            <img src="{{asset('/images/client/svg/email.svg')}}" alt="">
                                        </span>
                                        <span class="text">
                                            <span class="title">Mail Us</span>
                                            <span class="descr">marketing@betpro360.com</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="icon">
                                            <img src="{{asset('/images/client/svg/phone-call.svg')}}" alt="">
                                        </span>
                                        <span class="text">
                                            <span class="title">Get In Touch</span>
                                            <span class="descr">+234 816 689 1966</span>
                                        </span>
                                    </li>
                                </ul> -->
                            </div>
                            <div class="social text-center"> 
                                <p>Follow Us On</p>
                                <ul style="list-style: none">
                                    <li>
                                        <a href="https://web.facebook.com/BetPro360/" class="social-icon">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a href="https://twitter.com/BetPro_360" class="social-icon">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                        <a href="https://www.instagram.com/betpro_360/" class="social-icon">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                        <!-- <a href="https://www.pinterest.com/BETPRO360/" class="social-icon">
                                            <i class="fab fa-pinterest-p"></i>
                                        </a> -->
                                        <a href="https://wa.me/08166891966" class="social-icon">
                                            <i class="fab fa-whatsapp"></i>
                                        </a>
                                        <a href="t.me/BetPro_360" class="social-icon">
                                            <i class="fab fa-telegram-plane"></i>
                                        </a>
                                        <a href="https://www.youtube.com/channel/UCsHODFJ8Dr7xwNbpl7TwqXA?view_as=subscriber" class="social-icon">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                        
                                    </li>
                                </ul>
                            </div>
                            <div class="payment-method">
                                <h6 class="payment-method-title">
                                    Payment methods are We accept 
                                </h6>
                                <div class="all-method">
                                    <div class="single-method">
                                        <img src="{{asset('/images/client/brand-1.png')}}" alt="">
                                    </div>
                                    <div class="single-method">
                                        <img src="{{asset('/images/client/brand-2.png')}}" alt="">
                                    </div>
                                    <div class="single-method">
                                        <img src="{{asset('/images/client/brand-3.png')}}" alt="">
                                    </div>
                                    <div class="single-method">
                                        <img src="{{asset('/images/client/brand-4.png')}}" alt="">
                                    </div>
                                    <div class="single-method">
                                        <img src="{{asset('/images/client/brand-5.png')}}" alt="">
                                    </div>
                                    
                                    <div class="single-method">
                                        <img src="{{asset('/images/client/brand-5.png')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="s-media">
        <a href="/" class="s-item facebook">
            <span class="fa fa-facebook"></span>
        </a>
        
        <a href="/" class="s-item twitter">
            <span class="fa fa-twitter"></span>
        </a>
        

        <a href="/" class="s-item gplus">
            <span class="fa fa-google-plus"></span>
        </a>

        <a id="sm-close"  class="s-item print">
            <span class="fa fa-arrow-left text-white"></span>
        </a>

    </div>
    <a id="sm-open"  class="s-item print sm-collapse">
        <span class="fa fa-arrow-right text-white"></span>
    </a>
<!-- footer end -->

    <!-- copyright footer begin -->
    <div class="copyright-footer">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-5 col-md-6 col-lg-6 d-lg-flex d-lg-flex d-block align-items-center mb-4">
                    <p class="copyright-text">
                        <span style="border: 2px solid red; border-radius: 100%; padding: 8px; font-size: 18px;">18+</span> <strong>Be</strong>Gamble<strong>Aware</strong>
                    </p>
                </div>
                <div class="text-right col-md-6 col-xl-4 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                    <p class="copyright-text">
                        Copyright &copy; <?= date('Y') ?><a href="#">BetPro360.com</a> - All Rights Reserved
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright footer end -->