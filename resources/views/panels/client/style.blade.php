<link rel="stylesheet" href="{{asset('css/client/bootstrap.min.css')}}">
<!-- fontawesome icon  -->
<link rel="stylesheet" href="{{asset('css/client/fontawesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/client/fontawesomee.min.css')}}">
<!-- flaticon css -->
<link rel="stylesheet" href="{{asset('../css/fonts/flaticon.css')}}">
<!-- animate.css -->
<link rel="stylesheet" href="{{asset('css/client/animate.css')}}">
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{asset('css/client/owl.carousel.min.css')}}">
<!-- magnific popup -->
<link rel="stylesheet" href="{{asset('css/client/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('css/client/odometer.min.css')}}">
<!-- stylesheet -->
<link rel="stylesheet" href="{{asset('css/client/style.css')}}">
<link rel="stylesheet" href="{{asset('css/client/toastr.min.css')}}">
<!-- responsive -->
<link rel="stylesheet" href="{{asset('css/client/responsive.css')}}">
<link rel="stylesheet" href="{{asset('css/client/register-page-responsive.css')}}">
<link rel="stylesheet" href="{{asset('css/client/dashboard-responsive.css')}}">
<link rel="stylesheet" href="{{asset('css/client/result-page-responsive.css')}}">