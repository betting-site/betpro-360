@extends('layouts.client.app')

@section('content')
    <!-- breadcrumb begin -->
    <div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->

    <!-- login begin -->
    <div class="login">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-8">
                    <div class="section-title">
                        <h2>Login To Place bets</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="login-form">
                        
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group">

                                <div class="input-group">

                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>

                                    <input id="user_id" type="text" class="form-control @error('user_id') is-invalid @enderror " name="user_id" value="{{ old('email') ?: old('user_id') }}" placeholder="E-Mail Address or Phone Number" required autocomplete="email">

                                    @error('user_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="input-group">

                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </div>


                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  placeholder="Password" required autocomplete="new-password">

                                   

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                             @if (session('status'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error ! </strong> {{ session('status') }}
                                </div>
                            @endif

                            <div class="form-group row mb-4">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mx-auto d-block">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>

                            <p class="mt-4 text-center"><a class="text-primary" href="/password/reset">Forgot your password ? </a></p>
                            
                            <p class="text-center"> <a class="text-primary" href="{{asset('contact')}}">Having problems trying to login ? Contact US </a></p>
                        </form>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- login end -->

@endsection
