@extends('layouts.client.app')

@section('content')


<!-- breadcrumb begin -->
    <div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
    <div class="login">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-8">
                    <div class="section-title">
                        <h2>Register To Predict Now</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-7">
                    <div class="login-form">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <!-- <label for="name" class="col-form-label text-md-right">{{ __('Firstname') }}</label>
 -->
                                        <div class="input-group">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-user"></i>
                                                </span>
                                            </div>

                                            <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required placeholder="Firstname" autocomplete="firstname" autofocus>

                                            @error('firstname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-6">

                                    <div class="form-group">

                                        <!-- <label for="lastname" class="col-form-label text-md-right">{{ __('Lastname') }}</label> -->

                                        <div class="input-group">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-user"></i>
                                                </span>
                                            </div>

                                            <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required placeholder="Lastname" autocomplete="lastname" autofocus>

                                            @error('lastname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                

                                <div class="col-xl-6 col-lg-6 col-md-6">

                                    <div class="form-group">

                                        <!-- <label for="email" class="col-form-label text-md-right">{{ __('Email Address') }}</label> -->

                                        <div class="input-group">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-envelope"></i>
                                                </span>
                                            </div>

                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="Email Address" autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-xl-6 col-lg-6 col-md-6">

                                    <div class="form-group">

                                        <!-- <label for="phone" class="col-form-label text-md-right">{{ __('Phone Number') }}</label> -->

                                        <div class="input-group">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>

                                            <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" required placeholder="Phone Number" autocomplete="phone_number">

                                            @error('phone_number')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <div class="form-group">

                                        <!-- <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label> -->

                                        <div class="input-group">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                            </div>

                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password" autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    
                                    <div class="form-group ">
                                        <!-- <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label> -->

                                        <div class="input-group">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                            </div>

                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password" autocomplete="new-password">
                                        </div>
                                    </div>
                                </div>
                            </div>  


                            <p class="mb-4 mt-4"> By clicking Register, you agree to our <a class="text-primary" href="{{ route('terms') }} ">Terms of Use</a> and our <a class="text-primary" href="{{ route('privacy') }}">Privacy Policy.</a> </p>

                            <div class="form-group row mb-4">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mx-auto d-block">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                            <p class="mb-2 mt-4 text-center"> Already have an account ? <a class="text-primary" href="{{ route('login') }}" >  {{ __('Login') }} </a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
