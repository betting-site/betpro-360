<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'BetPro360') }}</title>
        <link rel="shortcut icon" href="{{ asset('images/client/icon.png') }}" type="image/icon type">

        @include('panels.admin.style')

        
    </head>
    <style>
        nav{
        display: inline-block !important; 
    }
    textarea{
        resize: none !important;
    }
    </style>

    <body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
        @include('panels.admin.sidebar')

        <div id="app" class="app-content content">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>

            @include('panels.admin.navbar')

            <div class="content-wrapper">

                <!-- <div class="content-body"> -->
                    @yield('content')

                <!-- </div> -->

            </div>

        </div>
        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        @include('panels.admin.footer')

        @include('panels.admin.script')

        @yield('footer')
        
        @yield('scripts')
    </body>
</html>
