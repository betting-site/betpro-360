<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BetPro360') }}</title>
     <link rel="shortcut icon" href="{{ asset('images/client/icon.png') }}" type="image/icon type">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    @include('panels.admin.style')
</head>
<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" 
    data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

        <div id="app">

            <!-- BEGIN: Content-->
            <div class="app-content content">
                <div class="content-wrapper">
                    <div class="content-header row"></div>
                    <div class="content-body">
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- END: Content-->
            </div>

        @include('panels.admin.script')
        @yield('scripts')
    </body>
</html>
