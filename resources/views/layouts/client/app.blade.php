<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BetPro360') }}</title>
    <link rel="icon" href="{{ asset('images/client/icon.png') }}" type="image/icon type">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    @include('panels.client.style')
    @yield('style')
</head>
<body>
    @include('panels.client.preloader')
    @include('panels.client.header')
        <div>
            @yield('content')
        </div>

    @include('panels.client.footer')
    @include('panels.client.script')
    @yield('scripts')
    </body>
</html>
