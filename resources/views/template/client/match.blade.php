@extends('layouts.client.app')

@section('style')
<style type="text/css">
    
.page-link{
   padding: 1.2rem 1.5rem; 
   color: #111705; 
   font-weight: 600;
}

.page-item, .page-item.active .page-link{
    background: rgba(248, 27, 100, 0.08);
    color: rgba(42, 42, 42, 0.65);
    border: 1px solid rgba(248, 27, 100, 0.2);
}

</style>
@endsection

@section('content')
<!-- breadcrumb begin -->
    <div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

    <!-- payment begin -->
    <div class="user-panel-dashboard">   
        <div class="payment-history">
            <div class="container">
                @if($plan)
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="user-panel-title">
                            <h3>Match Details</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-lg-2"></div>
                    <div class="col-xl-8 col-lg-8">
                        <!-- <div class="transaction-area"> -->
                            <table class="table table-bordered">
                                <thead style="background:#01013F" class="text-white">
                                    <tr>
                                        <th>#</th>
                                        <th scope="col" class="text-center">{{$predictions->teams ? $predictions->teams : N/A}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>   
                                        <th>Date & Time</th>
                                        <th scope="row" class="text-center">{{$predictions->match_date ? $predictions->match_date : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>Home Form</th>
                                        <th scope="row" class="text-center">{{$predictions->home_form ?  $predictions->home_form : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>Home Odd</th>
                                        <th scope="row" class="text-center">{{$predictions->home_odd ? $predictions->home_odd : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>Away Form</th>
                                        <th scope="row" class="text-center">{{$predictions->away_form ?  $predictions->away_form : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>Away Odd</th>
                                        <th scope="row" class="text-center">{{$predictions->away_odd ? $predictions->away_odd : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>Draw</th>
                                        <th scope="row" class="text-center">{{$predictions->draw_odd ? $predictions->draw_odd : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>{{ucwords(strtolower($active->title))}}</th>
                                        <th scope="row" class="text-center">{{$active->recommendation ? $active->recommendation : 'N/A'}}</th>
                                    </tr>
                                    
                                    <tr>   
                                        <th>FT Recommendtion</th>
                                        <th scope="row" class="text-center">{{$predictions->ft_recommendation ? $predictions->ft_recommendation : 'N/A'}}</th>
                                    </tr>

                                    <tr>   
                                        <th>Preview</th>
                                        <th scope="row" class="text-center">{{$predictions->preview ? $predictions->preview : 'N/A'}}</th>
                                    </tr>



                                </tbody>
                            </table>
                        <!-- </div> -->
                    </div>
                    <div class="col-xl-2 col-lg-2"></div>
                </div>
                @else

                <div> 
                    <h5 class="text-center"> <a href="{{route('dashboard')}} ">Sorry you need to upgrade to access this page</a></h5>
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- payment end -->

@endsection
