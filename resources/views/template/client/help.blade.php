@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <!-- <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-8">
                <div class="section-title">
                    <h2>Faq</h2>
                </div>
            </div>
        </div> -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="login-form">
                    @foreach($questions as $key  => $ques)
                    <div class="row">
                        <div class="d-flex w-100 ">
                            <span class="mr-4 ml-4">{{$key+1}}.</span>
                            <h6 class="mb-2 pb-4 ">{{ $ques->questions }} </h6>
                        </div>
                        <p class="mb-2 pb-4 ml-4 justify-content-around text-justify">{{ $ques->answers }}</p> <hr>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
