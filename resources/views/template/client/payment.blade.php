@extends('layouts.client.app')

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('css/client/toastr.css')}}">
<style>
    .logo{
        width: 60px;
        height: 60px;
    }
    #pay{
        display: none
    }
</style>
@endsection

@section('content')


    <!-- user panel header begin -->
    <div class="user-panel-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">

                    <div class="user-face">
                        <div class="part-left">
                            <div>
                                <div class="part-pic">

                                    @if(empty(Auth::user()->avatar))
                                    <img src='{{ asset("images/client/avatar.png") }}' class="avatar" alt="user imag">
                                    @else
                                    <img src='{{ asset("storage/".Auth::user()->avatar)}}' class="avatar" alt="user image">
                                    @endif
                                </div>
                            </div>
                            <div class="part-name">
                                <span class="welcome-text">Welcome</span> 
                                <span class="user-name-text"> {{ Auth::user()->firstname }}</span>
                                <h6 class="text-white"> {{ Auth::user()->user_id}}</h6>
                            </div>
                        </div>
                        <div class="part-right">
                            <ul>
                                
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sign-out" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z" class=""></path></svg>
                                        <span class="text">Log Out</span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12">
                    <div class="dashboard-menu">
                        <ul>
                            <li>
                                <a href="{{route('dashboard')}}" class="menu-item">
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{route('prediction')}}" class="menu-item">
                                    Predictions
                                </a>
                            </li>
                            <li>
                                <a href="{{route('transaction')}}" class="menu-item">
                                   Transactions
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('update')}}" class="menu-item">
                                   Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- user panel header end -->

    <!-- payment begin -->
    <div class="user-panel-dashboard">   
        <div class="login" style="padding-top: 0">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-8 col-lg-8">
                        <div class="login-form">
                            <form method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-6">
                                        
                                        <div class="form-group">
                                            <label for="name" class="col-form-label text-md-right">{{ __('Plan Type') }}</label>
                                            
                                            <select class="form-control @error('plan_type') is-invalid @enderror " name="plan_type" required>
                                                <option value="{{$plans->id}}">{{$plans->name}}</option>
                                            </select>
                                            
                                            <input type="hidden" value="{{$plans->id}}" id="plan_id">

                                            <input type="hidden" value="{{Auth::user()->email}}" id="email">

                                            <input type="hidden" value="{{Auth::user()->phone_number}}" id="phone_number">

                                            @error('plan_type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        
                                        <div class="form-group">
                                            <label for="name" class="col-form-label text-md-right">{{ __('Country') }}</label>
                                            
                                            <select class="form-control " name="" required>
                                                <option value="{{ $country->id }}" >{{ $country->name }} </option>
                                            </select>
                                            
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="plan price" class="col-form-label text-md-right">{{ __('Plan Duration') }}</label>

                                            <select class="form-control @error('plan_duration') is-invalid @enderror" name="plan_duration" id="plan_duration" required autofocus>
                                                <option value="">-Select Plan Duration-</option>
                                                @foreach($duration as $durtn)
                                                <option value="{{$durtn->duration_type}}">{{$durtn->title}}</option>
                                                @endforeach
                                            </select>

                                            @error('plan_duration')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-6 price"></div>

                                    

                                    <div class="col-6">
                                        
                                        <div class="form-group">
                                            <label for="name" class="col-form-label text-md-right">{{ __('Payment type') }}</label>
                                            
                                            <select class="form-control @error('payment_type') is-invalid @enderror " name="payment_type" id="payment_type" required>
                                                <option value="cash" >Cash</option>
                                                <option value="transfer">Transfer</option>
                                            </select>
                                            
                                            @error('payment_type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    

                                    <div class="col-6">
                                    
                                        <div class="form-group" id="gateway">
                                            <label for="name" class="col-form-label text-md-right">{{ __('Gateway') }}</label>

                                            <select class="form-control @error('gateway_name') is-invalid @enderror " name="gateway_name" required>
                                                <option value="paystack" >Paystack</option>
                                                <!-- <option value="wave">Flutterwave</option> -->
                                            </select>

                                            @error('gateway_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="col-12 transfer"></div>


                                <div class="col-12">
                                    <div class="row justify-content-center">
                                        <div>
                                            <img src="{{asset('/images/client/flutterwave-logo.png')}}" class="logo  mb-4 mt-4 mr-2">
                                        </div>
                                        <div>
                                            <img src="{{asset('/images/client/paystack-logo.png')}}" class="logo  mb-4 mt-4 mr-2">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row mb-0">
                                    <div class="pay col-12 ">
                                        <button type="button" id="submit_pay" class="btn btn-primary mx-auto d-block" disabled >
                                            {{ __('Pay Now') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- payment end -->

@endsection

@section('scripts')
<script src="{{asset('js/client/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('js/client/toastr.js')}}"></script>
<script src="https://js.paystack.co/v1/inline.js"></script> 
<script src="https://checkout.flutterwave.com/v3.js"></script> 

<script>
    $(document).ready(function(){
        

        $('#submit_pay').on('click', function(e) {
            e.preventDefault();
            var public_key = 'pk_test_34b84e2071f1f5b5698d76da2432c9f66ab2de97';
            var amount_paid = $('#price').val();
            var email = $('#email').val();
            var phone_number = $('#phone_number').val();
            var gateway_name = $('#gateway option:selected').val();
            var payment_type = $('#payment_type option:selected').val();
            var transaction_ref = 'TC-'+ Math.floor((Math.random() * 1000000000) + 1);
            var currency = 'NGN';
            var plan_id = $("#plan_id").val();
            var plan_duration = $('#plan_duration option:selected').val();


            if(gateway_name == 'wave'){
                (function payWithFlutter() {
                    FlutterwaveCheckout({
                        public_key: "FLWPUBK_TEST-b0a04f29f9f9428c4190e79ce8dc8696-X",
                        amount: parseInt(amount_paid),
                        currency: 'NGN',
                        tx_ref: transaction_ref,
                        customer: {
                            email: email,
                            phone_number: phone_number,
                        },
                        callback: function (response) {
                            
                            $.ajax({
                                type: "POST",
                                url: '/payment/process/',
                                dataType: 'json',
                                data: { gateway_name: gateway_name, payment_type: payment_type, email : email, plan_duration: plan_duration, phonenumber: phone_number, plan_id: plan_id, amount_paid: amount_paid, currency: currency, transaction_ref: transaction_ref, "_token": "{{ csrf_token() }}"
                                    },
                                success: function (data) {
                                if (data.status === 'success') {
                                    // console.log(data);
                                        toastr.success(data.message);
                                        // window.location="/dashboard";
                                    } else {
                                       toastr.error(data.message);
                                    }
                                }
                            })
                        },
                        customizations: {
                            title: "BETPRO360",
                            description: "Payment for Plan",
                            logo: "https://assets.piedpiper.com/logo.png",
                        }
                    })
                })();

            }else{

                (function handleInitialPayment() {
                    $.ajax({
                        cache: false,
                        type: "POST",
                        url: '/payment/process',
                        dataType: 'json',
                        data: { gateway_name: gateway_name, payment_type: payment_type, email : email, plan_duration: plan_duration, phonenumber: phone_number, plan_id: plan_id, amount_paid: amount_paid, currency: currency, transaction_ref: transaction_ref, "_token": "{{ csrf_token() }}"
                        },
                    
                        success: function (data) {
                            toastr.clear();

                            if (data.status == 'success') {
                            
                                amount_paid = data.amount; email = data.email; transaction_ref = data.transaction_ref; last_id = data.last_id;

                                if(gateway_name == 'wave'){
                                    
                                    payWithFlutter(amount_paid, email, transaction_ref)
                                }else{
                                    
                                    payWithPaystack(amount_paid, email, transaction_ref, last_id);
                                }


                            } else {

                                toastr.error(data.message);
                                toastr.error('An error occured while processing your request')
                                return;
                            }
                        }
                    })
                })();   
            }
        })



        function payWithPaystack(amount_paid, email, transaction_ref){
            var handler = PaystackPop.setup({
            key: 'pk_test_34b84e2071f1f5b5698d76da2432c9f66ab2de97',
            email: email,
            amount: parseInt(amount_paid * 100),
            currency: 'NGN',
            ref: transaction_ref,
            callback: function(response){
                $.ajax({
                    type: "GET",
                    url: '/payment/process/update/',
                    dataType: 'json',
                    data: {transaction_response : response.reference, last_id : last_id, "_token": "{{ csrf_token() }}"},
                    success: function (data) {
                    if (data.status === 'success') {
                        // console.log(data);
                            toastr.success(data.message);
                            window.location="/dashboard";
                        } else {
                           toastr.error(data.message);
                        }
                    }
                })
            },
                onClose: function(){
                    alert('window closed');
                }
            });
            handler.openIframe();
        }


       

    });
</script>
@endsection