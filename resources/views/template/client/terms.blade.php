@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <!-- <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-8">
                <div class="section-title">
                    <h2>Terms and Conditions</h2>
                </div>
            </div>
        </div> -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="login-form">
                    <div class="part-text">
                        <h4 class="mb-2 mt-2">Terms and Conditions </h4>
                        <h6 class="mb-2 mt-2"> Introduction </h6>
                        <p class="text-justify">
                            Visiting our site betpro360.com and subscribing to any of our offers means that you have agreed to  with:
                            The content of these Terms and Conditions
                            <ul class="mb-4 ml-4">
                                <li>
                                    Our Cookies Policy
                                </li>
                                <li>
                                    The content of our Privacy Policy
                                </li>
                                <li>
                                    Industry rules and regulations in line with our products and services
                                </li>
                            </ul>
                        </p>
                        <p class="text-justify">
                            It is important to note that reading the content of these "Terms and Conditions" is very
                            important. If you do not agree with the content therein, it's advisable not to use our site.
                            The content of this T&C is subject to changes. These changes might come up as a result of
                            regulatory requirements, product upgrades, and customers satisfaction metrics.
                            You will be adequately informed and advised before any change will be implemented on the
                            content of the T&C.
                            One of our core objectives is to improve the user experience at all times and to maintain
                            awesome user satisfaction.
                            This means that we will at all times strive to help you minimize risks to its lowest point while
                            also giving you a reasonable guarantee on our products and services.
                            We are also in full support of responsible gambling, hence, our 18+ policy stands. We will not
                            take responsibility for any liability arising from any user who violates these rules and policies.
                        </p>
                        <h6 class="mb-2 mt-2 font-weight-bold"> Account </h6>
                        <h6 class="mb-2 mt-2 ">Signing up </h6>
                        <p class="text-justify">
                        The most important criterion for signing up and using our platform is the fact that you must be 18+ as at the time of registering with us.
                        To comply with this policy, we have the right to demand proof of age from you and keep your account non-operational until you provide satisfactory documents that support your claims.
                        We can do email verification and identifications either at the point of registering or when we deem it fit.
                        For the email verifications, it is mostly done at the point of registering.
                        Our one account policy means that you can only have one account with us. If during our verification and identification exercise, we found out that you are running more than one account with us, we have the right to outright delete one of such accounts.  
                        </p>
                        <h6 class="mb-2 mt-2"> Personal details </h6>
                        <p class="text-justify">
                            All users have the right to select username and password as they so wish.
                            In the event that you feel your account has been compromised, you can make use of the forgot password option to update your password.
                            The safety and privacy of your account are important, hence it's important to keep it that way. This is why our privacy policy is as important as the T&C.
                        </p>
                        <h6 class="mb-2 mt-2"> Prediction and Picks </h6>
                        <p class="text-justify">
                            Prediction is our core and that's why we are the best at it. We give our clients one of the best guarantees on our prediction by making sure that the risks are reduced drastically to the lowest minimum.
                            While taking the risk to the lowest minimum is great for our users, we do not in any way take full responsibility for the outcomes of games. As this is beyond our control.
                            Analysis and superb predictions are one part of a bet, the other part rests on the shoulders of the users. Hence, your discretion is key to winning as well.
                        </p>
                        <h6 class="mb-2 mt-2"> Prediction and Picks </h6>
                        <p class="text-justify">
                            Others
                            BetPro360 reserve the right to:
                            <ul class="mb-4 ml-4">
                                <li>
                                    Restrict access to the website at its own condition.
                                </li>
                                <li>
                                    Improve or change the modus operandi of an offer when it feels there is a need for that.
                                </li>
                                <li>
                                    All the intellectual property on the website.
                                </li>
                            </ul>
                        </p>
                        <p class="text-justify">

                            It's very important to note that the content of these Terms and Conditions, the Privacy Policy, the Cookies
                            Policy, the Refund Policy, and the rules and regulations guiding this industry are the most vital
                            documents that stand as an agreement between all the parties involved.
                            Agreeing to these documents indicates your willingness to comply with its content at every time.
                            Also, it is worthy to note that these documents supersede any other information you might receive from
                            any source.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
