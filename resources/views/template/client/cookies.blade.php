@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <!-- <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-8">
                <div class="section-title">
                    <h2>Cookies Policy</h2>
                </div>
            </div>
        </div> -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="login-form">
                    <div class="part-text">
                        <h4 class="mb-2 mt-2">Cookies Policy </h4>
                            <p class="text-justify">
								Cookies are a small piece of data sent from a website and stored on the user's computer by the web browser while the user is browsing. It enables us to show you personalized content when you visit the website. They are widely used to “remember” you and your preferences, either for a single visit (through a “session cookie”) or for multiple repeat visits (using a “persistent cookie”). They ensure a consistent and efficient experience for visitors, and perform essential functions such as allowing users to register and remain logged in.
                            </p>
                            <h6 class="mb-2 mt-2">Our cookies policy </h6>
                            <p class="text-justify">
                                To make full use of this website, and your device computer, tablet or mobile ("device") when you browse the Internet you should accept cookies as much online functionality has been developed specifically to only work via the use of cookies. It is important to note that cookies placed via this website do not store any of your personal or sensitive information (such as your name, address or payment details).
                                In order to make the information on cookies in this policy clearer for you, we have split the cookies into 5 categories
                            </p>
                            <h6 class="mb-2 mt-2">Performance cookies </h6>
                            <p class="text-justify">
                                These are cookies used specifically for gathering data on how visitors use a website, which pages of a website are visited most often, or if they get error messages on web pages. These cookies monitor only the performance of the site as the user interacts with it. These cookies don’t collect identifiable information on visitors, which means all the data collected is anonymous and only used to improve the functionality of a website.
                            </p> 
                            <h6 class="mb-2 mt-2">Functionality Cookies </h6>
                            <p class="text-justify">   
                                Functionality cookies allow websites to remember the user’s site preferences and choices they make on the site including username, region, and language. This allows the website to provide personalized features
                            </p>
                            <h6 class="mb-2 mt-2">Essential Cookies </h6>
                            <p class="text-justify">
                                These are cookies that are either:  used solely to carry out or facilitate the transmission of communications over a network; or  strictly necessary to provide an online service (e.g. our website or a service on our website) which you have requested.
                            </p>
                            <h6 class="mb-2 mt-2">Non-essential cookies</h6>
                            <p class="text-justify"> 
                                These are any cookies that do not fall within the definition of essential cookies, such as cookies used to analyse your behaviour on a website (‘analytical’ cookies) or cookies used to display advertisements to you (‘advertising’ cookies).
                            </p>
                            <h6 class="mb-2 mt-2">How do I disable/enable cookies?</h6>
                            <p class="text-justify"> 
                                In order to disable/enable cookies from your device, you will need to do this via your Internet browser.
                                We have explained how you may manage cookies on your computer via some of the main Internet
                                browsers below.
                            </p>
                            <h6 class="mb-2 mt-2"> Google Chrome</h6>
                               <ul class="mb-4 ml-4">
                                   <li>
                                        In the settings menu, select 'show advanced settings' at the bottom of the page
                                   </li>
                                   <li>
                                        Select the 'Content settings' button in the privacy section
                                   </li>
                                   <li>
                                        The top section of the page that then appears tells you about cookies and allows you to set the cookies you want. It also allows you to clear any cookies currently stored.
                                   </li>
                               </ul>
                               <h6 class="mb-2 mt-2"> Mozilla Firefox</h6>
                                <ul class="mb-4 ml-4">
                                    <li>
                                        In the tools menu, select 'options' Select the privacy tab in the options box
                                    </li>
                                    <li>
                                        From the drop-down choose, 'use custom settings for history'. This will bring up the options for cookies and you can choose to enable or disable them by clicking the
                                        tickbox.
                                   </li>
                                   <li>
                                        Select the 'Content settings' button in the privacy section
                                   </li>
                                </ul>
                                <h6 class="mb-2 mt-2"> Internet Explorer 6+</h6>
                                <ul class="mb-4 ml-4">
                                    <li>
                                        In the tools menu, select 'Internet options'
                                   </li>
                                   <li>
                                        Click the privacy tab
                                   </li>
                                   <li>
                                        You will see a privacy settings slider which has six settings that allow you to control the number of cookies that will be placed: Block All Cookies, High, Medium High, Medium (default level), Low, and Accept All Cookies.
                                   </li>
                                </ul>
                               <h6 class="mb-2 mt-2"> Safari browser</h6>
                                <ul class="mb-4 ml-4">
                                    <li>
                                        In the settings menu, select the 'Preferences' option
                                    </li>
                                    <li>
                                       Open the privacy tab
                                    </li>
                                    <li>
                                       Select the option you want from the 'block cookies' section
                                    </li>
                                </ul>

                               <h6 class="mb-2 mt-2"> All other browsers</h6> 
                               <p class="text-justify">
                                For information on how to manage cookies via other browsers, please consult your documentation or
                                online help files. 

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
