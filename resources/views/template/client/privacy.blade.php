@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <!-- <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-8">
                <div class="section-title">
                    <h2>Privacy Policy</h2>
                </div>
            </div>
        </div> -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="login-form">
                    <div class="part-text">
                        <h4 class="mb-2 mt-2">Privacy Policy </h4>
                            <p class="text-justify">
                                Dear esteemed customer, this privacy policy is developed to give you a full insight into how Betpro360 uses and protect all forms of information you give when you are on our site.

                                <h6 class="mb-2 mt-2">User Information</h6>
                                When you visit the website in a bid to do business with us, here is the information we asked of you so as to serve you better:
                                <ul class="mb-4 ml-4">
                                    <li>
                                        Name
                                    </li>
                                    <li>
                                        Email address
                                    </li>
                                    <li>
                                        Mobile number
                                    </li>

                                </ul>
                            </p>
                            <p class="text-justify">
                                The information we collect above is collected at the point of registering as a user.
                                The main purpose of collecting this information is to facilitate the uniqueness of each user and to make it possible for you to have personalized service.
                                This information also allows us to communicate with you.
                                Helping us give important updates of our services and product and all relevant information that will help us serve you better.
                            </p>

                            <ul class="mb-4 ml-4">
                                <li>
                                    Age of users: 18years +
                                </li>
                            </ul>

                            <h6 class="mb-2 mt-2">Cookies policy </h6>
                            <p class="text-justify">
                                Yes, we use cookies. Cookies are a small piece of data sent from a website and stored on the user's computer by the web browser while the user is browsing. It enables us to show you personalized content when you visit the website.
                                For full Cookies Policy, please refer to COOKIES POLICY. Security
                                We pride in the use of one of the most effective websites security around - SSL.
                                That's why you will notice that our site always has the "HTTPS".
                                This means that your information is safe.
                            </p>

                            <h6 class="mb-2 mt-2">Third-party </h6>
                            <p class="text-justify">
                                We don’t and won't share your details (data) with the third party.
                                Safekeeping your Own Data You (users) should change their information if they notice any third party access.
                                This implies that it is advisable to change your information when you notice someone other than you has access to your information.
                            </p>

                            <h6 class="mb-2 mt-2">Changes to the Privacy Policy </h6>
                            <p class="text-justify">
                                As events unfold in the business world, you will be adequately informed if there are changes to the policy.
                                Thank you for taking the time to go through this important information.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
