@extends('layouts.client.app')

<style type="text/css">
    
    .users{
        box-shadow: 5px 5px 5px 5px #ccc; 
        margin: 10px; 
        padding:25px 10px 10px 10px; 
        border-radius: 50px;
    }

    .user-detail {
        background: #01013F;
    }

    .rounded{
        height: 120px;
        width: 200px;
    }

    .fab{
        padding-top: 12px
    }

    .s-item{
        padding-top: .5rem;
    }

</style>

@section('content')

<div class="user-panel-dashboard">

    <!-- user panel header begin -->
    <div class="user-panel-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    @if (session('status'))
                    <div class="row">
                        <div class="col-4 ml-auto">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Success!</strong> {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="user-face">
                        <div class="part-left">
                            <div>
                                <div class="part-pic">

                                    @if(empty(Auth::user()->avatar))
                                    <img src='{{ asset("images/client/avatar.png") }}' class="avatar" alt="user imag">
                                    @else
                                    <img src='{{ asset("public/".Auth::user()->avatar)}}' class="avatar" alt="user image">
                                    @endif
                                </div>
                            </div>
                            <div class="part-name">
                                <span class="welcome-text">Welcome</span> 
                                <span class="user-name-text"> {{ Auth::user()->firstname }}</span>
                                <h6 class="text-white"> {{ Auth::user()->user_id}}</h6>
                            </div>
                        </div>
                        <div class="part-right">
                            <ul>
                                
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sign-out" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z" class=""></path></svg>
                                        <span class="text">Log Out</span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12">
                    <div class="dashboard-menu">
                        <ul>
                            <li>
                                <a href="{{route('dashboard')}}" class="menu-item active">
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{route('transaction')}}" class="menu-item">
                                   Transactions
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('update')}}" class="menu-item">
                                   Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- user panel header end -->

    <div class="player-statics">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6">
                    <div class="user-panel-title">
                        <h3>User Details</h3>
                    </div>
                </div>
            </div>
            <div class="row users">
                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="single-static user-detail">
                        <div class="part-text">
                            <span class="title text-white">User ID : {{ Auth::user()->user_id }} </span>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="single-static user-detail">
                        <div class="part-text">
                            <span class="title text-white">Registered Email: {{ Auth::user()->email}} </span>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="single-static user-detail">
                        <div class="part-text">
                            <span class="title text-white">Mobile Number:  {{ Auth::user()->phone_number}} </span>
                        </div>
                    </div>
                </div>


                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="single-static user-detail">
                        <div class="part-text">
                            <span class="title text-white">Country:  @if ($country) {{ $country->name }} @endif  </span>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="single-static user-detail">
                        <div class="part-text">
                            <span class="title text-white">Account Status : {{ Auth::user()->status }} </span>
                        </div>
                    </div>
                </div>


                <div class="col-xl-4 col-lg-4 col-sm-6">
                    <div class="single-static user-detail">
                        <div class="part-text">
                            <span class="title text-white">Last Login : {{ Auth::user()->last_login }} </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- user-statics begin -->
    <div class="player-statics">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6">
                    <div class="user-panel-title">
                        <h3>Available Plans</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($plans as $key=> $plan)
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="single-static">
                        <div class="mb-4">
                            <img src="<?=('../images/client/'."$plan->id".'.png') ?>" class="rounded" alt="{{$plan->name}}">
                        </div>
                        <div class="part-text">
                            @if($plan->name == 'Free')
                            <span class="title">  <img src="{{ asset('/images/client/active.png')}}" width="200" alt="subscribe Now"></span>
                            @else
                                @if($plan_name->id == $plan->id)

                                    <img src="{{ asset('/images/client/active.png')}}" width="200" alt="{{$plan_name->name}}">

                                @else
                                <a href="{{ route('pay', $plan->id)}}">
                                    
                                    <img src="{{ asset('/images/client/subscribe.png')}}" width="200" alt="subscribe Now">
                                </a>
                                @endif
                            @endif   
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>


    <!-- user-statics begin -->
    <div class="player-statics">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6">
                    <div class="user-panel-title">
                        <h3>Football Categories</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($features as $key => $feature)
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="single-static">
                        <div class="mb-4">
                            <img src="<?=('../images/client/'."$plan_name->id".'.png') ?>" class="rounded" alt="{{$plan_name->name}}" >

                            <span class="title"> 
                                <p>
                                    <a href="{{ route('prediction', $feature->id)}}" style="color:#01013F; font-weight: 600; margin-top: -50px">{{$feature->title}}
                                    </a>
                                </p>

                                <img src="{{ asset('/images/client/active.png')}}" width="200" alt="active">
                                
                            </span>
                        </div>
                    </div>
                </div>
                @endforeach



                @if($subscriptions)
                    @foreach($subscriptions as $key =>$sub)

                    <div class="col-xl-3 col-lg-3 col-sm-6">
                        <div class="single-static">
                            
                            <div class="part-text">
                                @if(!is_null($sub->id))
                                    <div class="mb-4">


                                        <img src="<?=('../images/client/'."$sub->pid".'.png') ?>" class="rounded" alt="{{$sub->name}}">

                                        <p class="mt-1">
                                            <a href="{{ route('prediction', $sub->id)}}" style="color:#01013F; font-weight: 600; margin-top: -50px">{{$sub->title}}
                                            </a>
                                        </p>

                                        <span class="title"> 

                                            <img src="{{ asset('/images/client/active.png')}}" width="200" alt="active">

                                        </span>

                                    </div>
                                @else

                                <div class="mb-4">
                                    <img src="<?=('../images/client/'."$sub->pid".'.png') ?>" class="rounded" alt="{{$sub->name}}">

                                    <span class="title mt-2"> 

                                        <img src="{{ asset('/images/client/active.png')}}" width="200" alt="active">

                                    </span>
                                </div>


                                @endif
                                
                            </div>
                        </div>
                    </div>

                    @endforeach
                @endif
                
            </div>
        </div>
    </div>
</div>

@endsection
