@extends('layouts.client.app')

@section('content')


    <!-- breadcrumb begin -->
    <div class="breadcrumb-bettix ">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb end -->

    <!-- payment history begin -->
    <div class="user-panel-dashboard">   
        <!-- gold plan -->
        @foreach($planzz as $plann)
        <div class="payment-history">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="user-panel-title">
                            <h3>{{$plann->name}}</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="transaction-area">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        @foreach($country as $coun)
                                        <th scope="col">{{$coun->name}} ({{$coun->symbol}})</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $plants = App\Plan_price::plan_title($plann->id); ?>
                                    @foreach($plants as $plant)
                                    <tr>
                                        <th scope="row" class="d-flex">{{$plant->title}}</th>
                                        @foreach($country as $con)
                                        <?php $planprice = App\Plan_price::all_plan_price($plann->id, $plant->id, $con->id); ?>
                                        <td>{{ isset($planprice->price) ? $planprice->price : '--' }}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- end gold -->
    </div>
    <!-- payment history end -->
@endsection
