@extends('layouts.client.app')

@section('style')
<style type="text/css">
    .active{
        background: #01013F;
    }
    .bg-default{
        background: #ccc;
    }
</style>
@endsection

@section('content')

<!-- user panel header begin -->
<div class="user-panel-dashboard">
    <div class="user-panel-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    @if (session('status'))
                    <div class="row">
                        <div class="col-4 ml-auto">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <strong>Success!</strong> {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="user-face">
                        <div class="part-left">
                            <div>
                                <div class="part-pic">

                                    @if(empty(Auth::user()->avatar))
                                    <img src='{{ asset("images/client/avatar.png") }}' class="avatar" alt="user imag">
                                    @else
                                    <img src='{{ asset("storage/".Auth::user()->avatar)}}' class="avatar" alt="user image">
                                    @endif
                                </div>
                            </div>
                            <div class="part-name">
                                <span class="welcome-text">Welcome</span> 
                                <span class="user-name-text"> {{ Auth::user()->firstname }}</span>
                                <h6 class="text-white"> {{ Auth::user()->user_id}}</h6>
                            </div>
                        </div>
                        <div class="part-right">
                            <ul>
                                
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sign-out" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z" class=""></path></svg>
                                        <span class="text">Log Out</span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12">
                    <div class="dashboard-menu">
                        <ul>
                            <li>
                                <a href="{{route('dashboard')}}" class="menu-item ">
                                    Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{route('prediction')}}" class="menu-item active">
                                    Predictions
                                </a>
                            </li>
                            <li>
                                <a href="{{route('transaction')}}" class="menu-item">
                                   Transactions
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('update')}}" class="menu-item">
                                   Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- result begin -->
    <div class="result" style="padding: 0px 0;">
        <div class="container">
            <h2 class="text-center text-danger">{{$cat_title->title}} PREDICTION</h2>
            <div class="result-sheet-cover" >
                <div class="result-sheet mb-4">

                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a href="/prediction/<?=$cat_title->id?>/2dg" class="nav-link m-2 {{Request::is('prediction/'.$cat_title->id.'/2dg') ? 'active': ''}}">{{date("Y-m-d", strtotime("-1 day", strtotime("yesterday")))}}</a>
                        </li>
                        <li class="nav-item">
                            <a href="/prediction/<?=$cat_title->id?>/1dg" class="nav-link m-2 {{Request::is('prediction/'.$cat_title->id.'/1dg') ? 'active': ''}}">{{date("Y-m-d", strtotime("yesterday"))}}</a>
                        </li>
                        <li class="nav-item">
                            <a href="/prediction/<?=$cat_title->id?>/td" class="nav-link m-2 {{Request::is('prediction/'.$cat_title->id.'/td' ) ? 'active': 'bg-default'}}">TODAY</a>
                        </li>

                        <li class="nav-item">
                            <a href="/prediction/<?=$cat_title->id?>/1tm" class="nav-link m-2 {{Request::is('prediction/'.$cat_title->id.'/1tm') ? 'active': ''}}">{{date("Y-m-d", strtotime("tomorrow"))}}</a>
                        </li>

                        <li class="nav-item">
                            <a href="/prediction/<?=$cat_title->id?>/2tm" class="nav-link m-2 {{Request::is('prediction/'.$cat_title->id.'/2tm') ? 'active': ''}}">{{date("Y-m-d", strtotime("+1 day", strtotime("tomorrow")))}}</a>
                        </li>
                    </ul>

                    
                    @if($cat_title->id == 21 || $cat_title->id == 22 || $cat_title->id == 24 )

                        <h2 class="text-center text-danger">1ST SET</h2>
                        <table class="table" id="prediction">
                            <thead>
                                <tr style="background: #01013F">
                                    <th scope="col"></th>
                                    <th scope="col">Time</th>
                                    <th scope="col">League</th>
                                    <th scope="col">HOME FORM</th>
                                    <th scope="col">Match</th>
                                    <th scope="col">AWAY FORM</th>
                                    <th scope="col">1</th>
                                    <th scope="col">X </th>
                                    <th scope="col">2 </th>
                                    <th scope="col">Prediction </th>
                                    <th scope="col">Result</th>
                                    <th scope="col">1st Set</th>
                                </tr>
                            </thead>             
                            <tbody>
                                @if($cat_title->id == 21)
                                    <?php $total = 0 ?>
                                    @foreach($bets as $key => $prediction)

                                        @if(!is_null($prediction->sure2_odd))

                                            <tr>
                                                <th scope="row">
                                                    <span class="icon">
                                                        <i class="flaticon-football"></i>
                                                    </span>
                                                </th>
                                                <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                                <td>{{$prediction->nickname}}</td>
                                                <td>{{$prediction->home_form}}</td>
                                                <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                                <td>{{$prediction->away_form}}</td>
                                                <td>{{$prediction->home_odd}}</td>
                                                <td>{{$prediction->draw_odd}}</td>
                                                <td>{{$prediction->away_odd}}</td>
                                                <td class="text-center">{{$prediction->recommendation}}</td>
                                                <td class="text-center">
                                                    <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                        
                                                </td>
                                                <td class="text-center">{{$prediction->sure2_odd}}</td>
                                                
                                            </tr>

                                            {{$total += $prediction->recommendation}}
                                
                                        @endif

                                    @endforeach
                                    <tr>
                                        <td colspan="9"></td>
                                        <td class="text-center text-danger"><span>Total Odd : &nbsp;{{$total}}</span></td>
                                    </tr>

                                @elseif($cat_title->id == 22)
                                    <?php $total = 0 ?>
                                    @foreach($bets as $key => $prediction)

                                        @if(!is_null($prediction->sure3_odd))

                                            <tr>
                                                <th scope="row">
                                                    <span class="icon">
                                                        <i class="flaticon-football"></i>
                                                    </span>
                                                </th>
                                                <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                                <td>{{$prediction->nickname}}</td>
                                                <td>{{$prediction->home_form}}</td>
                                                <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                                <td>{{$prediction->away_form}}</td>
                                                <td>{{$prediction->home_odd}}</td>
                                                <td>{{$prediction->draw_odd}}</td>
                                                <td>{{$prediction->away_odd}}</td>
                                                <td class="text-center">{{$prediction->recommendation}}</td>
                                                <td class="text-center">
                                                    <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                        
                                                </td>
                                                <td class="text-center">{{$prediction->sure3_odd}}</td>
                                            </tr>

                                            {{$total += $prediction->recommendation}}
                                
                                        @endif

                                    @endforeach

                                    <tr>
                                        <td colspan="9"></td>
                                        <td class="text-center text-danger"><span>Total Odd : &nbsp;{{$total}}</span></td>
                                    </tr>

                                @elseif($cat_title->id == 24)
                                    <?php $total = 0 ?>
                                    @foreach($bets as $key => $prediction)

                                        @if(!is_null($prediction->investment_odd))

                                            <tr>
                                                <th scope="row">
                                                    <span class="icon">
                                                        <i class="flaticon-football"></i>
                                                    </span>
                                                </th>
                                                <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                                <td>{{$prediction->nickname}}</td>
                                                <td>{{$prediction->home_form}}</td>
                                                <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                                <td>{{$prediction->away_form}}</td>
                                                <td>{{$prediction->home_odd}}</td>
                                                <td>{{$prediction->draw_odd}}</td>
                                                <td>{{$prediction->away_odd}}</td>
                                                <td class="text-center">{{$prediction->recommendation}}</td>
                                                <td class="text-center">
                                                   <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                          
                                                </td>
                                                <td class="text-center">{{$prediction->investment_odd}}</td>
                                            </tr>

                                            {{$total += $prediction->recommendation}}
                                
                                        @endif

                                    @endforeach
                                    <tr>
                                        <td colspan="9"></td>
                                        <td class="text-center text-danger"><span>Total Odd : &nbsp;{{$total}}</span></td>
                                    </tr>

                                @endif
                            </tbody>
                        </table>

                        <h2 class="text-center text-danger">2ND SET</h2>
                        <table class="table" id="prediction">
                            <thead>
                                <tr style="background: #01013F">
                                    <th scope="col"></th>
                                    <th scope="col">Time</th>
                                    <th scope="col">League</th>
                                    <th scope="col">HOME FORM</th>
                                    <th scope="col">Match</th>
                                    <th scope="col">AWAY FORM</th>
                                    <th scope="col">1</th>
                                    <th scope="col">X </th>
                                    <th scope="col">2 </th>
                                    <th scope="col">Prediction </th>
                                    <th scope="col">Result</th>
                                    <th scope="col">2nd Set</th>
                                </tr>
                            </thead>        
                                
                            <tbody>
                                @if($cat_title->id == 21)
                                    <?php $total = 0 ?>
                                    @foreach($bets as $key => $prediction)

                                        @if(!is_null($prediction->sure2_2odd))
                                            
                                            <tr>
                                                <th scope="row">
                                                    <span class="icon">
                                                        <i class="flaticon-football"></i>
                                                    </span>
                                                </th>
                                                <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                                <td>{{$prediction->nickname}}</td>
                                                <td>{{$prediction->home_form}}</td>
                                                <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                                <td>{{$prediction->away_form}}</td>
                                                <td>{{$prediction->home_odd}}</td>
                                                <td>{{$prediction->draw_odd}}</td>
                                                <td>{{$prediction->away_odd}}</td>
                                                <td class="text-center">{{$prediction->recommendation}}</td>
                                                <td class="text-center">
                                                    <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                        
                                                </td>
                                                <td class="text-center">{{$prediction->sure2_odd}}</td>
                                            </tr>
                                
                                            {{$total += $prediction->recommendation}}
                                        @endif

                                    @endforeach
                                    <tr>
                                        <td colspan="9"></td>
                                        <td class="text-center text-danger"><span>Total Odd : &nbsp;{{$total}}</span></td>
                                    </tr>

                                @elseif($cat_title->id == 22)
                                    <?php $total = 0 ?>

                                    @foreach($bets as $key => $prediction)

                                        @if(!is_null($prediction->sure3_2odd))
                                            
                                            <tr>
                                                <th scope="row">
                                                    <span class="icon">
                                                        <i class="flaticon-football"></i>
                                                    </span>
                                                </th>
                                                <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                                <td>{{$prediction->nickname}}</td>
                                                <td>{{$prediction->home_form}}</td>
                                                <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                                <td>{{$prediction->away_form}}</td>
                                                <td>{{$prediction->home_odd}}</td>
                                                <td>{{$prediction->draw_odd}}</td>
                                                <td>{{$prediction->away_odd}}</td>
                                                <td class="text-center">{{$prediction->recommendation}}</td>
                                                <td class="text-center">
                                                    <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                         
                                                </td>
                                                <td class="text-center">{{$prediction->sure3_2odd}}</td>
                                            </tr>

                                            {{$total += $prediction->recommendation}}
                                
                                        @endif

                                    @endforeach

                                    <tr>
                                        <td colspan="9"></td>
                                        <td class="text-center text-danger"><span>Total Odd : &nbsp;{{$total}}</span></td>
                                    </tr>

                                @elseif($cat_title->id == 24)
                                    <?php $total = 0 ?>
                                    @foreach($bets as $key => $prediction)

                                        @if(!is_null($prediction->investment_2odd))
                                            
                                            <tr>
                                                <th scope="row">
                                                    <span class="icon">
                                                        <i class="flaticon-football"></i>
                                                    </span>
                                                </th>
                                                <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                                <td>{{$prediction->nickname}}</td>
                                                <td>{{$prediction->home_form}}</td>
                                                <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                                <td>{{$prediction->away_form}}</td>
                                                <td>{{$prediction->home_odd}}</td>
                                                <td>{{$prediction->draw_odd}}</td>
                                                <td>{{$prediction->away_odd}}</td>
                                                <td class="text-center">{{$prediction->recommendation}}</td>
                                                <td class="text-center">
                                                    <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                        
                                                </td>
                                                <td class="text-center">{{$prediction->investment_2odd}}</td>
                                            </tr>

                                        {{$total += $prediction->recommendation}}
                                
                                        @endif

                                    @endforeach

                                    <tr>
                                        <td colspan="9"></td>
                                        <td class="text-center text-danger"><span>Total Odd : &nbsp;{{$total}}</span></td>
                                    </tr>

                                @endif

                            </tbody>
                        </table>

                    @else

                        <table class="table" id="prediction">
                            <thead>
                                <tr style="background: #01013F">
                                    <th scope="col"></th>
                                    <th scope="col">Time</th>
                                    <th scope="col">League</th>
                                    <th scope="col">HOME FORM</th>
                                    <th scope="col">Match</th>
                                    <th scope="col">AWAY FORM</th>
                                    <th scope="col">1</th>
                                    <th scope="col">X </th>
                                    <th scope="col">2 </th>
                                    <th scope="col">Prediction </th>
                                    <th scope="col">Result</th>
                                </tr>
                            </thead>        
                                        
                            <tbody>


                                @foreach($bets as $key => $prediction)
                                    @if(Auth::user()->plan_id == 1)
                                        @if($key <= 4 )
                                        <tr>
                                            <th scope="row">
                                                <span class="icon">
                                                    <i class="flaticon-football"></i>
                                                </span>
                                            </th>
                                            <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                            <td>{{$prediction->nickname}}</td>
                                            <td>{{$prediction->home_form}}</td>
                                            <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                            <td>{{$prediction->away_form}}</td>
                                            <td>{{$prediction->home_odd}}</td>
                                            <td>{{$prediction->draw_odd}}</td>
                                            <td>{{$prediction->away_odd}}</td>
                                            <td class="text-center">{{$prediction->recommendation}}</td>
                                            <td class="text-center">
                                                <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                         
                                            </td>
                                        </tr>
                                        @endif 
                                    @else

                                        <tr>
                                            <th scope="row">
                                                <span class="icon">
                                                    <i class="flaticon-football"></i>
                                                </span>
                                            </th>
                                            <td class="date"> {{Carbon\Carbon::parse($prediction->match_date)->isoFormat('h:mm ')}}</td>
                                            <td>{{$prediction->nickname}}</td>
                                            <td>{{$prediction->home_form}}</td>
                                            <td class="team-name"><a href="/match/<?=$prediction->id.'/'.$cat_title->id?>" class="text-danger" target="tabs">{{$prediction->teams}}</a> </td>
                                            <td>{{$prediction->away_form}}</td>
                                            <td>{{$prediction->home_odd}}</td>
                                            <td>{{$prediction->draw_odd}}</td>
                                            <td>{{$prediction->away_odd}}</td>
                                            <td class="text-center">{{$prediction->recommendation}}</td>
                                            <td class="text-center">
                                                <span style="padding: 0.6rem"><i class="fa fa-circle text-{{$prediction->result == 'good' ? 'success' : ($prediction->result == 'bad' ? 'danger': 'white')}}" aria-hidden="true"></i></span>                         
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                                @if(Auth::user()->plan_id == 1)
                                    <tr>
                                        <th scope="row">
                                            <span class="icon">
                                                <i class="flaticon-football"></i>
                                            </span>
                                        </th>
                                        <td class="date"> 
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span> 
                                        </td>
                                        <td>
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td>
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td class="team-name">
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td>
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td>
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td>
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td>
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            <span >
                                                <i class="fa fa-lock text-danger" aria-hidden="true"></i>
                                            </span>                        
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>


                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- result end -->
</div>

@endsection
