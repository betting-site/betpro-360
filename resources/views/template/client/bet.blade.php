@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="login-form">
                    <!-- feature begin -->
                    <div class="feature" id="feature_section">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-xl-6 col-lg-6 col-md-8">
                                    <div class="section-title">
                                        <h2>Bet Category</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($categories as $cat)
                                <div class="col-xl-4 col-lg-4 col-md-6">
                                    <div class="single-feature">
                                        <div class="part-icon">
                                            <a class="social-icon"><i class="fa fa-check" style="color: #01013F; font-size: 40px"></i></a>
                                        </div>
                                        <div class="part-text">
                                            <h3 class="title">{{$cat->title}}</h3>
                                            <p>{{$cat->description}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- feature end -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
