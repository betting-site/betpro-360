@extends('layouts.client.app')

@section('style')
<style type="text/css">
    
.page-link{
   padding: 1.2rem 1.5rem; 
   color: #111705; 
   font-weight: 600;
}

.page-item, .page-item.active .page-link{
    background: rgba(248, 27, 100, 0.08);
    color: rgba(42, 42, 42, 0.65);
    border: 1px solid rgba(248, 27, 100, 0.2);
}

</style>
@endsection

@section('content')
<div class="user-panel-header">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="user-face">
                    <div class="part-left">
                        <div>
                            <div class="part-pic">

                                @if(empty(Auth::user()->avatar))
                                <img src='{{ asset("images/client/avatar.png") }}' class="avatar" alt="user imag">
                                @else
                                <img src='{{ asset("storage/".Auth::user()->avatar)}}' class="avatar" alt="user image">
                                @endif
                            </div>
                        </div>
                        <div class="part-name">
                            <span class="welcome-text">Welcome</span> 
                            <span class="user-name-text"> {{ Auth::user()->firstname }}</span>
                            <h6 class="text-white"> {{ Auth::user()->user_id}}</h6>
                        </div>
                    </div>
                    <div class="part-right">
                        <ul>
                            
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sign-out" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z" class=""></path></svg>
                                    <span class="text">Log Out</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <div class="dashboard-menu">
                    <ul>
                        <li>
                            <a href="{{route('dashboard')}}" class="menu-item">
                                Dashboard
                            </a>
                        </li>
                        <li>
                            <a href="{{route('prediction')}}" class="menu-item">
                                Predictions
                            </a>
                        </li>
                        <li>
                            <a href="{{route('transaction')}}" class="menu-item active">
                               Transactions
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('update')}}" class="menu-item ">
                               Edit Profile
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- payment begin -->
    <div class="user-panel-dashboard">   
        <!-- payment history begin -->
        <div class="payment-history">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="user-panel-title">
                            <h3>Transaction History</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="transaction-area">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">User ID</th>
                                        <th scope="col">Plan Name</th>
                                        <th scope="col">Plan Type</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Start Date</th>
                                        <th scope="col">End Date</th>
                                        <th scope="col">Transaction Method  </th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transactions as $key => $tranz)
                                    <tr>   
                                        <th>{{$key+1}}</th>
                                        <th scope="row" class="d-flex">{{$tranz->user_id}}</th>
                                        <td>{{$tranz->title}}</td>
                                        <td>{{$tranz->name}}</td>
                                        <td>{{$tranz->amount}}</td>
                                        <td>{{$tranz->start_date}}</td>
                                        <td>{{$tranz->end_date}}</td>
                                        <td>{{$tranz->payment_type}}</td>
                                        <td>{{$tranz->status}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        @if($transactions->links())
                        <div class="links mt-4">
                            {{ $transactions->links() }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- payment history end -->
    </div>
    <!-- payment end -->

@endsection
