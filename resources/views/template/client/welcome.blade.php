@extends('layouts.client.app')

@section('content')
<style>
.table-responsive::-webkit-scrollbar {
    width: 10px;
}
.table-responsive::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
.table-responsive::-webkit-scrollbar-thumb {
    border: 1px solid #f1f1f1;
    border-radius: 20px;
    background: #01013F; 
}
.table-responsive::-webkit-scrollbar-thumb:hover {
    background: #555; 
}
.bet-btn-base.active {
    background: #2a2a2a;
    color: #fff;
}
ul.nav { border: none;}

.rez {
    margin: 25px auto;
    font-size: 25px;
    color: #fff;
}

.rezz {
    margin: 10px auto;
    font-size: 10px;
    color: #fff;
}

ul.starz {margin: 30px auto; text-align: center;}

ul.starz li, ul.broll li {
    list-style: none;
    display: inline;
    margin: 5px;
}

ul.broll li div {display: inline-block;}

.plan h3 {
    text-align: center;
    margin: 10px auto;
}

.plan {
    min-width: 100%;
    margin: 50px auto;
    padding: 20px;
    border: 2px solid #01013F;
}

.plan-item {
    margin: 15px auto;
}
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 text-center" style="padding: 0; margin: 0 0 5px;">
                <a href="#"><img src='{{ asset("public/$advert1") }}' class="img-responsive d-none d-lg-block" style="margin: auto" title="advert1"></a>
                <a href="#"><img src='images/client/preset/slider.png' class="img-responsive d-block d-lg-none" style="margin: auto" title="advert1Mobile"></a>
            </div>

            <div class="col-lg-6 text-center" style="padding: 0; margin: 0 0 5px;">
                <!-- banner begin -->
                <div class="banner-slider owl-carousel owl-theme">
                    @if(isset($sliders))
                    @foreach($sliders as $slider)
                    <div class="banner banner-2 slide-1">
                        <div class="container">
                            <div class="row justify-content-xl-center justify-content-lg-center justify-content-md-center">
                                <a href="javascript:;"><img src='{{ asset("public/$slider->avatar" ? "public/$slider->avatar" : "images/client/preset/slider.png ") }}' class="img-responsive" title="slider1"></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="banner banner-2 slide-1">
                        <div class="container">
                            <div class="row justify-content-xl-center justify-content-lg-center justify-content-md-center">
                                <a href="javascript:;"><img src='{{ asset("images/client/preset/slider.png") }}' class="img-responsive" title="slider1"></a>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <!-- banner end -->
            </div>
            <div class="free col-lg-4 text-center" style="padding: 20px; margin: 0 0 5px; max-height: 500px; background: #01013F; color: #fff">
                <h2>FREE DAILY ACCA</h2>
                <ul class="row nav nav-tabs" data-type="free_acca">
                    <li class="date col-sm-4" data-type="yesterday">
                        <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block gotab" data-toggle="tab" href="#yesterday">Yesterday</a>
                    </li>
                    <li class="date col-sm-4"  data-type="today">
                        <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block active gotab" data-toggle="tab" href="#today">Today</a>
                    </li>
                    <li class="date col-sm-4"  data-type="tomorrow">
                        <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block gotab" data-toggle="tab" href="#tomorrow">Tomorrow</a>
                    </li>
                </ul>
                <div class="tab-content dtab">
                    <div id="yesterday" class="tab-pane fade">
                        <div class="table-responsive" style="max-height: 330px;">
                            <table class="table table-striped table-hover table-dark">
                                <thead>
                                    <tr>
                                        <th class="text-left" scope="col">Match</th>
                                        <th scope="col">1 </th>
                                        <th scope="col">X </th>
                                        <th scope="col">2 </th>
                                    </tr>
                                </thead>
                                <tbody class="prd"></tbody>
                            </table>
                        </div>
                    </div>                    
                    <div id="today" class="tab-pane fade show active">
                        <div class="table-responsive" style="max-height: 330px;">
                            <table class="table table-striped table-hover table-dark">
                                <thead>
                                    <tr>
                                        <th class="text-left" scope="col">Match</th>
                                        <th scope="col">1 </th>
                                        <th scope="col">X </th>
                                        <th scope="col">2 </th>
                                    </tr>
                                </thead>
                                <tbody class="prd"></tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tomorrow" class="tab-pane fade">
                        <div class="table-responsive" style="max-height: 330px;">
                            <table class="table table-striped table-hover table-dark">
                                <thead>
                                    <tr>
                                        <th class="text-left" scope="col">Match</th>
                                        <th scope="col">1 </th>
                                        <th scope="col">X </th>
                                        <th scope="col">2 </th>
                                    </tr>
                                </thead>
                                 <tbody class="prd"></tbody>
                            </table>
                            <p class="text-center"> <a href="asset('login')}}" class="text-white">See More </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- upcoming match begin -->
    <div class="upcoming-match" id="upcoming_match">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12">    
                    <div class="" style="background: #01013F; padding: 20px; height: 250px; margin-bottom: 40px;">
                        <h3 style="text-align: center; color: #FFBF00">BET-ROLLOVER</h3>
                        <div class="row">
                            <div class="col-md-4" style="text-align: center; color: #fff; border: 1px solid #fff;">
                                <h5 style="text-align: center; color: #fff; padding: 10px; margin-top: 20px;">Static text goes here, add your static contents here, make sure the contents are awesome and post them here</h5>
                            </div>
                            <div class="col-md-4" style="text-align: center; color: #fff; border: 1px solid #fff;">        
                                <div class="" style="padding: 20px;">

                                    <div class="row">
                                        <ul class="broll">
                                        @foreach($rollovers as $rollover)
                                            <li>
                                                <div>
                                                    <span>{{Carbon\Carbon::parse($rollover->created_at)->format('d/m')}}</span><br>
                                                    <i class="fa fa-check bg-{{ $rollover->status == 'good' ? 'success' : ($rollover->status == 'medium' ? 'warning': 'danger')}}"></i>
                                                </div>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-4" style="text-align: center; color: #fff; border: 1px solid #fff; padding: 20px;">
                                @if($rollover_remark)

                                {{$rollover_remark['remark'] ? $rollover_remark['remark'] : ''}}

                                @endif
                                <ul class="starz">
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                
                <div class="col-md-4">    
                    <div class="" style="background: #01013F; padding: 20px; height: 350px;">
                        <h3 style="text-align: center; color: #FFBF00">Investment Result</h3>
                        <!-- <div class="row">
                            @foreach($advisors as $advisor)
                            <div class="col-6">
                                <div class="rez">
                                    
                                    <i class="fa fa-check bg-{{ $advisor->status == 'good' ? 'success' : ($advisor->status == 'medium' ? 'warning': 'danger')}}"></i>
                                
                                    <span>{{Carbon\Carbon::parse($advisor->created_at)->format('d/m')}}</span>
                                </div>
                            </div>
                            @endforeach
                            <div class="row">
                                <div class="col-12 text-center text-white mt-4 ml-4">
                                    @if($advisor_remark)
                                    {{$advisor_remark['remark'] ? $advisor_remark['remark'] : ''}}
                                    @endif
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12">
                                <h5 style="text-align: center; color: #fff; border-top: 1px solid #fff; border-bottom: 1px solid #fff; padding: 10px; margin-top: 20px;">Static text goes here, add your static contents here, make sure the contents are awesome and post them here</h5>
                            </div>
                            <div class="col-md-12">
                                <ul class="starz">
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                    <li><i class="fa fa-star" style="color: #FFBF00;"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">        
                    <div class="" style="background: #01013F; padding: 20px; height: 350px;">
                        <h3 style="text-align: center; color: #fff">Results</h3>

                        <div class="row">
                            @foreach($invests as $invest)
                            <div class="col-6">
                                <div class="rez">
                                    <i class="fa fa-check bg-{{ $invest->status == 'good' ? 'success' : ($invest->status == 'medium' ? 'warning': 'danger')}}"></i>

                                    <span>{{Carbon\Carbon::parse($invest->created_at)->format('d/m')}}</span>
                                </div>
                            </div>
                            @endforeach
                            <!-- <div class="row"> -->
                                <div class="col-xs-12 text-center text-white m-auto">
                                    @if($invest_remark)
                                        {{$invest_remark['remark'] ? $invest_remark['remark'] : ''}}
                                    @endif
                                </div>
                            <!-- </div> -->
                        </div>
                    </div> 
                </div>
                <div class="col-md-4">
                    <div class="" style="">
                        <div class="single-match">
                            <div class="part-head">
                                <h5 class="match-title">@if($timer){{$timer['title'] ? $timer['title']: ''}} @endif</h5>
                            </div>
                            <div class="part-team">
                                <div class="match-details" style="margin: auto;">
                                    <div class="match-time">
                                        <span class="date">
                                            <!-- Fri 09 Oct 2019 -->
                                            @if($timer){{Carbon\Carbon::parse($timer['timer'])->isoFormat('MMM Do YYYY')}} @endif</span>
                                        <span class="time"> 
                                            <!-- 09:00 am -->
                                           @if($timer) {{Carbon\Carbon::parse($timer['timer'])->isoFormat('h:mm a')}} @endif
                                        </span>
                                    </div>
                                    <div class="buttons">
                                        <a href="{{route('home')}}" class="bet-btn bet-btn-dark-light" style="width: auto; padding: 0 20px;">Next Investment Start In:</a>
                                    </div>
                                </div>
                            </div>

                            <div class="part-timer timer" data-date="<?= ($timer) ? Carbon\Carbon::parse($timer['timer'])->isoFormat('D MMMM YYYY h:mm:ss') : ''?>">
                            
                                <div class="single-time">
                                    <span class="number hour">@if($timer){{Carbon\Carbon::parse($timer['timer'])->isoFormat('h')}} @endif</span>
                                    <span class="title">Hours</span>
                                </div>
                                <div class="single-time">
                                    <span class="number minute">@if($timer){{Carbon\Carbon::parse($timer['timer'])->isoFormat('mm')}} @endif</span>
                                    <span class="title">Minutes</span>
                                </div>
                                <div class="single-time">
                                    <span class="number second">@if($timer){{Carbon\Carbon::parse($timer['timer'])->isoFormat('ss')}} @endif</span>
                                    <span class="title">Seconds</span>
                                </div>
                            </div>
                        </div>
                    </div>      
                </div>        
            </div>
        </div>
        <div class="container">
            <div class="row">
                
                <div class="col-md-12">    
                    <div class="expert" style="background: #01013F; padding: 20px; height: auto; margin-bottom: 40px;">
                        <p style="text-align: center; float: left; max-width: 200px; color: #FFBF00;">
                            VIEW FOOTBALL EXPERT ADVISOR RESULTS
                        </p>
                        <p style="float: right; color: #FFBF00;"><i class="fa fa-arrow-down"></i></p>
                        <div style="clear: both"></div>
                        <div class="row expact" style="display: none;">
                            <div class="col-md-4" style="text-align: center; color: #fff; border: 1px solid #fff;">
                                <h5 style="text-align: center; color: #fff; padding: 10px; margin-top: 20px;">Expert Leo</h5>
                                <p>(4 Daily Odds)</p>
                            </div>
                            <div class="col-md-4" style="text-align: center; color: #fff; border: 1px solid #fff;">        
                                <div class="" style="padding: 20px;">

                                    <div class="row">
                                        <ul class="broll">
                                        @foreach($rollovers as $rollover)
                                            <li>
                                                <div>
                                                    <span>{{Carbon\Carbon::parse($rollover->created_at)->format('d/m')}}</span><br>
                                                    <i class="fa fa-check bg-{{ $rollover->status == 'good' ? 'success' : ($rollover->status == 'medium' ? 'warning': 'danger')}}"></i>
                                                </div>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-4" style="text-align: center; color: #fff; border: 1px solid #fff; padding: 20px;">
                                @if($rollover_remark)

                                {{$rollover_remark['remark'] ? $rollover_remark['remark'] : ''}}

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- upcoming match end -->

    <div class="container-fluid">
        <div class="row">

            
            <div class="col-md-12 text-center" style="padding: 0; margin: 0;">
                <a href="#"><img src='{{ asset("public/$advert2") }}' class="img-responsive" style="margin: auto" title="advert2"></a>
            </div>

        </div>
    </div>

    <!-- about begin -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 text-center">
                <div class="plan">
                    <h3>Gold Plan</h1>
                    <div class="row">
                        <div class="row">
                            <?php $golds = App\category::where('plan_id', '1,2,3')->get();?>

                            @foreach($golds as $key=>$gold)
                            <div class="col-3">
                                <div class="plan-item">
                                    <div class="plan-icon">
                                        <i class="fa fa-user" style="color: #01013F; font-size: 20px"></i>
                                    </div>
                                    <div class="plan-text">
                                        <p>{{ucwords(strtolower($gold->title ? $gold->title : 'N/A'))}}</p>
                                    </div>
                                </div>
                            </div>
                            @if($key > 2)
                                @break
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="plan">
                    <h3>Free Plan</h1>
                    <div class="row">
                        <?php $frees = App\category::where('plan_id', '1')->get();?>

                        @foreach($frees as $key=>$free)
                        <div class="col-3">
                            <div class="plan-item">
                                <div class="plan-icon">
                                    <i class="fa fa-user" style="color: #01013F; font-size: 20px"></i>
                                </div>
                                <div class="plan-text">
                                    <p>{{ucwords(strtolower($free->title ? $free->title : 'N/A'))}}</p>
                                </div>
                            </div>
                        </div>
                        @if($key > 2)
                            @break
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="plan">
                    <h3>Silver Plan</h1>
                    <div class="row">
                        <?php $silvers = App\category::where('plan_id', '1,2')->get();?>

                        @foreach($silvers as $key=>$silver)
                        <div class="col-3">
                            <div class="plan-item">
                                <div class="plan-icon">
                                    <i class="fa fa-user" style="color: #01013F; font-size: 20px"></i>
                                </div>
                                <div class="plan-text">
                                    <p>{{ucwords(strtolower($silver->title ? $silver->title : 'N/A'))}}</p>
                                </div>
                            </div>
                        </div>
                        @if($key > 2)
                            @break
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- feature end -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 text-center" style="">
                <div class="row">
                    <div class="col-sm-12" style="height: 100px; margin-bottom: 5px;">
                        <a href="#"><img src='{{ asset("public/$advert3") }}' class="img-responsive" style="margin: auto; height: 100px; min-width: 100%;" title="advert3"></a>
                    </div>
                    <div class="col-sm-12" style="height: 100px; margin-top: 5px;">
                        <a href="#"><img src='{{ asset("public/$advert4") }}' class="img-responsive" style="margin: auto; height: 100px; min-width: 100%;" title="advert4"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-center" style="height: 210px;">
                <a href="#"><img src='{{ asset("public/$advert5") }}' class="img-responsive" style="margin: auto; height: 210px" title="advert5"></a>
            </div>
        </div>
    </div>


    <!-- upcoming match begin -->
    <div class="upcoming-match" id="upcoming_match">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-8">
                    <div class="section-title">
                        <h2>Latest Winning Tips </h2>
                        <!-- <p>Betpro360 shows real time odds for betting with the higher stakes you can get. We place your bets in various bMakers to do highest liquidity</p> -->
                    </div>
                </div>
            </div>

            <div class="row"> 

                <div class="popular col-md-5 col-lg-5 text-center mb-4" style="padding: 20px; margin: 0; max-height: 500px; background: #01013F; color: #fff">
                    <h2>WINNING TIPS</h2>
                    <ul class="row nav nav-tabs" data-type="popular">
                        <li class="date col-sm-4" data-type="yesterday">
                            <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block gotab" data-toggle="tab" href="#yesterday2">Yesterday</a>
                        </li>
                        <li class="date col-sm-4"  data-type="today">
                            <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block active gotab" data-toggle="tab" href="#today2">Today</a>
                        </li>
                        <li class="date col-sm-4"  data-type="tomorrow">
                            <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block gotab" data-toggle="tab" href="#tomorrow2">Tomorrow</a>
                        </li>
                    </ul>


                    <div class="tab-content dtab">
                        <div id="yesterday2" class="tab-pane fade">
                            <div class="table-responsive" style="max-height: 330px;">
                                <table class="table table-striped table-hover table-dark">
                                    <thead>
                                        <tr>
                                            <th class="text-left" scope="col">Match</th>
                                            <th scope="col">1 </th>
                                            <th scope="col">X </th>
                                            <th scope="col">2 </th>
                                        </tr>
                                    </thead>
                                    <tbody class="prd"></tbody>
                                </table>
                                <p class="text-center"> <a href="asset('login')}}" class="text-white">See More </a></p>
                            </div>
                        </div>                    
                        <div id="today2" class="tab-pane fade show active">
                            <div class="table-responsive" style="max-height: 330px;">
                                <table class="table table-striped table-hover table-dark">
                                    <thead>
                                        <tr>
                                            <th class="text-left" scope="col">Match</th>
                                            <th scope="col">1 </th>
                                            <th scope="col">X </th>
                                            <th scope="col">2 </th>
                                        </tr>
                                    </thead>
                                    <tbody class="prd"></tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tomorrow2" class="tab-pane fade">
                            <div class="table-responsive" style="max-height: 330px;">
                                <table class="table table-striped table-hover table-dark">
                                    <thead>
                                        <tr>
                                            <th class="text-left" scope="col">Match</th>
                                            <th scope="col">1 </th>
                                            <th scope="col">X </th>
                                            <th scope="col">2 </th>
                                        </tr>
                                    </thead>
                                    <tbody class="prd"></tbody>
                                </table>
                                <p class="text-center"> <a href="asset('login')}}" class="text-white">See More </a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-2"></div>
                

                <div class="upcoming col-md-5 col-lg-5 text-center mb-4" style="padding: 20px; margin: 0; max-height: 500px; background: #01013F; color: #fff">
                        <h2>UPCOMING PREDICTION</h2>
                        <ul class="row nav nav-tabs" data-type="upcoming_pred">
                            <li class="date col-sm-4" data-type="yesterday">
                                <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block gotab" data-toggle="tab" href="#yesterday3">Yesterday</a>
                            </li>
                            <li class="date col-sm-4"  data-type="today">
                                <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block active gotab" data-toggle="tab" href="#today3">Today</a>
                            </li>
                            <li class="date col-sm-4"  data-type="tomorrow">
                                <a class="bet-btn bet-btn-base mb-2 mt-2 mr-2 btn-block gotab" data-toggle="tab" href="#tomorrow3">Tomorrow</a>
                            </li>
                        </ul>
                        <div class="tab-content dtab">
                            <div id="yesterday3" class="tab-pane fade">
                                <div class="table-responsive" style="max-height: 330px;">
                                    <table class="table table-striped table-hover table-dark">
                                        <thead>
                                            <tr>
                                                <th class="text-left" scope="col">Match</th>
                                                <th scope="col">1 </th>
                                                <th scope="col">X </th>
                                                <th scope="col">2 </th>
                                            </tr>
                                        </thead>
                                        <tbody class="prd"></tbody>
                                    </table>
                                </div>
                            </div>                    
                            <div id="today3" class="tab-pane fade show active">
                                <div class="table-responsive" style="max-height: 330px;">
                                    <table class="table table-striped table-hover table-dark">
                                        <thead>
                                            <tr>
                                                <th class="text-left" scope="col">Match</th>
                                                <th scope="col">1 </th>
                                                <th scope="col">X </th>
                                                <th scope="col">2 </th>
                                            </tr>
                                        </thead>
                                        <tbody class="prd"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="tomorrow3" class="tab-pane fade">
                                <div class="table-responsive" style="max-height: 330px;">
                                    <table class="table table-striped table-hover table-dark">
                                        <thead>
                                            <tr>
                                                <th class="text-left" scope="col">Match</th>
                                                <th scope="col">1 </th>
                                                <th scope="col">X </th>
                                                <th scope="col">2 </th>
                                            </tr>
                                        </thead>
                                        <tbody class="prd"></tbody>
                                    </table>
                                    <p class="text-center"> <a href="asset('login')}}" class="text-white">See More </a></p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- upcoming match end -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center" style="padding: 0; margin: 0;">
                <a href="#"><img src='{{ asset("public/$advert6") }}' class="img-responsive" style="margin: auto" title="advert6"></a>
            </div>
        </div>
    </div>

    <!-- Introduction -->
    <div class="row justify-content-center">
        <div class="p-4 m-4 text-white" style="background: #01013F;">
            <div class="part-text">
                <h2 class="text-center">INTRODUCING BETPRO360</h2>
                <p class="m-2 p-2">
                    Finally, the Best Prediction Website Designed for You Sport is fun. Losing or winning, it's magical. What other activities are capable of bringing the same tenacity and sense of fulfillment as passionately as sport does? You can hardly name a few! Another thing that's more magical is earning from that win or lose. And that's what you get from us.
                    There are no two ways about it, you either love sport passionately or you love the rewards that come with it.
                </p>
            </div>
        </div>
    </div>
    <!-- End Introduction -->
    
@endsection

@section('scripts')

<script type="text/javascript">

        $('.gotab.active').each(function() {
            findit($(this));
        })
      

        function findit(tiz, day = '') {
            
            day =  tiz.closest('.date').data('type');
            type =  tiz.closest('.nav').data('type');

            $.ajax({
                type:'get',
                url: "/match",
                dataType: 'json',
                data: {day: day, type: type},  
                success: function (data) {
                var match = '';

                if ( data !== '') {

                    data.forEach(result);

                    function result(item) {
                        match +='<tr><td class="text-left">'+item.teams+'</td><td>'+item.home_odd+'</td><td>'+item.draw_odd+'</td><td>'+item.away_odd+'</td></tr>'
                    }
                    
                }

                    tiz.closest('div.popular').find('.prd').html(match);
                    tiz.closest('div.upcoming').find('.prd').html(match);
                    tiz.closest('div.free').find('.prd').html(match);
                }
            })
        }

        $('.gotab').click(function(){
            findit($(this));
        })
    // })

</script>

@endsection


