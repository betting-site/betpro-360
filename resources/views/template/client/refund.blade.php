@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <!-- <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-8">
                <div class="section-title">
                    <h2>Refund Policy</h2>
                </div>
            </div>
        </div> -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="login-form">
                    <div class="part-text">
                        <h4 class="mb-2 mt-2">Refund Policy </h4>
                        <p class="text-justify">
							Just because we are well aware of our intention to be perfect while serving you, it's important to make you fully aware of our refund policy.
							As a company, our sole intention is to help you make money in your passion; a refund can only be made when you are yet to benefit from the offer.
							This implies that you have to request for a refund before you are given value.
							After placing your refund request, we will do everything in our capacity to make the refund available to you within 14 working days.
							We are certain that there won't be any need for you to seek or request for a refund because we will satisfy you with our best and that will be the best you have ever received.
							Thank you for your continuous trust in us. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
