@extends('layouts.client.app')

@section('content')

<!-- breadcrumb begin -->
<div class="breadcrumb-bettix register-page">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="breadcrumb-content"></div>
                </div>
            </div>
        </div>
    </div>
<!-- breadcrumb end -->

<!-- regsiter begin -->
<div class="login">
    <div class="container">
        <!-- <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-8">
                <div class="section-title">
                    <h2>Bet360 Offers</h2>
                </div>
            </div>
        </div> -->
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="login-form">
                    <div class="part-text">
                        <h4 class="mb-2 mt-2">Bet360 Offers </h4>
                            <p class="text-justify">
                                Our core desire is to give you value at all time. This is why all our plans are designed in ways that will make it possible for you to benefit from one or more regardless of your pocket.
                                In simple terms, you get more value than what your money can get you elsewhere.
                                Free Unregistered has access to
                                <ol class="mb-4 ml-4">
                                    <li>
                                        FREE ACCA on the homepage
                                    </li>
                                    <li>
                                        Upcoming Predictions/Tips on the homepage
                                    </li>
                                </ol>
                            </p>
                            <p class="text-justify">
                                
                                This plan is designed for you if you are just starting out with us.
                                
                                <h6 class="mb-2 mt-2">Free Plan</h6>

                                The free plan is free to only the REGISTERED USERS and it contains the following categories (5)
                                <ol class="mb-4 ml-4">
                                    <li>
                                        DOUBLE CHANCE
                                    </li>
                                    <li>
                                        OVER 1.5
                                    </li>
                                    <li>
                                        UNDER 4.5
                                    </li>
                                    <li>
                                        FULL- TIME DRAW
                                    </li>
                                    <li>
                                        CORRECT SCORE
                                    </li>
                                </ol>
                                NOTE RESTRICTION: Only the 1st 5 entries are FREE in the entire category
                                The accuracy level is up to 65%.
                            </p> 

                            <h6 class="mb-2 mt-2">Silver Plan </h6>
                            <p class="text-justify">
                                The Silver Plan is SUBSCRIPTION BASED and contains the following categories including the Free Plan (14)
                            </p>
                            <ol class="mb-4 ml-4">
                                <li>
                                    HOME/ AWAY WIN EITHER HALF Predictions
                                </li>
                                <li>
                                    FIRST HALF GOALS Predictions
                                </li>
                                <li>
                                    OVER/UNDER 1.5 HT Predictions
                                </li>
                                <li>
                                    UNDER 3.5 Predictions
                                </li>
                                <li>
                                    FIRST HALF WINNER Predictions
                                </li>
                                <li>
                                    HALF TIME DRAW Predictions
                                </li>
                                <li>
                                    CORNER Predictions
                                </li>
                                <li>
                                   TAKE THE RISK Predictions
                                </li>
                                <li>
                                    VIEW ALL MATCHES (FT Predictions)
                                </li>
                            </ol>
                            <p class="text-justify"> The accuracy level is up to 75%. </p>

                            <h6 class="mb-2 mt-2">Gold Plan </h6> 
                            <p class="text-justify">
                                
                                The Gold Plan is SUBSCRIPTION BASED and contains the following categories including the Free &Silver Plan (24)
                            </p>
                            <ol class="mb-4 ml-4">
                                <li>
                                    BOTH TEAMS TO SCORE Predictions
                                </li>
                                <li>
                                    OVER 2.5 Predictions
                                </li>
                                <li>
                                    COMBO Predictions
                                </li>
                                <li>
                                    MULTI-GOAL Predictions
                                </li>
                                <li>
                                    MOST SCORING HALF Predictions
                                </li>
                                <li>
                                    HANDICAP Predictions
                                </li>
                                <li>
                                    HALFTIME-FULLTIME Predictions
                                </li>
                                <li>
                                   SURE 2 ODDS Accumulator
                                </li>
                                <li>
                                    SURE 3 ODDS Accumulator
                                </li>
                                <li>
                                    BET-BUILDER Accumulator
                                </li>
                            </ol>
                            <p class="text-justify">The accuracy level is up to 85%.</p>

                            <h6 class="mb-2 mt-2">Investment Plan</h6> 
                                <p class="text-justify">
                                    The investment plan is SUBSCRIPTION BASED and contains 2 categories as listed below
                                </p>
                                <ol class="mb-4 ml-4">
                                    <li>
                                        Investment Tips
                                    </li>
                                    <li>
                                        Investment Extra
                                    </li>
                                </ol>
                               <p class="text-justify"> Its odds range from 1.55 to 2.05 daily.This plan has been proven to have an accuracy level of up to 95%.
                                </p>

                                <h6 class="mb-2 mt-2">Expert Advisor/ Special Tips Advisor Plan</h6> 
                                <p class="text-justify">
                                The Special Tips Advisor Plan is SUBSCRIPTION BASED. It has no category on the website; we
                                interact directly with subscribers on either WhatsApp or Skype.
                                </p>
                                <h6 class="mb-2 mt-2"> BET-ROLLOVER Accumulator</h6> 
                                <p class="text-justify">
                                This category is very unique and valuable; it combines the best game of the day for a period of 7 days to accumulate to big odds.Each of the games daily is played with a continuous specific amount. Its odds range from 1.20 to 1.30 daily.
                                This plan has been proven to have an accuracy level of up to 95%.
                                </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
