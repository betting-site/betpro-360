@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All Plan Price
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a class="btn btn-primary text-white" data-toggle="modal" data-target="#addPrice">Add Plan Price </a>    
                </div>
            </div>
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Plan</th>
                                                    <th>Duration</th>
                                                    <th>Country</th>
                                                    <th>Price</th>
                                                    <th>ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($prices as $key => $price)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td class="product-name">{{ $price->name != '' ? $price->name : 'N/A'}}</td>
                                                    <td class="product-name">{{ $price->duration != '' ? $price->duration : 'N/A'}}</td>
                                                    <td class="product-name">{{ $price->country != '' ? $price->country : 'N/A'}}</td>
                                                    <td class="product-name">@money($price->amount != '' ? $price->amount : 'N/A')</td>
                                                    <td class="product-price">
                                                        <form method="post" action="{{route('delete.price', $price->id)}}">
                                                        @csrf
                                                           

                                                            <a data-toggle="modal" data-target="#editPrice" v-on:click="setCurrent({{json_encode($price, TRUE)}})">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                                            

                                                            <button type="submit" style="border:none; background: none"><i class="fa fa-trash text-danger"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

                <div class="modal fade text-left" id="addPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Add Price Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('create.price') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan Type</span>
                                                    </div>
                                                    <div class="col-md-8 ">
                                                        <select class="form-control @error('plan_name') is-invalid @enderror" name="plan_name" required>
                                                            
                                                            @foreach($plans as $plan)
                                                            <option value="{{$plan->id}}"> {{$plan->name}} </option>
                                                            @endforeach
                                                        </select>
                                                         
                                                        @error('plan_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan Duration</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select class="form-control @error('duration') is-invalid @enderror" name="duration" required>
                                                             
                                                            @foreach($durations as $duration)
                                                            <option value="{{$duration->id}}"> {{$duration->title}} </option>
                                                            @endforeach
                                                        </select>
                                                         
                                                        @error('duration')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Country</span>
                                                    </div>
                                                    <div class="col-md-8" >
                                                        <select class="form-control @error('country') is-invalid @enderror" name="country" required>

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}"> {{$country->name}} {{$country->currency}} </option>
                                                            @endforeach
                                                        </select>
                                                         
                                                        @error('country')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Amount</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('amount') is-invalid @enderror" name="amount" placeholder="Plan amount" autofocus required>
                                                        
                                                        @error('amount')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                 <div class="modal fade text-left" id="editPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Edit Plan Price </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('edit.price')}} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                         <div class="row">

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan Type</span>
                                                    </div>
                                                    <div class="col-md-8 ">
                                                        <select class="form-control @error('plan_name') is-invalid @enderror" name="plan_name" required>
                                                            <option :value="current.pid"> @{{current.name}} </option>
                                                            @foreach($plans as $plan)
                                                            <option value="{{$plan->id}}"> {{$plan->name}} </option>
                                                            @endforeach
                                                        </select>
                                                         
                                                        @error('plan_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan Duration</span>
                                                    </div>
                                                    <div class="col-md-8 select-type" data-type="duration">
                                                        <select class="form-control @error('duration') is-invalid @enderror" name="duration" required>
                                                            <option :value="current.pdid"> @{{current.duration}} </option>

                                                            @foreach($durations as $duration)
                                                            <option value="{{$duration->id}}"> {{$duration->title}} </option>
                                                            @endforeach
                                                        </select>
                                                         
                                                        @error('duration')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Country</span>
                                                    </div>
                                                    <div class="col-md-8 select-type" data-type="country">
                                                        <select class="form-control @error('country') is-invalid @enderror" name="country" required>

                                                            <option :value="current.cid"> @{{current.country}} </option>

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}"> {{$country->name}} {{$country->currency}} </option>
                                                            @endforeach
                                                        </select>
                                                         
                                                        @error('country')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Amount</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('amount') is-invalid @enderror" name="amount" placeholder="Plan amount" :value="current.amount" autofocus required>
                                                        
                                                        @error('amount')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="id" :value="current.id">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Update </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            
            </section>
            <!-- Data list view end -->
        </div>
    </div>
     
@endsection

