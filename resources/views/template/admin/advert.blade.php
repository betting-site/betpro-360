@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All Advert Setting
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a class="btn btn-primary text-white" data-toggle="modal" data-target="#addAdvert">Add Advert </a>    
                </div>
            </div>
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>#</th>
                                                    <th>country</th>
                                                    <th>location</th>
                                                    <th>destination url</th>
                                                    <th>view image</th>
                                                    <th>ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($adverts as $key => $advert)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td class="product-name">{{ $advert->name != '' ? $advert->name : 'N/A'}}</td>
                                                    <td class="product-name">{{ $advert->location_name != '' ? $advert->location_name : 'N/A'}}</td>
                                                    <td class="product-name">{{ $advert->dest_url != '' ? $advert->dest_url : 'N/A'}}</td>
                                                    <td class="product-name">
                                                        <a target="tabs" href='{{ asset("public/$advert->avatar") }}'>
                                                            <img height="50px" width="50px" src='{{ asset("public/$advert->avatar") }}' class="img-fluid mb-1 rounded-sm" alt="avatar">
                                                        </a>
                                                    <td class="product-price">
                                                        <form method="post" action="{{route('delete.advert', $advert->id)}}">
                                                        @csrf
                                                            

                                                            <a data-toggle="modal" data-target="#editInvest" v-on:click="setCurrent({{json_encode($advert, TRUE)}})">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                                            

                                                            <button type="submit" style="border:none; background: none"><i class="fa fa-trash text-danger"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

                <div class="modal fade text-left" id="addAdvert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Add Invest Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('create.advert') }} " enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">

                                                    <div class="col-md-4">
                                                        <span>Country</span>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <select name="country" class="form-control @error('country') is-invalid @enderror">
                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}} {{$country->symbol}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    @error('country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div> 
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Location Name</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="location_name" id="" class="form-control @error('location_name') is-invalid @enderror">
                                                            <option value="advert1">Advert 1mobile</option>
                                                            <option value="advert2">Advert 1desk</option>
                                                            <option value="advert3">Advert 2</option>
                                                            <option value="advert4">Advert 3</option>
                                                            <option value="advert5">Advert 4</option>
                                                            <option value="advert6">Advert 5</option>
                                                            <option value="advert7">Advert 6</option>
                                                            <option value="slider1">slider 1</option>
                                                            <option value="slider2">slider 2</option>
                                                            <option value="slider3">slider 3</option>
                                                            <option value="slider4">slider 4</option>
                                                            <option value="slider5">slider 5</option>
                                                            <option value="slider6">slider 6</option>
                                                            <option value="slider7">slider 7</option>
                                                            <option value="slider8">slider 8</option>
                                                            <option value="slider9">slider 9</option>
                                                            <option value="slider10">slider 10</option>
                                                        </select>
                                                        @error('location_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Image Upload</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input id="avatar" type="file" accept="image/*" class="form-control @error('avatar') is-invalid @enderror" name="avatar" autocomplete="avatar">

                                                        @error('avatar')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Destination URL</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="url" class="form-control @error('dest_url') is-invalid @enderror" name="dest_url" placeholder="dest_url" autofocus>
                                                        
                                                        @error('dest_url')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                 <div class="modal fade text-left" id="editInvest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Edit Advert Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('edit.advert')}} " enctype="multipart/form-data">
                                @csrf

                               <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">

                                                    <div class="col-md-4">
                                                        <span>Country</span>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <select name="country" class="form-control @error('country') is-invalid @enderror">
                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}} {{$country->symbol}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    @error('country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div> 
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Location Name</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="location_name" id="" class="form-control @error('location_name') is-invalid @enderror">
                                                            <option value="advert1">Advert 1mobile</option>
                                                            <option value="advert2">Advert 1desk</option>
                                                            <option value="advert3">Advert 2</option>
                                                            <option value="advert4">Advert 3</option>
                                                            <option value="advert5">Advert 4</option>
                                                            <option value="advert6">Advert 5</option>
                                                            <option value="advert7">Advert 6</option>
                                                            <option value="slider1">slider 1</option>
                                                            <option value="slider2">slider 2</option>
                                                            <option value="slider3">slider 3</option>
                                                            <option value="slider4">slider 4</option>
                                                            <option value="slider5">slider 5</option>
                                                            <option value="slider6">slider 6</option>
                                                            <option value="slider7">slider 7</option>
                                                            <option value="slider8">slider 8</option>
                                                            <option value="slider9">slider 9</option>
                                                            <option value="slider10">slider 10</option>
                                                        </select>
                                                        @error('location_name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Image Upload</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input id="avatar" type="file" accept="image/*" class="form-control @error('avatar') is-invalid @enderror" name="avatar" autocomplete="avatar">

                                                        @error('avatar')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Destination URL</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="url" class="form-control @error('dest_url') is-invalid @enderror" name="dest_url" :value="current.dest_url" placeholder="dest_url" autofocus>
                                                        
                                                        @error('dest_url')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                        
                                            <input type="hidden" name="id" :value="current.id">
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Update </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Data list view end -->
        </div>
    </div>
     
@endsection

