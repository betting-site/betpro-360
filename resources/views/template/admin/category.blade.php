@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All categories
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a class="btn btn-primary text-white" data-toggle="modal" data-target="#addCategory">Add categories</a>  
                </div>
            </div>
            <!-- @can('create category')@endcan -->
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>title</th>
                                                    <th>plan</th>
                                                    <th>description</th>
                                                    <th>special</th>
                                                    <th>status</th>
                                                    <th width="150px">ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($categories as $key => $category)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td class="product-name">{{ $category->title != '' ? $category->title : 'N/A'}}</td>
                                                    <td class="product-name">{{ $category->plan_id != '' ? $category->plan_id : 'N/A'}}</td>
                                                    <td class="product-name">{{ $category->description != '' ? $category->description : 'N/A'}}</td>
                                                    <td>
                                                        <span class="badge badge-pill badge-{{ $category->special == 'yes' ? 'success' : 'danger'}}" style="padding: 0.6rem">{{$category->special}}</span>                        
                                                    </td>
                                                    <td>
                                                        <span class="badge badge-pill badge-{{ $category->status == 'active' ? 'success' : 'danger'}}" style="padding: 0.6rem">{{$category->status}}</span>                        
                                                    </td>
                                                    <td class="product-price">
                                                        <form method="post" action="{{route('delete.category', $category->id)}}">
                                                        @csrf
                                                            
                                                            <a data-toggle="modal" data-target="#editCategory" v-on:click="setCurrent({{json_encode($category, TRUE)}})">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                                            

                                                            <button type="submit" style="border:none; background: none"><i class="fa fa-trash text-danger"></i></button>
                                                           <!-- @can('edit category') @endcan -->

                                                            
                                                            <!-- @can('delete category')@endcan -->
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

                <div class="modal fade text-left" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Add Catgory Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('create.category') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Catgory title</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Catgory title" autofocus required>
                                                        
                                                        @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan </span>
                                                    </div>
                                                    <div class="col-md-8 select-type" data-type="plan_id">
                                                        <select class="select2-ajax form-control @error('plan_id') is-invalid @enderror" name="plan_id[]" id="select2-ajax" multiple="multiple" required></select>

                                                        @error('plan_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Description </span>
                                                    </div>
                                                    <div class="col-md-8">
                                                       <textarea class="form-control @error('description') is-invalid @enderror" name="description"></textarea>

                                                        @error('description')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Specials</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="special" id="" class="form-control @error('special') is-invalid @enderror">
                                                            <option value="no">No</option>
                                                            <option value="yes">Yes</option>
                                                        </select>
                                                        @error('special')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Status</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="status" id="" class="form-control @error('status') is-invalid @enderror">
                                                            <option value="active">Active</option>
                                                            <option value="inactive">Inactive</option>
                                                        </select>
                                                        @error('status')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                 <div class="modal fade text-left" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Edit Category Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('edit.category') }} ">
                                @csrf
                                <div class="modal-body">

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Catgory title</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" :value="current.title" placeholder="Catgory title" autofocus required>
                                                        
                                                        @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan </span>
                                                    </div>
                                                    <div class="col-md-8 select-type" data-type="plan_id">
                                                        <select class="select2-ajax form-control @error('plan_id') is-invalid @enderror" name="plan_id[]" id="select2-ajaxx" multiple="multiple" required></select>

                                                        @error('plan_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Description </span>
                                                    </div>
                                                    <div class="col-md-8">
                                                       <textarea class="form-control @error('description') is-invalid @enderror" name="description">@{{current.description}}</textarea>

                                                        @error('description')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Specials</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="special" id="" class="form-control @error('special') is-invalid @enderror">
                                                            <option value="no">no</option>
                                                            <option value="yes">yes</option>
                                                        </select>
                                                        @error('special')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Status</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="status" id="" class="form-control @error('status') is-invalid @enderror">
                                                            <option value="active">Active</option>
                                                            <option value="inactive">Inactive</option>
                                                        </select>
                                                        @error('status')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="id" :value="current.id">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Update </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            
            </section>
            <!-- Data list view end -->
        </div>
    </div>
@endsection

