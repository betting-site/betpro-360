@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All transactions
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a class="btn btn-primary text-white" data-toggle="modal" data-target="#addTransaction">Add transactions</a>    
                </div>
            </div>
            <!-- @can('create transaction') -->
            <!-- @endcan -->
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>User ID</th>
                                                    <th>Plan Name</th>
                                                    <th>Amount</th>
                                                    <th>Country</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Transaction Method </th>
                                                    <th>Status</th>  
                                                    <th>ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($transactions as $key => $transaction)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td class="product-name">{{ $transaction->user_id != '' ? $transaction->user_id : 'N/A'}}</td>
                                                    <td class="product-name">{{ $transaction->name != '' ? $transaction->name : 'N/A'}}</td>
                                                    <td class="product-name">{{$transaction->amount != '' ? $transaction->amount : 'N/A'}}</td>
                                                    <td class="product-name">{{ $transaction->cname != '' ? $transaction->cname : 'N/A'}}</td>
                                                    <td class="product-name">{{ $transaction->start_date != '' ? $transaction->start_date : 'N/A'}}</td>
                                                    <td class="product-name">{{ $transaction->end_date != '' ? $transaction->end_date : 'N/A'}}</td>
                                                    <td class="product-name">{{ $transaction->payment_type != '' ? $transaction->payment_type : 'N/A'}}</td>
                                                    <td>
                                                        <span class="badge badge-pill badge-{{ $transaction->status == 'success' ? 'success' : 'danger'}}" style="padding: 0.6rem">{{$transaction->status}}</span>                        
                                                    </td>
                                                    <td class="product-price">
                                                        <!-- @csrf -->
                                                            <a data-toggle="modal" data-target="#editTransaction"  v-on:click="setCurrent({{json_encode($transaction, TRUE)}})">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                                            <!-- @can('edit transaction') -->
                                                            <!-- @endcan -->

                                                            <!-- @can('delete transaction') -->
                                                            <!-- <button type="submit" style="border:none; background: none">
                                                                <i class="fa fa-trash text-danger"></i>
                                                            </button> -->
                                                            <!-- @endcan -->
                                                        <!-- </form> -->
                                                    </td>
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

                <div class="modal fade text-left" id="addTransaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Add Transaction Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('create.subscription') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Subscriber Name</span>
                                                    </div>
                                                    <div class="col-md-8 select-type" data-type="user_id">
                                                        <select class="select2-data-ajax form-control @error('user_id') is-invalid @enderror" name="user_id[]" id="select2-ajax" multiple="multiple" required></select>
                                                         
                                                        @error('user_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan Type</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="plan_id" class="form-control @error('plan_id') is-invalid @enderror">
                                                            @foreach($plans as $plan)
                                                            <option value="{{$plan->id}}">{{$plan->name}}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('plan_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Amount</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('amount') is-invalid @enderror" name="amount" placeholder="Amount" autofocus required>
                                                        
                                                        @error('amount')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Payment Type</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="payment_type" id="" class="form-control @error('payment_type') is-invalid @enderror">
                                                            <option value="Transfer">Transfer</option>
                                                        </select>
                                                        @error('payment_type')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Start Date</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type='date' class="form-control @error('start_date') is-invalid @enderror" name="start_date" placeholder="start_date">        
                                                        @error('start_date')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>End Date</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type='date' class="form-control @error('end_date') is-invalid @enderror" name="end_date" placeholder="end_date">        
                                                        @error('end_date')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Status</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="status" id="" class="form-control @error('status') is-invalid @enderror">
                                                            <option value="success">Success</option>
                                                            <option value="fail">Fail</option>
                                                        </select>
                                                        @error('status')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                 <div class="modal fade text-left" id="editTransaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Edit Transaction Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('edit.subscription') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Plan Type</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="plan_id" class="form-control @error('plan_id') is-invalid @enderror">

                                                            @foreach($plans as $plan)
                                                            <option value="{{$plan->id}}">{{$plan->name}}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('plan_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Amount</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('amount') is-invalid @enderror" name="amount" :value="current.amount" placeholder="Amount" autofocus required>
                                                        
                                                        @error('amount')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Payment Type</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="payment_type" class="form-control @error('payment_type') is-invalid @enderror">
                                                            <option value="Transfer">Transfer</option>
                                                        </select>
                                                        @error('payment_type')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Start Date</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type='date' class="form-control @error('start_date') is-invalid @enderror" name="start_date" placeholder="start_date">        
                                                        @error('start_date')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>End Date</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type='date' class="form-control @error('end_date') is-invalid @enderror" name="end_date" placeholder="end_date">        
                                                        @error('end_date')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Status</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="status" class="form-control @error('status') is-invalid @enderror">
                                                            <option value="success">Success</option>
                                                            <option value="fail">Fail</option>
                                                        </select>
                                                        @error('status')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="id" :value="current.id">
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Update </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            
            </section>
            <!-- Data list view end -->
        </div>
    </div>
    
    <!-- @can('transaction')
    @endcan -->
     
@endsection

