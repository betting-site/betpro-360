@extends('layouts.admin.dashboard')

@section('content')

    <div class="content-body">
        <section id="dashboard-analytics"> 
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-start pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-primary font-medium-5"></i>
                                </div>
                            </div>

                            <h2 class="text-bold-700 mt-2 mb-2">{{$users['all_user'] != '' ? $users['all_user'] : 'N/A'}}</h2>
                            <a href="{{route('user')}}}" class="text-bold-500 h4 mb-2" style="color: #7367f0">Active Users</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-start pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="fa fa-users text-primary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700 mt-2 mb-2">{{$users['subscription'] != '' ? $users['subscription'] : 'N/A'}}</h2>
                            <a href="{{route('subscription')}}" class="text-bold-500 h4 mb-2" style="color: #7367f0">Active Subscribers</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-start pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="feather icon-credit-card text-primary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700 mt-2 mb-2"> @if($users['amount']) @money($users['amount'] != '' ? $users['amount'] : 'N/A') @else {{'₦'. ' 0.00'}} @endif</h2>
                            <a href="{{route('subscription')}}" class="text-bold-500 h4 mb-2" style="color: #7367f0">Amount Recieved</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex flex-column align-items-start pb-0">
                            <div class="avatar bg-rgba-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i class="fa fa-yelp text-primary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700 mt-2 mb-2">{{$users['plan'] != '' ? $users['plan'] : 'N/A'}}</h2>
                            <a href="{{route('plan')}}" class="text-bold-500 h4 mb-2" style="color: #7367f0">All Plans</a>
                        </div>
                    </div>
                </div>
            </div> 
        </section> 

        <section id="basic-datatable">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <!-- DataTable starts -->
                                <h3>Recent Users</h3>
                                <div class="table-responsive">
                                    <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>firstname</th>
                                                <th>userid</th>
                                                <th>country</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        @foreach ($users['users'] as $key => $user)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td class="product-name">{{ $user->firstname != '' ? $user->firstname : 'N/A'}}</td>
                                                <td class="product-name">{{ $user->user_id != '' ? $user->user_id : 'N/A'}}</td>
                                                <td class="product-name">{{ $user->cname != '' ? $user->cname : 'N/A'}}</td>
                                            </tr>
                                        @endforeach   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <!-- DataTable starts -->
                                <h3>Recent Transactions</h3>
                                <div class="table-responsive">
                                    <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>id</th>
                                                <th>plan</th>
                                                <th>amount</th>
                                                <th>start</th>
                                                <th>end </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users['transactions'] as $key => $transaction)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td class="product-name">{{ $transaction->user_id != '' ? $transaction->user_id : 'N/A'}}</td>
                                                <td class="product-name">{{ $transaction->name != '' ? $transaction->name : 'N/A'}}</td>
                                                <td class="product-name">@money($transaction->amount != '' ? $transaction->amount : 'N/A')</td>
                                                <td class="product-name">

                                                 {{(Carbon\Carbon::parse($transaction->start_date)->format('d-m-yy')) ? (Carbon\Carbon::parse($transaction->start_date)->format('d-m-yy')) : 'N/A' }}</td>

                                                <td class="product-name">{{(Carbon\Carbon::parse($transaction->end_date)->format('d-m-yy')) ? (Carbon\Carbon::parse($transaction->end_date)->format('d-m-yy')) : 'N/A' }}</td>
                                            </tr>
                                        @endforeach   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

