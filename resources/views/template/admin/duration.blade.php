@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All Plan Duration
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a class="btn btn-primary text-white" data-toggle="modal" data-target="#addDuration">Add Plan Duration</a>    
                </div>
            </div>
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>title</th>
                                                    <th>duration type</th>
                                                    <th>ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($durations as $key => $duration)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td class="product-name">{{ $duration->title != '' ? $duration->title : 'N/A'}}</td>
                                                    <td class="product-name">{{ $duration->duration_type != '' ? $duration->duration_type : 'N/A'}}</td>
                                                    <td class="product-price">
                                                        <form method="post" action="{{route('delete.duration', $duration->id)}}">
                                                        @csrf
                                                            <a data-toggle="modal" data-target="#editDuration" v-on:click="setCurrent({{json_encode($duration, TRUE)}})">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                                            

                                                            <button type="submit" style="border:none; background: none"><i class="fa fa-trash text-danger"></i></button>

                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

                <div class="modal fade text-left" id="addDuration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Add Duration Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('create.duration') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Duration Name</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Duration Name" autofocus required>
                                                        
                                                        @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Duration Type</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('duration_type') is-invalid @enderror" name="duration_type" placeholder="2 weeks , 6 months, 2 years " autofocus required>
                                                        
                                                        @error('duration_type')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                 <div class="modal fade text-left" id="editDuration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Edit Duration Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('edit.duration') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                       <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Duration Name</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" :value="current.title" placeholder="Duration Name" autofocus required>
                                                        
                                                        @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Duration Type</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('duration_type') is-invalid @enderror" name="duration_type" :value="current.duration_type" placeholder="2 weeks , 6 months, 2 years " autofocus required>
                                                        
                                                        @error('duration_type')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="id" :value="current.id">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Update </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            
            </section>
            <!-- Data list view end -->
        </div>
    </div>
     
@endsection

