@extends('layouts.admin.dashboard')

@section('content')

    <div class="content-body">
        <section id="dashboard-analytics"> 
            <div class="row">
                <!-- <div class="col-lg-3 col-md-12 col-sm-12"></div> -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content">  
                            <div class="card-body card-dashboard">
                                <div class="d-flex justify-content-start align-items-center mb-1">
                                    <div class="user-page-info">
                                        <h3 class="mb-3">{{isset($prediction) ? 'Edit' : 'Add'}} Match Prediction</h3>
                                    </div>
                                </div>

                                <form class="form form form-vertical" method="POST" action="{{ route('create.prediction') }} ">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Teams</label>
                                                    <input type="text" class="form-control @error('teams') is-invalid @enderror" name="teams" value="{{ isset($prediction->teams) ? $prediction->teams : '' }}" placeholder="Home team" autofocus required>
                                                    
                                                    @error('teams')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">League</label>
                                                    <select name="league_id" class="form-control @error('league_id') is-invalid @enderror">
                                                        <option value="">-Select League-</option>
                                                        @foreach($leagues as $league)
                                                        <option value="{{$league->id}}">{{$league->nickname}}</option>
                                                        @endforeach
                                                    </select>

                                                    @error('league_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Upcoming prediction</label>
                                                    <select class="form-control" name="upcoming">
                                                        <option value="Yes"> Yes</option>
                                                        <option value="No"> No </option>
                                                    </select>
                                                    
                                                    @error('upcoming')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Free Acca</label>
                                                    <select class="form-control" name="free_acca">
                                                        <option value="Yes"> Yes</option>
                                                        <option value="No"> No </option>
                                                    </select>
                                                    
                                                    @error('winning_tips')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                           

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Home Form</label>
                                                    <input type="text" class="form-control @error('home_form') is-invalid @enderror" name="home_form" value="{{ isset($prediction->home_form) ? $prediction->home_form : '' }}" placeholder="Home Form" autofocus maxlength="5">
                                                    
                                                    @error('home_form')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Away Form</label>
                                                    <input type="text" class="form-control @error('away_form') is-invalid @enderror" name="away_form" value="{{ isset($prediction->away_form) ? $prediction->away_form : '' }}" placeholder="Away Form" autofocus maxlength="5">
                                                    
                                                    @error('away_form')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Home Odd</label>
                                                    <input type="text" class="form-control @error('home_odd') is-invalid @enderror" name="home_odd" value="{{ isset($prediction->home_odd) ? $prediction->home_odd : '' }}" placeholder="Home Odd" autofocus >
                                                    
                                                    @error('home_odd')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Draw Odd</label>
                                                    <input type="text" class="form-control @error('draw_odd') is-invalid @enderror" name="draw_odd" value="{{ isset($prediction->draw_odd) ? $prediction->draw_odd : '' }}" placeholder="Draw Odd" autofocus >
                                                    
                                                    @error('draw_odd')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Away Odd</label>
                                                    <input type="text" class="form-control @error('away_odd') is-invalid @enderror" name="away_odd" value="{{ isset($prediction->away_odd) ? $prediction->away_odd : '' }}" placeholder="Away Odd" autofocus >
                                                    
                                                    @error('away_odd')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Match Date</label>
                                                    <input type="date" class="form-control @error('match_date') is-invalid @enderror" value="" name="match_date" placeholder="Match Date" required>        
                                                    @error('match_date')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Match Time</label>
                                                    <input type="text" class="form-control @error('time') is-invalid @enderror" name="time" value="{{ isset($prediction->match_date) ? Carbon\Carbon::parse($prediction->match_date)->isoFormat('H:MM') : ''}}" placeholder="10:00">        
                                                    @error('time')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Preview</label>
                                                    <input type="text" class="form-control @error('preview') is-invalid @enderror" name="preview" value="{{ isset($prediction->preview) ? $prediction->preview : '' }}" placeholder="Preview" autofocus >
                                                    
                                                    @error('preview')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">FT Recommendation</label>
                                                    <input type="text" class="form-control @error('ft_recommendation') is-invalid @enderror" name="ft_recommendation" value="{{ isset($prediction->ft_recommendation) ? $prediction->ft_recommendation : '' }}" placeholder="FT recommendation" autofocus >
                                                    
                                                    @error('ft_recommendation')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Result</label>
                                                    <select class="form-control" name="result">
                                                        <option value="neutral"> Neutral</option>
                                                        <option value="good"> Good</option>
                                                        <option value="bad">Bad</option>
                                                    </select>
                                                    
                                                    @error('result')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Winning Tips</label>
                                                    <select class="form-control" name="winning_tips">
                                                        <option value="Yes"> Yes</option>
                                                        <option value="No"> No </option>
                                                    </select>
                                                    
                                                    @error('winning_tips')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            @if(!isset($prediction))
                                                @foreach($categories as $category)
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical" class="text-uppercase">{{$category->title}}</label>
                                                        <input type="hidden" name="cat_id[]" value="{{$category->id}}">
                                                        <input type="text" class="form-control @error('{{$category->nickname}}') is-invalid @enderror" name="recommendation[]" placeholder="{{$category->title}}" autofocus >
                                                        
                                                        @error('{{$category->nickame}}')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                    @if ($category->id > 19)
                                                        @break
                                                    @endif
                                                @endforeach

                                            @else

                                                @foreach($edit_preds as $category)
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical" class="text-uppercase">{{$category->title}}</label>
                                                        <input type="hidden" name="cat_id[]" value="{{$category->id}}">
                                                        <input type="text" class="form-control @error('{{$category->nickname}}') is-invalid @enderror" name="recommendation[]" value="{{$category->recommendation}}" placeholder="{{$category->title}}" autofocus >
                                                        
                                                        @error('{{$category->nickame}}')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                    @if ($category->id > 19)
                                                        @break
                                                    @endif
                                                @endforeach


                                            @endif

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">SURE 2</label>
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <input type="number" class="form-control @error('sure2') is-invalid @enderror" name="recommendation[]" value="" placeholder="Recommendation" autofocus >
                                                            
                                                            @error('sure2')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-4">
                                                            
                                                            <input type="text" class="form-control @error('sure2_odd') is-invalid @enderror" name="sure2_odd" value="{{ isset($prediction->sure2_odd) ? $prediction->sure2_odd : '' }}" placeholder="1ST SET ODDS" autofocus>
                                                            
                                                            @error('sure2_odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-4">
                                                            
                                                            <input type="text" class="form-control @error('sure2_2odd') is-invalid @enderror" name="sure2_2odd" value="{{ isset($prediction->sure2_2odd) ? $prediction->sure2_2odd : '' }}" placeholder="2ND SET ODDS" autofocus>
                                                            
                                                            @error('sure2_2odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cat_id[]" value="21">
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">SURE 3</label>
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <input type="number" class="form-control @error('sure3') is-invalid @enderror" name="recommendation[]" value="" placeholder="Recommendation" autofocus >
                                                            
                                                            @error('sure3')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-4">
                                                            
                                                            <input type="text" class="form-control @error('sure3_odd') is-invalid @enderror" name="sure3_odd" value="{{ isset($prediction->sure3_odd) ? $prediction->sure3_odd : '' }}" placeholder="1ST SET ODDS" autofocus>
                                                            
                                                            @error('sure3_odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-4">
                                                            
                                                            <input type="text" class="form-control @error('sure3_2odd') is-invalid @enderror" name="sure3_2odd" value="{{ isset($prediction->sure3_2odd) ? $prediction->sure3_2odd : '' }}" placeholder="2ND SET ODDS" autofocus>
                                                            
                                                            @error('sure3_2odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cat_id[]" value="22">
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Bet Builder</label>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input type="number" class="form-control @error('betbuilder') is-invalid @enderror" name="recommendation[]" value="" placeholder="Recommendation" autofocus >
                                                            
                                                            @error('betbuilder')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <div class="col-6">
                                                            
                                                            <input type="text" class="form-control @error('betbuilder_odd') is-invalid @enderror" name="betbuilder_odd" value="{{ isset($prediction->betbuilder_odd) ? $prediction->betbuilder_odd : '' }}" placeholder="Bet Builder Odds" autofocus>
                                                            
                                                            @error('betbuilder_odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cat_id[]" value="23">
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Investment</label>
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <input type="number" class="form-control @error('investment') is-invalid @enderror" name="recommendation[]" value=""  placeholder="Recommendation" autofocus >
                                                            
                                                            @error('investment')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-4">
                                                            
                                                            <input type="text" class="form-control @error('investment_odd') is-invalid @enderror" name="investment_odd" value="{{ isset($prediction->investment_odd) ? $prediction->investment_odd : '' }}"  placeholder="1ST SET ODDS" autofocus>
                                                            
                                                            @error('investment_odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-4">
                                                            
                                                            <input type="text" class="form-control @error('investment_2odd') is-invalid @enderror" name="investment_2odd" value="{{ isset($prediction->investment_2odd) ? $prediction->investment_2odd : '' }}"  placeholder="2ND SET ODDS" autofocus>
                                                            
                                                            @error('investment_2odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cat_id[]" value="24">
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="first-name-vertical" class="text-uppercase">Bet RollOver</label>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input type="number" class="form-control @error('betrollover') is-invalid @enderror" name="recommendation[]" value="" placeholder="Recommendation" autofocus >
                                                            
                                                            @error('betrollover')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <div class="col-6">
                                                            
                                                            <input type="text" class="form-control @error('betrollover_odd') is-invalid @enderror" name="betrollover_odd" value="{{ isset($prediction->betrollover_odd) ? $prediction->betrollover_odd : '' }}" placeholder="Bet Rollover Odds" autofocus>
                                                            
                                                            @error('betrollover_odd')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cat_id[]" value="25">
                                            </div>

                                            <input type="hidden" name="id" value="{{isset( $prediction->id) ? ( $prediction->id) : ''}}">
                                        
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mx-auto d-block">{{isset($prediction) ? 'Update' : 'Add'}} Prediction</button>
                                            </div>   
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="col-2"></div>
            </div> 
        </section> 
    </div>

@endsection

