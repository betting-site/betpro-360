@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All Prediction
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a href="{{route('addprediction')}}" class="btn btn-primary text-white">Add Prediction</a>    
                </div>
            </div>
            <!-- @can('create prediction') -->
            <!-- @can('prediction') -->
            <!-- @endcan -->
            <!-- @endcan -->
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th> Match Date</th>
                                                    <th>league</th>
                                                    <th>Teams</th> 
                                                    <th width="150px">ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($predictions as $key => $prediction)
                                                <tr>
                                                    <td>{{$key+1}}</td>

                                                    <td class="product-name">{{ $prediction->match_date != '' ? $prediction->match_date : 'N/A'}}</td>
                                                    <td class="product-name">{{ $prediction->nickname != '' ? $prediction->nickname : 'N/A'}}</td>
                                                    <td class="product-name">{{ $prediction->teams != '' ? $prediction->teams : 'N/A'}}</td>

                                                    
                                                    <td class="product-price">
                                                        <form method="post" action="{{route('delete.prediction', $prediction->id)}}">
                                                        @csrf

                                                            <a href="{{route('addprediction', $prediction->id)}}">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                            
                                                            <!-- @can('edit prediction') -->
                                                            <!-- @endcan -->

                                                            <button type="submit" style="border:none; background: none"><i class="fa fa-trash text-danger"></i></button>
                                                            <!-- @can('delete prediction') -->
                                                            <!-- @endcan -->
                                                        </form>
                                                    </td> 
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

            </section>
            <!-- Data list view end -->
        </div>
    </div>
    
@endsection

