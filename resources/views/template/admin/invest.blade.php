@extends('layouts.admin.dashboard')
@section('mystyle')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        nav{
        display: inline-block !important; 
    }
    </style>
@endsection


@section('content')
    <div id="app">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Admin</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin')}}">Admin</a>
                                </li>
                                <li class="breadcrumb-item active">All Invest Time
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <a class="btn btn-primary text-white" data-toggle="modal" data-target="#addInvest">Add Invest Time</a>    
                </div>
            </div> -->
        </div>


        @if(session('status'))
        <div id="toast-container" class="toast-container toast-top-right alert-dismissible" role="alert" >
            <div class="toast toast-success" aria-live="polite" style="display: block;" >
                <div class="toast-title">
                    Notification
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="toast-message"> {{ session('status') }}</div>
            </div>
        </div>
        @endif

        <div class="content-body">
            <!-- Data list view starts -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <!-- DataTable starts -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration table-responsive-sm table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>title</th>
                                                    <th>description</th>
                                                    <th>button</th>
                                                    <th>timer</th>
                                                    <th>ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($investtime as $key => $invest)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td class="product-name">{{ $invest->title != '' ? $invest->title : 'N/A'}}</td>
                                                    <td class="product-name">{{ $invest->desc != '' ? $invest->desc : 'N/A'}}</td>
                                                    <td class="product-name">{{ $invest->button != '' ? $invest->button : 'N/A'}}</td>
                                                    <td class="product-name">{{ $invest->timer != '' ? $invest->timer : 'N/A'}}</td>
                                                    <td class="product-price">
                                                        <form method="post" action="{{route('delete.invest', $invest->id)}}">
                                                        @csrf
                                                        

                                                            <a data-toggle="modal" data-target="#editInvest" v-on:click="setCurrent({{json_encode($invest, TRUE)}})">
                                                                <i class="fa fa-pencil text-info"></i>
                                                            </a>
                                                            

                                                            <button type="submit" style="border:none; background: none"><i class="fa fa-trash text-danger"></i></button>

                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DataTable ends -->

                <div class="modal fade text-left" id="addInvest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Add Invest Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('create.invest') }} ">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Title</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Invest Title" autofocus required>
                                                        
                                                        @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Button</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('button') is-invalid @enderror" name="button" placeholder="Button" autofocus required>
                                                        
                                                        @error('button')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Timer</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="time" class="form-control @error('timer') is-invalid @enderror" name="timer" placeholder="Timer" autofocus required>
                                                        
                                                        @error('timer')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Description</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control form-control @error('status') is-invalid @enderror" name="desc"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                 <div class="modal fade text-left" id="editInvest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel33">Edit Invest Info</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form form-horizontal" method="POST" action="{{route('edit.invest') }} ">
                                @csrf

                               <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Title</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" :value="current.title" placeholder="Invest Title" autofocus required>
                                                        
                                                        @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Button</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control @error('button') is-invalid @enderror" name="button" :value="current.button" placeholder="Button" autofocus required>
                                                        
                                                        @error('button')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Timer</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="datetime-local" class="form-control @error('timer') is-invalid @enderror" name="timer" :value="current.timer" placeholder="Timer" autofocus required>
                                                        
                                                        @error('timer')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Description</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control form-control @error('desc') is-invalid @enderror" name="desc">@{{current.desc}}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="id" :value="current.id">
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Update </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Data list view end -->
        </div>
    </div>
     
@endsection

