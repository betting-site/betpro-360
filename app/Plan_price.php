<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class Plan_price extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'country_id', 'duration_id', 'plan_id',
    ];

    public function duration()
    {
        return $this->hasMany(Plan_duration::class);
    }

    public static function plan_title($plan_id){

        return DB::table('plans as p')
        ->select(DB::raw('pd.id, pd.title'))
        ->join('plan_prices as pp', 'pp.plan_id', '=', 'p.id')
        ->join('plan_durations as pd', 'pd.id', '=', 'pp.duration_id')
        ->where('p.id', $plan_id)
        ->groupBy('pd.title', 'pd.id')
        ->get()->toArray();
        

    }

    public static function all_plan_price($plan_id, $pd_id, $con_id){

        return DB::table('plan_prices as p')
        ->select(DB::raw('p.price'))
        ->where('p.plan_id', $plan_id)
        ->where('p.country_id', $con_id)
        ->where('p.duration_id', $pd_id)
        ->first();
        

    }

    public static function unique_plan_price($country, $plan_id, $plan_duration){
        return DB::table('plan_prices as pp')
            ->select(DB::raw('pp.id, pp.price'))
            ->join('plan_durations as pd', 'pd.id', 'pp.duration_id')
            ->where('pp.country_id',$country)
            ->where('pp.plan_id', $plan_id)
            ->where('pd.duration_type', $plan_duration)
            ->first();
    }

    
}
