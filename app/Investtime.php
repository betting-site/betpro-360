<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investtime extends Model
{
    
    protected $fillable = ['title', 'desc', 'button', 'timer'];

}
