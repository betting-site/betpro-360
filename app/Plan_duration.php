<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan_duration extends Model
{
    protected $fillable = [
        'title', 'duration_type'
    ];

    public function duration()
    {
        return $this->hasMany(Plan_price::class);
    }
}
