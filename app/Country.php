<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    protected $fillable = ['name', 'currency', 'symbol'];


    public function country()
    {
        return $this->hasOne(User::class, 'country_id');
    }
}
