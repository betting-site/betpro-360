<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    
    protected $fillable = ['result_date', 'expert_id', 'status', 'result_type', 'remark'];

}
