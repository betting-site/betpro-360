<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Plan extends Model
{
    
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function price()
    {
        return $this->hasMany(Plan_price::class);
    }

    public function duration()
    {
        return $this->hasMany(Plan_price::class);
    }

}
