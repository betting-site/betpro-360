<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Cat_prediction extends Model
{
 	use SoftDeletes;
    
    protected $fillable = ['pid' , 'cat_id', 'recommendation'];
}

