<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
 	use SoftDeletes;

    protected $fillable = ['title' , 'nickname', 'plan_id' , 'description', 'special', 'status'];


    public function prediction()
    {
    	return $this->hasMany(Prediction::class, 'cat_id');
    }

    public function league(){
    	return $this->hasMany(League::class, 'id');
    }
}
