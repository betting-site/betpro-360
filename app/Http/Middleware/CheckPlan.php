<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;

use Closure;

class CheckPlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $check_plan = DB::table('subscriptions')->select(DB::raw('user_id, start_date, end_date, amount, status'))->where('user_id', Auth::user()->user_id)->where('status','success')->orderBy('start_date', 'desc')->first();

        $today = date_create(date("Y-m-d"));

        $plan_date = date_create($check_plan->end_date);

        $date_diff = date_diff($today, $plan_date)->format("%R%a");
        
        // dd($date_diff < 0);

        if($date_diff <= 0){
            
            $update_plan = User::findOrFail(Auth::user()->id);

            $update_plan->plan_id = 1;

            $update_plan->save();

            return redirect()->route('dashboard')->with('status',"Plan Expired! Subscribe");

        }else{

            return $next($request);

        }
    }
}
