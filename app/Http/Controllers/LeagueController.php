<?php

namespace App\Http\Controllers;

use App\League;
use Illuminate\Http\Request;
use Auth\Validator;

class LeagueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $league = League::all();

        return view('template.admin.league', ['leagues'=> $league]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string', 'max:255' ], 
        ]);

        $league = League::create([
            'title' => $request['title'], 
            'nickname' => $request['nickname'], 
        ]);
        
        return redirect()->route('league')->with('status', 'League created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function show(League $league)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $league = League::findOrFail($request->id);
        $league->title = $request->title;
        $league->nickname = $request->nickname;
        $league->save();

        return redirect()->route('league')->with('status', 'League Updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, League $league)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $league = League::find($request->id)->delete();

        return redirect()->route('league')->with('status', 'League deleted');


    }
}
