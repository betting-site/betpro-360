<?php

namespace App\Http\Controllers;

use App\Plan;
use App\Category;
use Illuminate\Http\Request;
use Auth\Validator;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plan = Plan::all();

        return view('template.admin.plan', ['plans'=> $plan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255' ], 
        ]);

        $plan = new Plan();

        $plan->name = $request->name;

        $plan->save();
        
        return redirect()->route('plan')->with('status', 'Plan created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $plan = substr($request->plan_id, -1);

        switch($plan){

            case "1":
                $plan_feature = Category::where('plan_id', '1')->get();

            break;

            case "2":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2');
                        })->get();
            break;

            case "3":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2')
                                    ->orWhere('plan_id', '1,2,3');
                        })->get();
                
            break;

            case "4":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2')
                                    ->orWhere('plan_id', '1,2,3');
                        })->get();
                
            break;          
        }

        $plan_feature = [];

        foreach ($tags->unique('firstname') as $tag) {
            
            $plan_feature[] = ['id' => $tag->id, 'text' => $tag->firstname];

        }

        return response()->json($plan_feature, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $plan = Plan::findOrFail($request->id);
        $plan->name = $request->name;
        $plan->save();

        return redirect()->route('plan')->with('status', 'Plan Updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plans $plans)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plans  $plans
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $plan = Plan::find($request->id)->delete();

        return redirect()->route('plan')->with('status', 'Plan deleted');
    }
}
