<?php

namespace App\Http\Controllers;

use App\Plan_duration;
use Illuminate\Http\Request;
use Auth\Validator;

class PlandurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $duration = Plan_duration::all();

        return view('template.admin.duration', ['durations'=> $duration]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            
            'title' => ['required'], 

            'duration_type' => ['required']
        ]);

        $duration = new Plan_duration();

        $duration->title = $request->title;

        $duration->duration_type = $request->duration_type;

        $duration->save();
        
        return redirect()->route('duration')->with('status', 'Duration created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plan_duration  $plan_duration
     * @return \Illuminate\Http\Response
     */
    public function show(Plan_duration $plan_duration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plan_duration  $plan_duration
     * @return \Illuminate\Http\Response
     */
     public function edit(Request $request)
    {

        $plan = Plan_duration::findOrFail($request->id);
        $plan->title = $request->title;
        $plan->duration_type = $request->duration_type;
        $plan->save();

        return redirect()->route('duration')->with('status', 'Plan Duration Updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plan_duration  $plan_duration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plan_duration $plan_duration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plan_duration  $plan_duration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $duration = Plan_duration::find($request->id)->delete();

        return redirect()->route('duration')->with('status', 'Duration deleted');
    }
}
