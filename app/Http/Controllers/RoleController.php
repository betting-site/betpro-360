<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Spatie\Permission\Models\Permission;

use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    
    public function index(){

    	$role =  Role::all();

    	return view('template.admin.role', ['roles'=> $role ]);
    }


    public function model_has_role(Request $request){

    	$users = $request->user_id;

    	foreach ($users as $key => $user_id) {

    		$role = User::findOrFail($user_id)->assignRole($request->role_id);
    	}

        return redirect()->route('role')->with('status', 'User Role Created');
    }


    public function revoke_user(Request $request){
       
        $users = $request->user_id;

        foreach ($users as $key => $user_id) {

            $role = User::findOrFail($user_id)->removeRole($request->role_id);
        }

        return redirect()->route('role')->with('status', 'User Role Revoked');
    }

    public function role_has_permission(Request $request){

        $permissions = $request->permission_id;

        foreach ($permissions as $key => $perm) {

            $permission = Role::findOrFail($request->role_id)->givePermissionTo($perm);
        }

        return redirect()->route('role')->with('status', 'User Permission Created');
    }



    public function create(Request $request){

        $this->validate($request, [

            'name' => ['required', 'string'], 
        ]);


        $contact = Role::create([
            'name' => $request['name'], 
            'guard_name' => 'web'
        ]);
        
        return redirect()->route('role')->with('status', 'Request Recieved ');
    }
}
