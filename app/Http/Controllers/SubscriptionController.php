<?php

namespace App\Http\Controllers;

use App\subscriptions;
use Illuminate\Http\Request;
use App\Plan_price;
use App\Plan;
use App\Subscription;
use Auth\Validator;
use App\User;
use Auth;
use DB;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $transaction = User::transaction();

        $plan = Plan::all();

        return view('template.admin.transaction', ['transactions'=> $transaction, 'plans' => $plan]);

    }

    public function pay(Request $request)
    {
        $this->validate($request, [
            'gateway_name' => ['required'], 
            'payment_type' => ['required'], 
            'plan_duration' => ['required', 'string'],
            'plan_id' => ['required'],
            'amount_paid' => ['required']
        ]);

        $gateway_name = $request->gateway_name;
        $payment_type = $request->payment_type;
        $plan_duration = $request->plan_duration;
        $plan_id = $request->plan_id;
        $currency = $request->currency;
        $amount = $request->amount_paid;
        $transaction_ref = $request->transaction_ref;
        $user_id = Auth::user()->user_id;
        $country = Auth::user()->country_id;
        $email = Auth::user()->email;


        $plan_amount = Plan_price::unique_plan_price($country, $plan_id, $plan_duration);


        switch ($request->gateway_name) {
            case 'wave':

                if($plan_amount->price == $amount){

                    $url = "https://api.flutterwave.com/v3/payments";

                    $fields = (object)[

                        'amount' => $plan_amount->price,
                        'currency' => $currency,
                        'payment_options' => 'card',
                        'tx_ref' => $transaction_ref,
                        'redirect_url' => 'localhost',
                        'customer'=> (object)[
                            'phonenumber' => Auth::user()->phone_number,
                            'email' =>  $email,
                        ],
                        'customizations'=> (object)[
                            'title' => 'BetPro360'
                        ]
                    ];

                    $fields_string = json_encode($fields);

                    //open connection
                    $ch = curl_init();
                      
                    //set the url, number of POST vars, POST data
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, true);
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Authorization: Bearer FLWSECK_TEST-a0e1136868f17189deea849ad2c01da5-X",
                        "Cache-Control: no-cache",
                    ));
                      
                    //So that curl_exec returns the contents of the cURL; rather than echoing it
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
                      

                    //execute post
                    $response = curl_exec($ch);
                    
                    
                    // dd($fields);

                    // dd($fields_string);
                    $err = curl_error($ch);


                    // $tranx = json_decode($response);
                    $enddur = '+'.$plan_duration;
                    $end_date = date('Y-m-d H:i:s', strtotime($enddur));

                    // dd($end_date);
                    $subscription = Subscription::create([
                        'user_id' => $user_id, 
                        'pricing_id' => $plan_amount->id,
                        'amount' => $amount,
                        'payment_type' => $payment_type,
                        'gateway_name' => $gateway_name,
                        'status' => 'fail',
                        'reference' => $transaction_ref,
                        'created_at' => NOW(),
                        'start_date' => NOW(),
                        'end_date' => $end_date
                    ]);
                

                    return response()->json(['amount'=>$amount, 'gateway_name' => $gateway_name, 'email'=>$email, 'last_id'=> $subscription->id, 'transaction_ref'=>$transaction_ref, 'status'=>'success'], 200);


                }else{

                    return response()->json(['message'=> $err, 'status'=>'fail'], 200);

                }
                
                break;
            
            default:
                if($plan_amount->price == $amount){

                    $url = "https://api.paystack.co/transaction/initialize";
                    $fields = [
                        'email' =>  $email,
                        'amount' => $plan_amount->price,
                    ];

                    $fields_string = http_build_query($fields);
                      //open connection
                    $ch = curl_init();
                      
                    //set the url, number of POST vars, POST data
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, true);
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Authorization: Bearer sk_test_91751591463c9b51ddaf506f0e47917af25cd6b7",
                        "Cache-Control: no-cache",
                    ));
                      
                    //So that curl_exec returns the contents of the cURL; rather than echoing it
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
                      
                    //execute post
                    $response = curl_exec($ch);

                    $err = curl_error($ch);


                    $tranx = json_decode($response);

                    if($tranx->status == true){
                        $enddur = '+'.$plan_duration;
                        $end_date = date('Y-m-d H:i:s', strtotime($enddur));

                        // dd($end_date);
                        $subscription = Subscription::create([
                            'user_id' => $user_id, 
                            'pricing_id' => $plan_amount->id,
                            'amount' => $amount,
                            'payment_type' => $payment_type,
                            'gateway_name' => $gateway_name,
                            'status' => 'fail',
                            'reference' => $transaction_ref,
                            'created_at' => NOW(),
                            'start_date' => NOW(),
                            'end_date' => $end_date
                        ]);
                

                        return response()->json(['amount'=>$amount, 'email'=>$email, 'last_id'=> $subscription->id, 'transaction_ref'=>$transaction_ref, 'reference'=> $tranx->data->reference, 'status'=>'success'], 200);
                    }

                }else{
                    return response()->json(['message'=> $err, 'status'=>'fail'], 200);

                }
            break;
        }
    }

    public function create(Request $request){
        $this->validate($request, [
            'plan_id' => ['required'], 
            'user_id' => ['required'], 
            'payment_type' => ['required'], 
            'start_date' => ['required'],
            'end_date' => ['required'],
            'status' => ['required'],
            'amount' => ['required']
        ]);

        $users = $request->user_id;

        $plan_id = $request->plan_id;

        $find_users = User::find($users);

        foreach($find_users as $user){

            $subscribe = new Subscription();
            $subscribe->user_id = $user->user_id; 
            $subscribe->payment_type = $request->payment_type;
            $subscribe->start_date = $request->start_date; 
            $subscribe->end_date = $request->end_date;
            $subscribe->status = $request->status;
            $subscribe->amount = $request->amount;

            $update_user = User::find($users);

            foreach($update_user as $u){

                $u->plan_id = $plan_id;
                $u->save();
            }

            $subscribe->save();
        }

        return redirect()->route('subscription')->with('status', 'Subscription created');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $reference = $request->transaction_response;
        $last_id = $request->last_id;
        $user_id = Auth::user()->id;

        $curl = curl_init();
  
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/".$reference,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "Authorization: Bearer sk_test_91751591463c9b51ddaf506f0e47917af25cd6b7",
              "Cache-Control: no-cache",
            ),
        ));
          
        $response = curl_exec($curl);


        $err = curl_error($curl);
        
        curl_close($curl);

        $tranx = json_decode($response);

          
        if ($tranx->status == true) {


            $user_subscription = Subscription::find($last_id);

            $user_pricing_id = Plan_price::find($user_subscription->pricing_id)->plan_id;

            $user_subscription->status = 'success';


            $user_subscription->save();



            if($user_pricing_id <= 3){


                $user_plan = User::find($user_id);

                $user_plan->plan_id = $user_pricing_id;

                $user_plan->save();

            }

            return response()->json(['message'=>$tranx->message, 'status'=>'success'], 200);
            
        } else {
            
            return response()->json(['message'=> $err, 'status'=>'fail'], 200);
           
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subscriptions  $subscriptions
     * @return \Illuminate\Http\Response
     */
    public function show(subscriptions $subscriptions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subscriptions  $subscriptions
     * @return \Illuminate\Http\Response
     */
    public function edit(subscriptions $subscriptions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subscriptions  $subscriptions
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subscriptions  $subscriptions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $subscription = Subscription::find($request->id)->delete();

        return redirect()->route('subscription')->with('status', 'User Subscription deleted');

    }
}
