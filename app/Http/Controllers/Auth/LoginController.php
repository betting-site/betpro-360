<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth\Validator;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {   
        $input = $request->all();
  
        $this->validate($request, [
            'user_id' => ['required', 'string', 'max:35'],
            'password' => ['required', 'string', 'max:25'],
        ]);
  
        $fieldType = filter_var($request->user_id, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';

        
        if(Auth::attempt([$fieldType => $input['user_id'], 'password' => $input['password'], 'status' => 'Active']))
        {
    
            return redirect('dashboard');
            
        }else{   
            return redirect()->route('login')
                ->with('status', 'Incorrect Email or Password or Account Deactivated.');
        }
          
    }
}
