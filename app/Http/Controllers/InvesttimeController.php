<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth\Validator;

use App\Investtime;

class InvesttimeController extends Controller
{
    public function index(){

    	$investtime =  Investtime::all();

    	return view('template.admin.invest', ['investtime'=> $investtime ]);

    }


    public function create(Request $request){

    	$this->validate($request, [
    		'title' => ['required'],  
        ]);

        $result = Investtime::create([
        	'title' => $request['title'],
        	'desc' => $request['desc'],
        	'button' => $request['button'],
        	'timer' => $request['timer'],
        ]);


        return redirect()->route('invest')->with('status', 'Investment Time created');
    }


    public function edit(Request $request)
    {

        // dd($request->timer);
        $invest = Investtime::findOrFail($request->id);
        $invest->title = $request->title;
        $invest->desc = $request->desc;
        $invest->button = $request->button;
        $invest->timer = $request->timer;
        $invest->save();

        return redirect()->route('invest')->with('status', 'Investment Time Updated');
    }


    public function destroy(Request $request, $id)
    {
        $invest = Investtime::find($request->id)->delete();

        return redirect()->route('invest')->with('status', 'Investment Time deleted');


    }
}
