<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;

use App\Help;

use App\Disclaimer;

use Auth\Validator;

use App\User;

use App\Plan;

use App\Country;

use App\Category;

use App\Result;

use App\Investtime;

use DB;

use Auth;

use Spatie\Permission\Models\Permission;

use Spatie\Permission\Models\Role;

use App\Mail\ContactFormMail;



class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $ch = curl_init();
              
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, 'ip-api.com/json/');
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Cache-Control: no-cache"
        ));
          
        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
          
        //execute post
        $response = curl_exec($ch);

        $err = curl_error($ch);


        $tranx = json_decode($response);

       

        if($tranx){

            $user_country = $tranx->country;


            $slider = DB::table('adverts as a')
                    ->join('countries as c', 'c.id', 'a.country_id')
                    ->where('c.name', $user_country)
                    ->where(function ($query){
                                $query->orWhere('a.location_name', 'slider1')
                                ->orWhere('a.location_name', 'slider2')
                                ->orWhere('a.location_name', 'slider3')
                                ->orWhere('a.location_name', 'slider4')
                                ->orWhere('a.location_name', 'slider6')
                                ->orWhere('a.location_name', 'slider7')
                                ->orWhere('a.location_name', 'slider8')
                                ->orWhere('a.location_name', 'slider9')
                                ->orWhere('a.location_name', 'slider10');
                        })->get();


            $adverts = DB::table('adverts as a')
                    ->select('a.avatar')
                    ->join('countries as c', 'c.id', 'a.country_id')
                    ->where('c.name', $user_country)
                    ->where(function ($query){
                                $query->orWhere('a.location_name', 'advert1')
                                ->orWhere('a.location_name', 'advert2')
                                ->orWhere('a.location_name', 'advert3')
                                ->orWhere('a.location_name', 'advert4')
                                ->orWhere('a.location_name', 'advert5')
                                ->orWhere('a.location_name', 'advert6')
                                ->orWhere('a.location_name', 'advert7');
                        })->get();

            foreach ($adverts as $key => $advert) {
                $arrays[] = (array) $advert;
            }
            
        }else{

            $slider = [];

            $arrays = [];
        }
        

        $advert1 = isset($arrays[0]['avatar']) ? $arrays[0]['avatar'] : 'image/preset/advert1.png';
        $advert2 = isset($arrays[1]['avatar']) ? $arrays[1]['avatar'] : 'image/preset/advert2.png';
        $advert3 = isset($arrays[2]['avatar']) ? $arrays[2]['avatar'] : 'image/preset/advert2.png';
        $advert4 = isset($arrays[3]['avatar']) ? $arrays[3]['avatar'] : 'image/preset/advert2.png';
        $advert5 = isset($arrays[4]['avatar']) ? $arrays[4]['avatar'] : 'image/preset/advert2.png';
        $advert6 = isset($arrays[5]['avatar']) ? $arrays[5]['avatar'] : 'image/preset/advert2.png';
        $advert7 = isset($arrays[6]['avatar']) ? $arrays[6]['avatar'] : 'image/preset/advert2.png';


        $category = Category::take(6)->get();

        $advisor = DB::table('results as r')
                    ->select(DB::raw('r.id , r.status, r.result_type, u.firstname'))
                    ->join('advisors as a', 'r.expert_id', 'a.id')
                    ->join('users as u', 'u.id', 'a.user_id')
                    ->where('r.result_type', 'advisor')
                    ->groupBy('u.firstname', 'r.id', 'r.status', 'r.result_type')->get();


        $invest = DB::table('results')->where('result_type', 'invest')->orderBy('created_at', 'DESC')->take(4)->get();

        $rollover = DB::table('results')->where('result_type', 'rollover')->orderBy('created_at', 'DESC')->take(4)->get();


        $advisor_remark = Result::where('result_type', 'advisor')->latest()->first();

        $invest_remark = Result::where('result_type', 'invest')->latest()->first();

        $rollover_remark = Result::where('result_type', 'rollover')->latest()->first();


        $timer = Investtime::first();


        return view('template.client.welcome', 
                ['categories'=> $category, 'invests'=> $invest, 'advisors' => $advisor, 'rollovers' => $rollover, 
                'advisor_remark' =>$advisor_remark, 'invest_remark' => $invest_remark,
                'rollover_remark' => $rollover_remark, 'timer' => $timer, 'sliders'=> $slider,
                'advert1' => $advert1, 'advert2' => $advert2, 'advert3' => $advert3, 'advert4' => $advert4,
                'advert5' => $advert5, 'advert6' => $advert6, 'advert7' => $advert7
                ]);

    }

    public function prediction(Request $request){

        $date = $request->day;

        $type = $request->type;

        switch($type){

            case 'free_acca':

            switch ($date) {

                case 'yesterday':

                    $free_acca = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.free_acca', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d",strtotime("yesterday")) . '%');
                            })->get();

                    break;

                case 'tomorrow':

                    $free_acca = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.free_acca', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d",strtotime("tomorrow")). '%');
                            })->get();

                    break;
                
                default:

                    $free_acca = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.free_acca', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d") . '%');
                            })->get();

                    break;
            }

            return response()->json($free_acca, 200);

            break;

            case 'upcoming_pred':

            switch ($date) {

                case 'yesterday':

                    $upcoming_pred = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.upcoming_pred', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d",strtotime("yesterday")) . '%');
                            })->get();

                    break;

                case 'tomorrow':

                    $upcoming_pred = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.upcoming_pred', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d",strtotime("tomorrow")). '%');
                            })->get();

                    break;
                
                default:

                    $upcoming_pred = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.upcoming_pred', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d") . '%');
                            })->get();

                    break;
            }

            return response()->json($upcoming_pred, 200);

            break;

            case 'popular':
             switch ($date) {

                case 'yesterday':

                // dd($request);

                    $popular = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.winning_tips', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d",strtotime("yesterday")) . '%');
                            })->get();

                    break;

                case 'tomorrow':

                    $popular = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.winning_tips', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d",strtotime("tomorrow")). '%');
                            })->get();

                    break;
                
                default:

                    $popular = DB::table('predictions as p')
                                ->select(DB::raw('p.id , p.teams, p.home_odd, p.away_odd, p.draw_odd'))
                                ->where('p.winning_tips', 'yes')
                                ->where('p.deleted_at', NULL)
                                ->where(function ($query){
                                    $query->where('p.match_date', 'like', '%'. date("Y-m-d") . '%');
                            })->get();

                    break;
            }

            return response()->json($popular, 200);

            break;

        }

    }


    public function contact_index()
    {
        return view('template.client.contact');
    }


    public function contact_create(Request $request)
    {
        $this->validate($request, [
            'fullname' => ['required', 'string', 'max:255' ], 
            'email' => ['required', 'string', 'email', 'max:255' ],
            'phone_number' => ['string', 'max:255'],
            'subject' => ['required', 'max:255'],
            'message' => ['required', 'string', 'max:255']
        ]);

        $contact = [
            'fullname' => $request['fullname'], 
            'email' => $request['email'],
            'phone_number' => $request['phone_number'],
            'subject' => $request['subject'],
            'message' => $request['message'],
            'screenshot' => $request->file('screenshot')->store('contact', 'public')
        ];

    
        Mail::to('suleabimbola@gmail.com')->send(new ContactFormMail($contact));
        
        return redirect()->route('contact')->with('status', 'Your Mail has been received');
    }



    public function updateProfile(Request $request, User $id)
    {
        $this->validate($request, [
            'firstname' => ['string', 'max:255' ], 
            'lastname' => ['string', 'max:255' ],
            'phone_number' => ['required', 'string', 'max:255'],
            // 'avatar' => ['required', 'image'],
            'country' => ['required', 'string']
        ]);
        
        
        $user = User::find(Auth::user()->id);
        
        if (!empty($user)) {
            
            $user->firstname = $request['firstname'];
            $user->lastname = $request['lastname'];
            $user->country_id = $request['country'];
            $user->phone_number = $request['phone_number'];
            // $user->username = $request['username'];
            $user->referred_by = $request['referred_by'];
            $user->profile_complete = 'yes';
            $user->avatar = $request->file('avatar')->store('avi', 'public');
            
            $user->save();
            return redirect()->route('dashboard')->with('status', 'Profile updated!');
        }
    }


    public function help(){

        $qa = Help::all();
        return view('template.client.help', ['questions' => $qa]);
    }

     public function offers_index()
    {
        return view('template.client.offers');
    }

     public function cookies_index()
    {
        return view('template.client.cookies');
    }

     public function refund_index()
    {
        return view('template.client.refund');
    }

    public function privacy_index()
    {
        return view('template.client.privacy');
    }

    public function terms_index()
    {
        return view('template.client.terms');
    }

}
