<?php

namespace App\Http\Controllers;

use App\Advisor;
use App\User;
use Illuminate\Http\Request;
use DB;

class AdvisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advisor =  DB::table('advisors as ad')->select(DB::raw('ad.id as id, firstname, lastname, u.user_id'))->join('users as u', 'u.id', 'ad.user_id')->get();

        $total_advisor = DB::table('advisors')->count();


        return view('template.admin.advisor', ['advisors'=> $advisor, 'total' => $total_advisor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        
        $advisor = Advisor::create([

            'user_id' => $request['user_id'], 
        ]);


        return redirect()->route('advisor')->with('status', 'Expert Advisor created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advisor  $advisor
     * @return \Illuminate\Http\Response
     */
    public function show(Advisor $advisor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advisor  $advisor
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $advisor = Advisor::findOrFail($request->id);
        $advisor->user_id = $request->user_id;
        $advisor->save();

        return redirect()->route('advisor')->with('status', 'Advisor Updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advisor  $advisor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advisor $advisor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advisor  $advisor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $advisor = Advisor::find($request->id)->delete();

        return redirect()->route('advisor')->with('status', 'Advisor deleted');
    }
}
