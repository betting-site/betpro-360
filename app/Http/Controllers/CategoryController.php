<?php

namespace App\Http\Controllers;

use App\Category;
use App\Disclaimer;
use Illuminate\Http\Request;
use Auth\Validator;
use App\Permission;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('template.client.category', ['categories' => $category]);
    }


    public function bet()
    {
        $category = Category::all();
        return view('template.client.bet', ['categories' => $category]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $category =  Category::all();
        return view('template.admin.category', ['categories' => $category]);
    }

    public function create(Request $request){
        $this->validate($request, [
            'title' => ['required', 'string', 'max:255' ], 
            'plan_id' => ['required'], 
            'description' => ['required'], 
        ]);

        $category = new Category();
        $category->title = $request->title;
        $category->plan_id = implode(',', $request->plan_id);
        $category->description = $request->description;
        $category->status = $request->status;
        $category->special = $request->special;
        $category->save();

        return redirect()->route('admincategory')->with('status', 'Category created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $search = trim($request->q);
        $type = $request->type;

        switch ($type) {

            case 'cat_id':
                $tags = DB::table('categories as c')
                ->select(DB::raw('c.id , c.title as name'))
                ->where(function($query) use ($search) {
                    $query->where('c.title','LIKE','%'.$search.'%')
                        ->orWhere('c.plan_id','LIKE','%'.$search.'%')
                        ->orWhere('c.description','LIKE','%'.$search.'%');
                })->get();

                break;

            case 'plan_id':
                $tags = DB::table('plans as p')
                ->select(DB::raw('p.id , p.name as name'))
                ->where(function($query) use ($search) {
                    $query->where('p.name','LIKE','%'.$search.'%');
                })->get();

                break;

            case 'permission':

                $tags = Permission::all();

                break;

            default:
                // All users
                $tags = [];

                break;
        }
        

        $formatted_tags = [];

        foreach ($tags->unique('name') as $tag) {
            
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name];

        }

        return \Response::json($formatted_tags);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request){

        // dd($request);

        $category = Category::findOrFail($request->id);
        $category->title = $request->title;
        $category->plan_id = implode(',', $request->plan_id);
        $category->description = $request->description;
        $category->status = $request->status;
        $category->special = $request->special;
        $category->save();

        return redirect()->route('admincategory')->with('status', 'Category Updated');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // dd($id);

        $category = Category::find($request->id)->delete();

        return redirect()->route('admincategory')->with('status', 'Category deleted');


    }
}
