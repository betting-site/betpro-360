<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan_price;
use App\Plan;
use App\Country;
use App\Plan_duration;
use DB;
use Auth\Validator;

class PlanPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $planzz = Plan::all();
        $country = Country::all();
        $duration = Plan_duration::all();

        return view('template.client.pricing', ['planzz' => $planzz,  'country' => $country, 'plan_duration' => $duration]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'amount' => ['required', 'string', 'max:255' ], 
            'plan_name' => ['required'], 
            'duration' => ['required'], 
            'country' => ['required'], 
        ]);

        $price = Plan_price::create([
            'plan_id' => $request['plan_name'], 
            'price' => $request['amount'], 
            'country_id' => $request['country'], 
            'duration_id' => $request['duration']
        ]);
        
        return redirect()->route('adminpricing')->with('status', 'Plan Pricing created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\plan_price  $plan_price
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        $planzz = Plan::all();
        $country = Country::all();
        $duration = Plan_duration::all();

        $price = DB::table('plan_prices as pp')
                ->select(DB::raw('pp.id, pp.price as amount, c.id as cid, c.name as country, pd.id as pdid, pd.title as duration , p.name as name, p.id as pid'))->where('pp.deleted_at', NULL)
                ->join('countries as c', 'c.id', 'pp.country_id')
                ->join('plan_durations as pd', 'pd.id', 'pp.duration_id')
                ->join('plans as p', 'p.id', 'pp.plan_id')
                ->get();

        return view('template.admin.price', ['plans' => $planzz,'prices' => $price, 'countries' => $country, 'durations' => $duration]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\plan_price  $plan_price
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $price = Plan_price::findOrFail($request->id);
        $price->price = $request->amount;
        $price->country_id = $request->country;
        $price->duration_id = $request->duration;
        $price->plan_id = $request->plan_name;
        $price->save();

        return redirect()->route('adminpricing')->with('status', 'Plan Price Updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\plan_price  $plan_price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, plan_price $plan_price)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\plan_price  $plan_price
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request, $id)
    {
        $price = Plan_price::findOrFail($request->id)->delete();

        return redirect()->route('adminpricing')->with('status', 'Plan Price deleted');


    }
}
