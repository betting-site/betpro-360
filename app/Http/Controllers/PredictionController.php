<?php

namespace App\Http\Controllers;

use App\Prediction;
use App\Plan;
use App\Category;
use App\League;
use App\Cat_prediction;
use Illuminate\Http\Request;
use Auth\Validator;
use App\User;
use Illuminate\Support\Str;
use DB;

use Auth;


class PredictionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prediction = DB::table('predictions as p')
                    ->select(DB::raw('p.id, l.title as league, l.nickname, teams, home_odd, draw_odd, match_date'))->where('p.deleted_at', NULL)
                    ->join('leagues as l', 'l.id', 'p.league_id')
                    ->get();

        $user_plan = Auth::user()->plan_id;
        
        $plan_name = Plan::find($user_plan);



        switch($user_plan){
            default:
                $plan_feature = Category::where('plan_id', '1')->get();
            break;
            case "2":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2');
                        })->get();
            break;
            case "3":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2')
                                    ->orWhere('plan_id', '1,2,3');
                        })->get();
                
            break;        
        }

        $league = League::all();

        return view('template.admin.prediction', ['predictions'=> $prediction, 'leagues'=> $league]);
    }


    public function client_index(Request $request, $day=null)
    {

        $league = League::all();

        $user_plan = Auth::user()->plan_id;

        $cat_id = $request->id;

        $date = $request->day;


        if(isset($cat_id)){

            $prediction = DB::table('categories as c')
                            ->select(DB::raw('id, title, plan_id'))
                            ->where('id', $cat_id)
                            ->first();


            // dd($prediction->plan_id);

            // dd(Str::contains('1,2',  explode(',', $prediction->plan_id)) && 
            //     (Str::contains(3, substr($prediction->plan_id, -1))));

            // (in_array($user_plan, explode(',', $prediction->plan_id)))


            if($prediction){

                switch($user_plan){
                    default:
                        $bet_category = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result, betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('plan_id', '1')
                                    ->where('cat.cat_id', $cat_id);

                       
                    break;
                    case "2":

                        $bet_category = DB::table('predictions as p')
                                        ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result, betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                        ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                        ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                        ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                        ->where('p.deleted_at', NULL)
                                        ->where('cat.cat_id', $cat_id)
                                        ->where(function ($query){
                                            $query->where('plan_id', '1')
                                            ->orWhere('plan_id', '1,2');
                                    });

                    break;
                    case "3":
                        $bet_category = DB::table('predictions as p')
                                        ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result, betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                        ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                        ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                        ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                        ->where('p.deleted_at', NULL)
                                        ->where('cat.cat_id', $cat_id)
                                        ->where(function ($query){
                                            $query->where('plan_id', '1')
                                            ->orWhere('plan_id', '1,2')
                                            ->orWhere('plan_id', '1,2,3');
                                    });  
                    break;
                    case "4":
                        $bet_category = DB::table('predictions as p')
                                        ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result, betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                        ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                        ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                        ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                        ->where('p.deleted_at', NULL)
                                        ->where('cat.cat_id', $cat_id)
                                        ->where(function ($query){
                                            $query->where('plan_id', '4');
                                    });
                    break;
            
                }

            }else{

                return redirect()->route('dashboard')->with('status', 'Upgrade Plan to View');

            }
        }else{

            return redirect()->route('prediction')->with('status', 'Select a Category');
        }

        if(isset($date)){

            switch($date){

                case "2dg":

                        $bet_date = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result,  betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('cat.cat_id', $cat_id)
                                ->union($bet_category->where('p.match_date', 'like', '%'. date("Y-m-d", strtotime("-1 day", strtotime("yesterday"))). '%'))->where('p.match_date', 'like', '%'. date("Y-m-d", strtotime("-1 day", strtotime("yesterday"))) . '%')
                                ->get();
   
                break;


                case "1dg":

                        $bet_date = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result,  betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('cat.cat_id', $cat_id)
                                    ->union($bet_category->where('p.match_date', 'like', '%'. date("Y-m-d", strtotime("yesterday")). '%'))
                                    ->where('p.match_date', 'like', '%'. date("Y-m-d", strtotime("yesterday")) . '%')
                                    ->get();

                break;

                case "1tm":

                        $bet_date = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result,  betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('cat.cat_id', $cat_id)
                                    ->union($bet_category->where('p.match_date', 'like', '%'.date("Y-m-d", strtotime("tomorrow")). '%'))
                                    ->where('p.match_date', 'like', '%'.date("Y-m-d", strtotime("tomorrow")). '%')
                                    ->get();

                break;


                case "2tm":

                        $bet_date = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result,  betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('cat.cat_id', $cat_id)
                                    ->union($bet_category->where('p.match_date', 'like', '%'.date("Y-m-d", strtotime("tomorrow")). '%'))
                                    ->where('p.match_date', 'like', '%'.date("Y-m-d", strtotime("+1 day", strtotime("tomorrow"))). '%')
                                    ->get();

                break;


                default:
                        $bet_date = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation,  c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result,  betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('cat.cat_id', $cat_id)
                                    ->union($bet_category->where('p.match_date', 'like', '%'. date("Y-m-d"). '%'))
                                    ->where('p.match_date', 'like', '%'. date("Y-m-d"). '%')
                                    ->get();

                break;


            }

        }else{

            $bet_date = DB::table('predictions as p')
                                    ->select(DB::raw('league_id as lid, p.id as id, l.nickname, cat_id as cid, cat.recommendation, c.title, teams,  home_odd, away_odd, draw_odd, upcoming_pred, winning_tips, free_acca, home_form, away_form, ft_recommendation, result,  betbuilder_odd, betrollover_odd, sure2_odd,  sure2_2odd, sure3_odd, sure3_2odd, investment_2odd, match_date'))
                                    ->join('cat_predictions as cat', 'cat.pid', '=', 'p.id')
                                    ->join('leagues as l', 'p.league_id', '=', 'l.id')
                                    ->join('categories as c', 'c.id', '=', 'cat.cat_id')
                                    ->where('p.deleted_at', NULL)
                                    ->where('cat.cat_id', $cat_id)
                                    ->union($bet_category->where('p.match_date', 'like', '%'. date("Y-m-d"). '%'))
                                    ->where('p.match_date', 'like', '%'. date("Y-m-d"). '%')
                                    ->get();

        }

        return view('template.client.prediction', ['predictions'=> $bet_category,  
                                            'cat_title'=> $prediction, 'leagues' => $league, 'bets' => $bet_date]);


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id = null)
    {

        $this->validate($request, [

            'league_id' => ['required', 'string'], 

            'teams' => ['required', 'string'],

            'home_odd' => ['required', 'string'],

            'draw_odd' => ['required', 'string'], 

            'away_odd' => ['required', 'string'],

            'match_date' => ['required', 'string']
        ]);

        

        if(isset($request->id)){


            $prediction = Prediction::findOrFail($request->id);

            $prediction->league_id = $request->league_id; 
            $prediction->teams = $request->teams;  
            $prediction->home_odd = $request->home_odd;
            $prediction->draw_odd = $request->draw_odd;  
            $prediction->away_odd = $request->away_odd;
            $prediction->winning_tips = $request->winning_tips;
            $prediction->result = $request->result; 
            $prediction->match_date = $request->match_date.' '. date('H:i', strtotime($request->time));
            $prediction->upcoming_pred = $request->upcoming; 
            $prediction->free_acca = $request->free_acca;
            $prediction->home_form= $request->home_form;
            $prediction->away_form = $request->away_form;
            $prediction->preview= $request->preview;
            $prediction->ft_recommendation = $request->ft_recommendation;
            $prediction->opor= $request->opor;
            $prediction->betbuilder_odd = $request->betbuilder_odd;
            $prediction->betrollover_odd = $request->betrollover_odd;
            $prediction->sure2_odd= $request->sure2_odd;
            $prediction->sure2_2odd = $request->sure2_2odd;
            $prediction->sure3_odd = $request->sure3_odd;
            $prediction->sure3_2odd = $request->sure3_2odd;
            $prediction->investment_odd = $request->investment_odd; 
            $prediction->investment_2odd = $request->investment_2odd;
            
        

            $prediction->save();

            $last_insert = $prediction->id;

            if($last_insert){

                foreach ($request->cat_id as $key1 => $cat) {


                    foreach ($request->recommendation as $key2 => $recommendation) {

                        if($key1 == $key2){

                            $category_pred = Cat_prediction::findOrFail($cat);

                            $category_pred->pid = $prediction->id;
                            $category_pred->cat_id = $cat;
                            $category_pred->recommendation = $recommendation;
                            $category_pred->save();
                        }
                    }
                }
            }
   
            return redirect()->route('adminprediction')->with('status', 'Prediction Updated');

        }else{

            $prediction = new Prediction();
            $prediction->league_id = $request->league_id; 
            $prediction->teams = $request->teams;  
            $prediction->home_odd = $request->home_odd;
            $prediction->draw_odd = $request->draw_odd;  
            $prediction->away_odd = $request->away_odd;
            $prediction->winning_tips = $request->winning_tips;
            $prediction->result = $request->result; 
            $prediction->match_date = $request->match_date.' '. date('H:i', strtotime($request->time));
            $prediction->upcoming_pred = $request->upcoming; 
            $prediction->free_acca = $request->free_acca;
            $prediction->home_form= $request->home_form;
            $prediction->away_form = $request->away_form;
            $prediction->preview= $request->preview;
            $prediction->ft_recommendation = $request->ft_recommendation;
            $prediction->opor= $request->opor;
            $prediction->betbuilder_odd = $request->betbuilder_odd;
            $prediction->betrollover_odd = $request->betrollover_odd;
            $prediction->sure2_odd= $request->sure2_odd;
            $prediction->sure2_2odd = $request->sure2_2odd;
            $prediction->sure3_odd = $request->sure3_odd;
            $prediction->sure3_2odd = $request->sure3_2odd;
            $prediction->investment_odd = $request->investment_odd; 
            $prediction->investment_2odd = $request->investment_2odd;
            
        

            $prediction->save();

            $last_insert = $prediction->id;

            if($last_insert){

                foreach ($request->cat_id as $key1 => $cat) {


                    foreach ($request->recommendation as $key2 => $recommendation) {

                        if($key1 == $key2){

                            $category_pred = new Cat_prediction();
                            $category_pred->pid = $prediction->id;
                            $category_pred->cat_id = $cat;
                            $category_pred->recommendation = $recommendation;
                            $category_pred->save();
                        }
                    }
                }
            }
            
            return redirect()->route('adminprediction')->with('status', 'Prediction created');
        }
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function specials(Request $request)
    {

        $prediction = DB::table('categories as c')
                    ->select(DB::raw('id, title, special'))
                    ->where('c.id', $request->id)
                    ->first();

        
          return response()->json(['result' => $prediction], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\predictions  $predictions
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id= null)
    {

        if(isset($request->id)) {

            $prediction = Prediction::findOrFail($request->id)->first();

            $edit_pred = DB::table('cat_predictions as cat')
                        ->join('categories as c', 'c.id', 'cat.cat_id')
                        ->where('pid', $request->id)
                        ->get();
            
            $league = League::all();

            return view('template.admin.addprediction', ['prediction' => $prediction, 'edit_preds' => $edit_pred, 'leagues' => $league]);

        }else{

            $league = League::all();

            $category = Category::all();

            return view('template.admin.addprediction', ['leagues' => $league, 'categories' => $category]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\predictions  $predictions
     * @return \Illuminate\Http\Response
     */
    public function match(Request $request, $id, $cat_id=null)
    {
        $user_plan = Auth::user()->plan_id;


        if(isset($request->id)){

            $prediction = Prediction::where('id', $request->id)->firstOrFail();
            
            if($prediction){

                $check_plan = DB::table('cat_predictions as cat')
                                ->join('categories as c', 'c.id', 'cat.cat_id')
                                ->where('cat.pid', $prediction->id)->exists();

                if($check_plan){

                    $active = DB::table('cat_predictions as cat')
                                ->join('categories as c', 'c.id', 'cat.cat_id')
                                ->where('cat.pid', $prediction->id)->first();
                    
                    $actual_column = DB::table('cat_predictions as cat')
                                ->join('categories as c', 'c.id', 'cat.cat_id')
                                ->where('cat.pid', $prediction->id)
                                ->where('cat.cat_id', $request->cat_id)->first();


                    $current_plan = substr($active->plan_id, -1);

                    // $check_current_plan = Str::contains($current_plan, $user_plan);

                    $check_current_plan = $user_plan;
                
                }else{

                    return redirect()->route('prediction')->with('status', 'No match details');
                }


            }else{

                return redirect()->route('dashboard')->with('status', 'Upgrade Plan to View');
            }


        }else{

            return redirect()->route('dashboard')->with('status', 'Access denied');

        }



        return view('template.client.match', ['predictions' => $prediction, 'active'=>$actual_column, 'plan' => $check_current_plan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\predictions  $predictions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $prediction = Prediction::findOrFail($request->id)->delete();

        return redirect()->route('adminprediction')->with('status', 'Prediction deleted');
    }
}
