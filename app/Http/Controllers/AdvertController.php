<?php

namespace App\Http\Controllers;

use App\Advert;

use App\Country;

use Auth\Validator;

use DB;

use Illuminate\Http\Request;

class AdvertController extends Controller
{
     public function index(){

    	$advert =  DB::table('adverts as ad')->select(DB::raw('ad.id as id, location_name, avatar, dest_url, name, currency, symbol'))->join('countries as c', 'ad.country_id', 'c.id')->get();

    	$country = Country::all();


    	return view('template.admin.advert', ['adverts'=> $advert, 'countries' => $country ]);

    }


    public function create(Request $request){


    	$this->validate($request, [
    		'location_name' => ['required'],  
        ]);

        // test advert to know whats up with admin

        $check_advert = Advert::select('location_name', 'country_id')
                        ->where('location_name', $request->location_name)
                        ->where('country_id', $request->country)->exists();

        if($check_advert ){

                $advert = Advert::where('location_name', $request->location_name)->where('country_id', $request->country)->first();

            $update_ads = Advert::findOrFail($advert['id']);

            $advert->location_name = $request->location_name;
            $advert->avatar = $request->file('avatar')->store('image', 'public');
            $advert->country_id = $request->country;
            $advert->dest_url = $request->dest_url;
            $advert->save();

            return redirect()->route('advert')->with('status', 'Advert Updated');
        }else{

            $advert = new Advert();
            $advert->location_name = $request->location_name;
            $advert->avatar = $request->file('avatar')->store('image', 'public');
            $advert->country_id = $request->country;
            $advert->dest_url = $request->dest_url;
            $advert->save();


            return redirect()->route('advert')->with('status', 'Advert created');
        }

    }


    public function edit(Request $request)
    {

        $advert = Advert::findOrFail($request->id);
        $advert->location_name = $request->location_name;
        $advert->avatar = $request->file('avatar')->store('image', 'public');
        $advert->country_id = $request->country;
        $advert->dest_url = $request->dest_url;
        $advert->save();

        return redirect()->route('advert')->with('status', 'Advert Updated');
    }


    public function destroy(Request $request, $id)
    {
        $advert = Advert::findOrFail($request->id)->delete();

        return redirect()->route('advert')->with('status', 'Advert deleted');
    }
}
