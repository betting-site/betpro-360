<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Auth\Validator;

class countryController extends Controller
{
    
    public function index()
    {   

        $country = Country::all();

        return view('template.admin.country', ['countries'=> $country]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255' ], 
            'currency' => ['required'], 
            'symbol' => ['required'], 
        ]);

        $country = Country::create([
            'name' => $request['name'], 
            'currency' => $request['currency'], 
            'symbol' => $request['symbol'], 
        ]);
        
        return redirect()->route('country')->with('status', 'Country created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $country = Country::findOrFail($request->id);
        $country->name = $request->name;
        $country->currency = $request->currency;
        $country->symbol = $request->symbol;
        $country->save();

        return redirect()->route('country')->with('status', 'Country Updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $country = Country::find($request->id)->delete();

        return redirect()->route('country')->with('status', 'Country deleted');
    }
}
