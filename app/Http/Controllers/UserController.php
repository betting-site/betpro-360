<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Plan;
use App\Plan_duration;
use App\Plan_price;
use App\Category;
use App\Country;
use App\League;
use App\Prediction;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {   

        $country_id = Auth::user()->country_id;

        $user_plan = Auth::user()->plan_id;

        if($user_plan == 3){

            $plan = DB::table('plans as p')
                    ->select(DB::raw('p.id, p.name'))
                    // ->whereNotIn('p.id', [$user_plan])
                    ->where('p.deleted_at', NULL)
                    ->get();
        }else{

            $plan = DB::table('plans as p')
                    ->select(DB::raw('p.id, p.name'))
                    // ->whereNotIn('p.id', [$user_plan])
                    ->whereNotIn('p.id', ['5'])
                    ->where('p.deleted_at', NULL)
                    ->get();
        }
        
        $country = Country::with('country')->where('id', $country_id)->first();

        
        $plan_name = Plan::find($user_plan);

        switch($user_plan){
            default:
                $plan_feature = Category::where('plan_id', '1')->get();
            break;
            case "2":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2');
                        })->get();
            break;
            case "3":
                $plan_feature = Category::where(function ($query){
                                    $query->where('plan_id', '1')
                                    ->orWhere('plan_id', '1,2')
                                    ->orWhere('plan_id', '1,2,3');
                        })->get();
                
            break;        
        }



        $check_subscription = DB::table('subscriptions as sub')
                            ->select(DB::raw('cat.id as id, cat.title, p.name, p.id as pid'))
                            ->join('plan_prices as pp', 'pp.id', 'sub.pricing_id')
                            ->join('plans as p', 'p.id', 'pp.plan_id')
                            ->leftJoin('categories as cat', 'cat.plan_id', 'pp.plan_id')
                            ->where(function ($query){
                                $query->where('sub.user_id', Auth::user()->user_id)
                                ->where('sub.status', 'success')
                                ->where('sub.end_date', '>', "'". date("Y-m-d")."'")
                                ->whereNotIn('p.id', [1,2,3])
                                ->orderBy('sub.created_at', 'desc');
                            })->get();
                            

        return view('template.client.user_profile', ['plans' => $plan, 'features'=>$plan_feature,'plan_name' => $plan_name, 'country'=>$country, 'subscriptions' => $check_subscription]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function payment(Request $request, $id)
    {

        if($request->id == 5){

            if(Auth::user()->plan_id == 3){

                $active_plan = Plan::find($id);
            
                $duration = Plan_duration::all();

                $country = Country::with('country')->where('id', Auth::user()->country_id)->first();

            }else{

                return redirect()->route('dashboard')->with('status', 'Upgrade Plan to Gold');

            }

        }else{

            $active_plan = Plan::find($id);
            
            $duration = Plan_duration::all();

            $country = Country::with('country')->where('id', Auth::user()->country_id)->first();

        }




        return view('template.client.payment', ['plans' => $active_plan, 'country'=>$country, 'duration'=> $duration]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $country  = Country::all();

        $countryName = Country::where('id', Auth::user()->country_id)->first();

        $maxDate = date('Y') - 19. .'-'.'12'.'-'.'30';


        return view('template.client.update_profile', ['countries'=> $country, 'countryName' => $countryName, 'maxDate' =>$maxDate]);
    }


    public function updateProfile(Request $request, User $id)
    {
        $this->validate($request, [
            'firstname' => ['string', 'max:255' ], 
            'lastname' => ['string', 'max:255' ],
            'phone_number' => ['required', 'string', 'max:255'],
            'avatar' => ['image'],
            'country' => ['required', 'string']
        ]);
        
        
        $user = User::findOrFail(Auth::user()->id);
        
        if ($user) {
            
            $user->firstname = $request['firstname'];
            $user->lastname = $request['lastname'];
            $user->country_id = $request['country'];
            $user->phone_number = $request['phone_number'];
            $user->email = $request['email'];
            $user->dob = $request['dob'];
            $user->password = $request['password'] ? Hash::make($request['password']) : Auth::user()->password;
            $user->profile_complete = 'yes';
            $user->avatar = $request->file('avatar') ? $request->file('avatar')->store('avi', 'public') : '';
            $user->save();
            
            return redirect()->route('dashboard')->with('status', 'Profile updated!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function tranzactn(Request $request, User $User)
    {

        $user_id = Auth::user()->user_id;

        $transaction = User::transaction($user_id);

        return view('template.client.transaction', ['transactions' => $transaction]);
    }

    public function searchprice(Request $request)
    {
        $country = Auth::user()->country_id;

        $plan_id = $request->plan_id;

        $plan_duration = $request->plan_duration;

        $plan_amount = Plan_price::unique_plan_price($country, $plan_id, $plan_duration);
        

        if($plan_amount){

            return response()->json(['message'=> 'Proceed to Pay', 'price' => $plan_amount->price, 'status'=>'true'], 200);
        }
        return response()->json(['message'=> 'Amount Not Found', 'price' => 0], 200);
        
    }

    public function searchInfo(Request $request)
    {

        $search = trim($request->q);
        $type = $request->type;

        switch ($type) {
            case 'user_id':
                $tags = DB::table('users as u')
                ->select(DB::raw('u.id , u.firstname as firstname, u.lastname'))
                ->where(function($query) use ($search) {
                    $query->where('u.firstname','LIKE','%'.$search.'%')
                        ->orWhere('u.lastname','LIKE','%'.$search.'%')
                        ->orWhere('u.phone_number','LIKE','%'.$search.'%')
                        ->orWhere('u.user_id','LIKE','%'.$search.'%')
                        ->orWhere('u.email','LIKE','%'.$search.'%');
                })
                ->paginate(10);

                break;

            default:
                // All users
                $tags = DB::table('users as u')
                ->select(DB::raw('u.id as id, u.firstname, u.lastname'))
                ->where(function($query) use ($search) {
                    $query->where('u.firstname','LIKE','%'.$search.'%')
                        ->orWhere('u.lastname','LIKE','%'.$search.'%')
                        ->orWhere('u.phone_number','LIKE','%'.$search.'%')
                        ->orWhere('u.user_id','LIKE','%'.$search.'%')
                        ->orWhere('u.email','LIKE','%'.$search.'%');
                })
                ->paginate(10);
                break;
        }
        

        $formatted_tags = [];

        foreach ($tags->unique('firstname') as $tag) {
            
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->firstname. ' '.$tag->lastname];

        }

        return \Response::json($formatted_tags);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $User)
    {

        $user = User::all_active();

        $plan = Plan::all();

        return view('template.admin.user', ['users'=> $user, 'plans' => $plan]);
    }

     public function edit(Request $request)
    {

        $user = User::findOrFail($request->id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->status = $request->status;
        $user->plan_id = $request->plan_id;
        $user->save();

        return redirect()->route('user')->with('status', 'User Profile Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = USer::find($request->id);
        $user->status = 'inactive';
        $user->save();

        return redirect()->route('user')->with('status', 'User Deactivated');
    }
}
