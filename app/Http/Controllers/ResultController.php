<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Result;

use DB;

class ResultController extends Controller
{
 	public function index()
    {   

        $result = Result::all();

        $expert_advisor = DB::table('advisors as ad')->select(DB::raw('ad.id as id, ad.user_id, firstname, lastname'))->join('users as u', 'u.id', 'ad.user_id')->get();

        return view('template.admin.result', ['results'=> $result, 'advisors' => $expert_advisor]);
    }


    public function create(Request $request){

    	$this->validate($request, [
    		'result_date' => ['required' ],  
        ]);

        $result = Result::create([
        	'result_date' => $request['result_date'],
        	'status' => $request['status'],
        	'result_type' => $request['result_type'],
            'expert_id' => $request['expert_id'],
        	'remark' => $request['remark'],
        ]);


        return redirect()->route('result')->with('status', 'Result created');
    }

    public function edit(Request $request)
    {

        $result = Result::findOrFail($request->id);
        $result->result_date = $request->result_date;
        $result->status = $request->status;
        $result->result_type = $request->result_type;
        $result->expert_id = $request->expert_id;
        $result->remark = $request->remark;
        $result->save();

        return redirect()->route('result')->with('status', 'Result Updated');
    }


    public function destroy(Request $request, $id)
    {
        $result = Result::find($request->id)->delete();

        return redirect()->route('result')->with('status', 'Result deleted');


    }
}
