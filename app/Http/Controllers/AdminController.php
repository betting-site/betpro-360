<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // count all subscriptions
        $user['subscription'] = DB::table('subscriptions')->where('status', 'success')->count();


        //count all active users
        $user['all_user'] = DB::table('users')->where('status', 'active')->count();


        //cash made
        $user['amount'] = DB::table('subscriptions')->where('status', 'success')->sum('amount');


        //all plans
        $user['plan'] = DB::table('plans as p')->where('p.deleted_at', NULL)->count();


        //recent successful transaction
        $user['transactions'] = DB::table('users as u')
                            ->select(DB::raw('sub.id, sub.amount, sub.user_id, sub.start_date, sub.payment_type, sub.end_date, sub.status, p.name, c.name as cname'))
                            ->leftJoin('subscriptions as sub', 'sub.user_id', 'u.user_id')
                            ->leftJoin('plans as p', 'u.plan_id', 'p.id')
                            ->leftJoin('countries as c', 'c.id', 'u.country_id')
                            ->where('sub.status', 'success')
                            ->orderBy('sub.created_at', 'desc')
                            ->take(10)
                            ->get();


        //recent registered users
        $user['users'] = DB::table('users as u')
                        ->select(DB::raw('p.name, u.id, c.name as cname, u.firstname, u.lastname, u.email, u.phone_number, u.status as ustatus, u.profile_complete, u.phone_number, u.user_id'))
                        ->leftjoin('plans as p', 'u.plan_id', 'p.id')
                        ->leftjoin('countries as c', 'c.id', 'u.country_id')
                        ->orderBy('u.created_at', 'desc')
                        ->take(10)
                        ->get();

        return view('template.admin.dashboard', ['users' => $user]);
    }
}
