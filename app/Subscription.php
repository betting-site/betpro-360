<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    protected $fillable = [
        'user_id', 'pricing_id', 'amount', 'payment_type', 'start_date', 'end_date','gateway_name', 'gateway_response', 'reference', 'status'            
    ];
}
