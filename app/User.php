<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Spatie\Permission\Traits\HasRoles;
use DB;

class User extends Authenticatable implements MustVerifyEmail
// class User extends Authenticatable 
{
    use SoftDeletes, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'user_id', 'email', 'phone_number', 'avatar', 'country_id',
        'verified_status', 'plan_id', 'last_login', 'ip_address', 'referred_by',  
        'status', 'profile_complete', 'password'
        // 'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function plan()
    {
        return $this->hasOne(Plan::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id');
    }

    public static function transaction($user_id = null)
    {
        if($user_id != null){

            return DB::table('subscriptions as sub')
                ->select(DB::raw('sub.id, sub.amount, sub.user_id, sub.payment_type, sub.start_date, sub.end_date, sub.status, pd.title, p.name'))
                ->leftJoin('plan_prices as pp', 'sub.pricing_id', 'pp.id')
                ->leftJoin('plan_durations as pd', 'pp.duration_id', 'pd.id')
                ->leftJoin('plans as p', 'pp.plan_id', 'p.id')
                ->where('sub.user_id', $user_id)
                ->paginate(20);

        }else{
            
            return DB::table('users as u')
            ->select(DB::raw('sub.id, sub.amount, sub.user_id, sub.start_date, sub.payment_type, sub.end_date, sub.status, p.name, c.name as cname'))
            ->leftJoin('subscriptions as sub', 'sub.user_id', 'u.user_id')
            ->leftJoin('plans as p', 'u.plan_id', 'p.id')
            ->leftJoin('countries as c', 'c.id', 'u.country_id')
            ->paginate(20);
        }
    }


    public static function all_active()
    {    
        return DB::table('users as u')
        ->select(DB::raw('p.name, u.id, c.name as cname, u.firstname, u.lastname, u.email, u.phone_number, u.status as ustatus, u.profile_complete, u.phone_number, u.user_id'))
        ->join('plans as p', 'u.plan_id', 'p.id')
        ->join('countries as c', 'c.id', 'u.country_id')
        ->paginate(20);
        
    }


        // DB::enableQueryLog();

        // dd(DB::getQueryLog());


    

   
}
