<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    
    protected $fillable = ['location', 'country_id', 'dest_url', 'avatar'];

    public function country()
    {
        return $this->hasMany(Country::class, 'id');
    }

}
