<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disclaimer extends Model
{
    protected $fillable = ['title' , 'description'];
}
