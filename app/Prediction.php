<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prediction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'league_id', 'teams', 'home_odd', 'draw_odd', 'away_odd', 'upcoming_pred', 'winning_tips', 'free_acca', 
        'result', 'home_form', 'away_form', 'preview', 'ft_recommendation', 'opor', 'betbuilder_odd', 
        'betrollover', 'betrollover_odd', 'sure2', 'sure2_odd',  'sure2_2odd',  'sure3_odd', 
        'sure3_2odd', 'investment', 'investment_odd', 'investment_2odd'
    ];

    public function category(){

        return $this->hasMany(Category::class, 'id');
    }
}
