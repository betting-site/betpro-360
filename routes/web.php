<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// , 'active_plan'

Route::group(['middleware' => ['auth', 'verified']], function(){

    Route::get('/dashboard', 'UserController@index')->name('dashboard')->middleware('profile');

    Route::get('/upgrade', 'HomeController@upgrade')->name('upgrade');

    Route::get('/transaction', 'UserController@tranzactn')->name('transaction');

    Route::get('/prediction/{id?}/{day?}', 'PredictionController@client_index')->name('prediction');


    Route::get('/update', 'UserController@profile')->name('update');

    Route::get('/user/{id}/pay', 'UserController@payment')->name('pay')->middleware('profile');

    Route::post('/payment/process', 'SubscriptionController@pay')->name('payee');
    
    Route::post('/update', 'UserController@updateprofile')->name('updateProfile');

    Route::get('/match/{id}/{cat_id?}', 'PredictionController@match')->name('match');

    Route::get('/search', 'UserController@searchprice');

    Route::get('/search/user', 'UserController@searchInfo')->name('searchInfo');

    Route::get('/payment/process/update/', 'SubscriptionController@update');

});


Route::group(['middleware' => ['auth', 'verified', 'role:super|expert|customer']], function(){
   
   Route::get('/admin', 'AdminController@index')->name('admin');

// });

// Route::group(['middleware' => ['auth', 'verified', 'is_admin']], function(){

    Route::get('/search/category', 'CategoryController@search');

    // crud league done
    Route::get('/league', 'LeagueController@index')->name('league');

    Route::post('/league/create', 'LeagueController@create')->name('create.league');

    Route::post('/league/edit', 'LeagueController@edit')->name('edit.league');

    Route::post('/league/delete/{id}', 'LeagueController@destroy')->name('delete.league');
    

    // crud country done
    Route::get('/country', 'countryController@index')->name('country');

    Route::post('/country/create', 'countryController@create')->name('create.country');

    Route::post('/country/edit', 'countryController@edit')->name('edit.country');

    Route::post('/country/delete/{id}', 'countryController@destroy')->name('delete.country');


    // crud result
    Route::get('/result', 'ResultController@index')->name('result');

    Route::post('/result/create', 'ResultController@create')->name('create.result');

    Route::post('/result/edit', 'ResultController@edit')->name('edit.result');

    Route::post('/result/delete/{id}', 'ResultController@destroy')->name('delete.result');


    // crud invest
    Route::get('/invest', 'InvesttimeController@index')->name('invest');

    Route::post('/invest/create', 'InvesttimeController@create')->name('create.invest');

    Route::post('/invest/edit', 'InvesttimeController@edit')->name('edit.invest');

    Route::post('/invest/delete/{id}', 'InvesttimeController@destroy')->name('delete.invest');


    // crud advisor
    Route::get('/advisor', 'AdvisorController@index')->name('advisor');

    Route::post('/advisor/create', 'AdvisorController@create')->name('create.advisor');

    Route::post('/advisor/edit', 'AdvisorController@edit')->name('edit.advisor');

    Route::post('/advisor/delete/{id}', 'AdvisorController@destroy')->name('delete.advisor');


    // crud advert
    Route::get('/advert', 'AdvertController@index')->name('advert');

    Route::post('/advert/create', 'AdvertController@create')->name('create.advert');

    Route::post('/advert/edit', 'AdvertController@edit')->name('edit.advert');

    Route::post('/advert/delete/{id}', 'AdvertController@destroy')->name('delete.advert');
    

    // crud duration 
    Route::get('/duration', 'PlandurationController@index')->name('duration');

    Route::post('/duration/create', 'PlandurationController@create')->name('create.duration');

    Route::post('/duration/edit', 'PlandurationController@edit')->name('edit.duration');

    Route::post('/duration/delete/{id}', 'PlandurationController@destroy')->name('delete.duration');


    // crud plan
    Route::get('/plan', 'PlanController@index')->name('plan');

    Route::post('/plan/create', 'PlanController@create')->name('create.plan');

    Route::post('/plan/edit', 'PlanController@edit')->name('edit.plan');

    Route::post('/plan/delete/{id}', 'PlanController@destroy')->name('delete.plan');


    // prediction
    Route::get('/admin/prediction', 'PredictionController@index')->name('adminprediction');

    Route::get('/admin/prediction/add/{id?}', 'PredictionController@show')->name('addprediction');
    // ->middleware('role:super-admin');

    Route::post('/prediction/create/{id?}', 'PredictionController@create')->name('create.prediction');
    // ->middleware('role:super-admin');

    Route::get('/specials', 'PredictionController@specials');

    Route::post('/prediction/delete/{id}', 'PredictionController@destroy')->name('delete.prediction');
    // ->middleware('role:super-admin');


    // category
    Route::get('/admin/category', 'CategoryController@show')->name('admincategory');

    Route::post('/category/create', 'CategoryController@create')->name('create.category');

    Route::post('/category/edit', 'CategoryController@edit')->name('edit.category');

    Route::post('/category/delete/{id}', 'CategoryController@destroy')->name('delete.category');


    // plan pricing
    Route::get('/admin/pricing', 'PlanPriceController@show')->name('adminpricing');

    Route::post('/price/create', 'PlanPriceController@create')->name('create.price');

    Route::post('/price/edit', 'PlanPriceController@edit')->name('edit.price');

    Route::post('/price/delete/{id}', 'PlanPriceController@destroy')->name('delete.price');


    // admin user
    Route::get('/admin/user', 'UserController@show')->name('user');

    Route::post('/user/edit', 'UserController@edit')->name('edit.user');

    Route::post('/user/delete/{id}', 'UserController@destroy')->name('delete.user');

    // admin role

    Route::get('/role', 'RoleController@index')->name('role');

    Route::post('/role/user', 'RoleController@model_has_role')->name('role.user');

    Route::post('/role/create', 'RoleController@create')->name('role.create');

    Route::post('/role/revoke', 'RoleController@revoke_user')->name('role.revoke');

    Route::post('/role/permission', 'RoleController@role_has_permission')->name('role.permission');


    // crud transaction
    Route::get('/admin/transaction', 'SubscriptionController@index')->name('subscription');

    Route::post('/transaction/create', 'SubscriptionController@create')->name('create.subscription');
    // ->middleware('role:super-admin');

    Route::post('/transaction/edit', 'SubscriptionController@edit')->name('edit.subscription');
    // ->middleware('role:super-admin');

    // Route::post('/transaction/delete/{id}', 'SubscriptionController@destroy')->name('delete.subscription')->middleware('role:super-admin');
    
});

Route::get('/match', 'HomeController@prediction');

Route::get('/plan/feature', 'PlanController@show');

Route::post('/contact', 'HomeController@contact_create')->name('addContact');

Route::get('/contact', 'HomeController@contact_index')->name('contact');

Route::get('/offers', 'HomeController@offers_index')->name('offers');

Route::get('/cookies', 'HomeController@cookies_index')->name('cookies');

Route::get('/refund', 'HomeController@refund_index')->name('refund');

Route::get('/privacy', 'HomeController@privacy_index')->name('privacy');

Route::get('/terms', 'HomeController@terms_index')->name('terms');

Route::get('/pricing', 'PlanPriceController@index')->name('pricing');

Route::get('/help', 'HomeController@help')->name('help');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/category', 'CategoryController@index')->name('category');

Route::get('/bet', 'CategoryController@bet')->name('bet');


Auth::routes(['verify' => true]);
// Auth::routes();