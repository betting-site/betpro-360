<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePredictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('predictions', function (Blueprint $table) {
            $table->id();
            $table->string('league_id')->nullable();
            $table->string('teams')->nullable();
            $table->string('home_odd')->nullable();
            $table->string('draw_odd')->nullable();
            $table->string('away_odd')->nullable();
            $table->string('winning_tips')->nullable();
            $table->string('upcoming_pred')->nullable();
            $table->string('free_acca')->nullable();
            $table->string('result')->nullable();
            $table->string('home_form')->nullable();
            $table->string('away_form')->nullable();
            $table->string('preview')->nullable();
            $table->string('opor')->nullable();
            $table->string('ft_recommendation')->nullable();
            $table->string('betrollover_odd')->nullable();
            $table->string('betbuilder_odd')->nullable();
            $table->string('sure2_odd')->nullable();
            $table->string('sure2_2odd')->nullable();
            $table->string('sure3_odd')->nullable();
            $table->string('sure3_2odd')->nullable();
            $table->string('investment_odd')->nullable();
            $table->string('investment_2odd')->nullable();
            $table->timestamp('match_date')->nullable();
            $table->softDeletesTz();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('predictions');
    }
}
