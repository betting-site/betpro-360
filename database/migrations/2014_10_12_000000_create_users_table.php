<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('user_id');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('avatar')->nullable();
            $table->string('country_id')->nullable();
            $table->unsignedInteger('plan_id')->nullable();
            $table->timestamp('last_login')->useCurrent = true;
            $table->ipAddress('ip_address')->nullable();
            $table->string('referred_by')->nullable();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
            $table->enum('profile_complete', ['Yes', 'No'])->default('No');
            $table->string('password');
            $table->softDeletesTz();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
