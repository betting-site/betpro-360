/*=========================================================================================
    File Name: documentation.js
    Description: Theme documentation js file
    ----------------------------------------------------------------------------------------
    Item Name: SPA Theme
    Version: 1.0
    Author: Tizycorp Digital
    Author URL: https://tizycorp.com
==========================================================================================*/

$(document).ready(function(){
   $('body').scrollspy({ target: '#sidebar-page-navigation' });
});